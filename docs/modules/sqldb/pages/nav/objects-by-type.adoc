= AdventureWorks2019 - Objects by type

include::nav-type-v.adoc[leveloffset=+1]

include::nav-type-u.adoc[leveloffset=+1]

include::nav-type-tr.adoc[leveloffset=+1]

include::nav-type-tf.adoc[leveloffset=+1]

include::nav-type-p.adoc[leveloffset=+1]

include::nav-type-fn.adoc[leveloffset=+1]
