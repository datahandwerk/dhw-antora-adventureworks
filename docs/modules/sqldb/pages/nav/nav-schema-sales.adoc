= Sales

== Description

Contains objects related to customers, sales orders, and sales territories.

== Objects

include::partial$navlist/navlist-schema-sales.adoc[]

