// tag::HeaderFullDisplayName[]
= Person.Address - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
39CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Person.Address
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1029578706
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:14
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-addressid]
=== AddressID

[cols="d,8a,m,m,m"]
|===
|1
|*AddressID*
|int
|NOT NULL
|(1,1)
|===


[#column-addressline1]
=== AddressLine1

[cols="d,8a,m,m,m"]
|===
|
|AddressLine1
|nvarchar(60)
|NOT NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-addressline1[+HumanResources.vEmployee.AddressLine1+]
* xref:purchasing.vvendorwithaddresses.adoc#column-addressline1[+Purchasing.vVendorWithAddresses.AddressLine1+]
* xref:sales.vindividualcustomer.adoc#column-addressline1[+Sales.vIndividualCustomer.AddressLine1+]
* xref:sales.vsalesperson.adoc#column-addressline1[+Sales.vSalesPerson.AddressLine1+]
* xref:sales.vstorewithaddresses.adoc#column-addressline1[+Sales.vStoreWithAddresses.AddressLine1+]
--


[#column-addressline2]
=== AddressLine2

[cols="d,8a,m,m,m"]
|===
|
|AddressLine2
|nvarchar(60)
|NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-addressline2[+HumanResources.vEmployee.AddressLine2+]
* xref:purchasing.vvendorwithaddresses.adoc#column-addressline2[+Purchasing.vVendorWithAddresses.AddressLine2+]
* xref:sales.vindividualcustomer.adoc#column-addressline2[+Sales.vIndividualCustomer.AddressLine2+]
* xref:sales.vsalesperson.adoc#column-addressline2[+Sales.vSalesPerson.AddressLine2+]
* xref:sales.vstorewithaddresses.adoc#column-addressline2[+Sales.vStoreWithAddresses.AddressLine2+]
--


[#column-city]
=== City

[cols="d,8a,m,m,m"]
|===
|
|City
|nvarchar(30)
|NOT NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-city[+HumanResources.vEmployee.City+]
* xref:purchasing.vvendorwithaddresses.adoc#column-city[+Purchasing.vVendorWithAddresses.City+]
* xref:sales.vindividualcustomer.adoc#column-city[+Sales.vIndividualCustomer.City+]
* xref:sales.vsalesperson.adoc#column-city[+Sales.vSalesPerson.City+]
* xref:sales.vstorewithaddresses.adoc#column-city[+Sales.vStoreWithAddresses.City+]
--


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_Address_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-postalcode]
=== PostalCode

[cols="d,8a,m,m,m"]
|===
|
|PostalCode
|nvarchar(15)
|NOT NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-postalcode[+HumanResources.vEmployee.PostalCode+]
* xref:purchasing.vvendorwithaddresses.adoc#column-postalcode[+Purchasing.vVendorWithAddresses.PostalCode+]
* xref:sales.vindividualcustomer.adoc#column-postalcode[+Sales.vIndividualCustomer.PostalCode+]
* xref:sales.vsalesperson.adoc#column-postalcode[+Sales.vSalesPerson.PostalCode+]
* xref:sales.vstorewithaddresses.adoc#column-postalcode[+Sales.vStoreWithAddresses.PostalCode+]
--


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid

.Default: DF_Address_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|
|===


[#column-spatiallocation]
=== SpatialLocation

[cols="d,8a,m,m,m"]
|===
|
|SpatialLocation
|geography
|NULL
|
|===


[#column-stateprovinceid]
=== StateProvinceID

[cols="d,8a,m,m,m"]
|===
|
|StateProvinceID
|int
|NOT NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-addressid>>*
|int
|NOT NULL
|(1,1)









// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-addressline1>>
|nvarchar(60)
|NOT NULL
|

|
|<<column-addressline2>>
|nvarchar(60)
|NULL
|

|
|<<column-city>>
|nvarchar(30)
|NOT NULL
|

|
|<<column-modifieddate>>

.Default: DF_Address_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-postalcode>>
|nvarchar(15)
|NOT NULL
|

|
|<<column-rowguid>>

.Default: DF_Address_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|

|
|<<column-spatiallocation>>
|geography
|NULL
|

|
|<<column-stateprovinceid>>
|int
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlineaddressunderlineaddressid]
=== PK_Address_AddressID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-AddressID>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:person.address.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:humanresources.vemployee.adoc[]
* xref:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]
* xref:aw:sqldb:sales.vindividualcustomer.adoc[]
* xref:aw:sqldb:sales.vsalesperson.adoc[]
* xref:aw:sqldb:sales.vstorewithaddresses.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
DFCCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
AddressID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - City : (nvarchar(30))
  - ModifiedDate : (datetime)
  - PostalCode : (nvarchar(15))
  - rowguid : (uniqueidentifier)
  SpatialLocation : (geography)
  - StateProvinceID : (int)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
**PK_Address_AddressID**

..
AddressID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  --
}

entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithaddresses.adoc[]" as Sales.vStoreWithAddresses << V >> {
  --
}

Person.Address <.. HumanResources.vEmployee
Person.Address <.. Purchasing.vVendorWithAddresses
Person.Address <.. Sales.vIndividualCustomer
Person.Address <.. Sales.vSalesPerson
Person.Address <.. Sales.vStoreWithAddresses

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  --
}

entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithaddresses.adoc[]" as Sales.vStoreWithAddresses << V >> {
  --
}

Person.Address <.. HumanResources.vEmployee
Person.Address <.. Purchasing.vVendorWithAddresses
Person.Address <.. Sales.vIndividualCustomer
Person.Address <.. Sales.vSalesPerson
Person.Address <.. Sales.vStoreWithAddresses

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  AdditionalContactInfo : (xml)
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - JobTitle : (nvarchar(50))
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - City : (nvarchar(30))
  - ModifiedDate : (datetime)
  - PostalCode : (nvarchar(15))
  - rowguid : (uniqueidentifier)
  SpatialLocation : (geography)
  - StateProvinceID : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  - Name : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  Demographics : (xml)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - JobTitle : (nvarchar(50))
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - SalesLastYear : (money)
  SalesQuota : (money)
  - SalesYTD : (money)
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  TerritoryGroup : (nvarchar(50))
  TerritoryName : (Name)
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithaddresses.adoc[]" as Sales.vStoreWithAddresses << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  - Name : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  --
}

Person.Address <.. HumanResources.vEmployee
Person.Address <.. Purchasing.vVendorWithAddresses
Person.Address <.. Sales.vIndividualCustomer
Person.Address <.. Sales.vSalesPerson
Person.Address <.. Sales.vStoreWithAddresses
"Person.Address::AddressLine1" <-- "HumanResources.vEmployee::AddressLine1"
"Person.Address::AddressLine1" <-- "Purchasing.vVendorWithAddresses::AddressLine1"
"Person.Address::AddressLine1" <-- "Sales.vIndividualCustomer::AddressLine1"
"Person.Address::AddressLine1" <-- "Sales.vSalesPerson::AddressLine1"
"Person.Address::AddressLine1" <-- "Sales.vStoreWithAddresses::AddressLine1"
"Person.Address::AddressLine2" <-- "HumanResources.vEmployee::AddressLine2"
"Person.Address::AddressLine2" <-- "Purchasing.vVendorWithAddresses::AddressLine2"
"Person.Address::AddressLine2" <-- "Sales.vIndividualCustomer::AddressLine2"
"Person.Address::AddressLine2" <-- "Sales.vSalesPerson::AddressLine2"
"Person.Address::AddressLine2" <-- "Sales.vStoreWithAddresses::AddressLine2"
"Person.Address::City" <-- "HumanResources.vEmployee::City"
"Person.Address::City" <-- "Purchasing.vVendorWithAddresses::City"
"Person.Address::City" <-- "Sales.vIndividualCustomer::City"
"Person.Address::City" <-- "Sales.vSalesPerson::City"
"Person.Address::City" <-- "Sales.vStoreWithAddresses::City"
"Person.Address::PostalCode" <-- "HumanResources.vEmployee::PostalCode"
"Person.Address::PostalCode" <-- "Purchasing.vVendorWithAddresses::PostalCode"
"Person.Address::PostalCode" <-- "Sales.vIndividualCustomer::PostalCode"
"Person.Address::PostalCode" <-- "Sales.vSalesPerson::PostalCode"
"Person.Address::PostalCode" <-- "Sales.vStoreWithAddresses::PostalCode"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


