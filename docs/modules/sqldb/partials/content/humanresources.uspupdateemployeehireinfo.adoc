// tag::HeaderFullDisplayName[]
= HumanResources.uspUpdateEmployeeHireInfo - P
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
22CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
HumanResources.uspUpdateEmployeeHireInfo
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
P 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
stored procedure
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
951674438
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]

// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]

// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:dbo.usplogerror.adoc[]
* xref:aw:sqldb:humanresources.employee.adoc[]
* xref:aw:sqldb:humanresources.employeepayhistory.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}





footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:humanresources.employeepayhistory.adoc[]" as HumanResources.EmployeePayHistory << U >> {
  - **BusinessEntityID** : (int)
  - **RateChangeDate** : (datetime)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.EmployeePayHistory <.. HumanResources.uspUpdateEmployeeHireInfo

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.errorlog.adoc[]" as dbo.ErrorLog << U >> {
  - **ErrorLogID** : (int)
  --
}

entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspprinterror.adoc[]" as dbo.uspPrintError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:humanresources.employeepayhistory.adoc[]" as HumanResources.EmployeePayHistory << U >> {
  - **BusinessEntityID** : (int)
  - **RateChangeDate** : (datetime)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

dbo.ErrorLog <.. dbo.uspLogError
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
dbo.uspPrintError <.. dbo.uspLogError
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.EmployeePayHistory <.. HumanResources.uspUpdateEmployeeHireInfo

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  - BirthDate : (date)
  - CurrentFlag : (Flag)
  - Gender : (nchar(1))
  - HireDate : (date)
  - JobTitle : (nvarchar(50))
  - LoginID : (nvarchar(256))
  - MaritalStatus : (nchar(1))
  - ModifiedDate : (datetime)
  - NationalIDNumber : (nvarchar(15))
  OrganizationNode : (hierarchyid)
  - rowguid : (uniqueidentifier)
  - SalariedFlag : (Flag)
  - SickLeaveHours : (smallint)
  - VacationHours : (smallint)
  ~ OrganizationLevel : (smallint)
  --
}

entity "puml-link:aw:sqldb:humanresources.employeepayhistory.adoc[]" as HumanResources.EmployeePayHistory << U >> {
  - **BusinessEntityID** : (int)
  - **RateChangeDate** : (datetime)
  - ModifiedDate : (datetime)
  - PayFrequency : (tinyint)
  - Rate : (money)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.EmployeePayHistory <.. HumanResources.uspUpdateEmployeeHireInfo


footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE PROCEDURE [HumanResources].[uspUpdateEmployeeHireInfo]
    @BusinessEntityID [int], 
    @JobTitle [nvarchar](50), 
    @HireDate [datetime], 
    @RateChangeDate [datetime], 
    @Rate [money], 
    @PayFrequency [tinyint], 
    @CurrentFlag [dbo].[Flag] 
WITH EXECUTE AS CALLER
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        BEGIN TRANSACTION;

        UPDATE [HumanResources].[Employee] 
        SET [JobTitle] = @JobTitle 
            ,[HireDate] = @HireDate 
            ,[CurrentFlag] = @CurrentFlag 
        WHERE [BusinessEntityID] = @BusinessEntityID;

        INSERT INTO [HumanResources].[EmployeePayHistory] 
            ([BusinessEntityID]
            ,[RateChangeDate]
            ,[Rate]
            ,[PayFrequency]) 
        VALUES (@BusinessEntityID, @RateChangeDate, @Rate, @PayFrequency);

        COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        -- Rollback any active or uncommittable transactions before
        -- inserting information in the ErrorLog
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
        END

        EXECUTE [dbo].[uspLogError];
    END CATCH;
END;

----
=======
// end::sql_modules_definition[]


