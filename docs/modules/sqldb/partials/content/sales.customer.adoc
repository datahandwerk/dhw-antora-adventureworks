// tag::HeaderFullDisplayName[]
= Sales.Customer - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
D5CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Sales.Customer
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1653580929
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:14
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-customerid]
=== CustomerID

[cols="d,8a,m,m,m"]
|===
|1
|*CustomerID*
|int
|NOT NULL
|(1,1)
|===

.Referencing Columns
--
* xref:sales.customer.adoc#column-accountnumber[+Sales.Customer.AccountNumber+]
--


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_Customer_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-personid]
=== PersonID

[cols="d,8a,m,m,m"]
|===
|
|PersonID
|int
|NULL
|
|===


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid

.Default: DF_Customer_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|
|===


[#column-storeid]
=== StoreID

[cols="d,8a,m,m,m"]
|===
|
|StoreID
|int
|NULL
|
|===


[#column-territoryid]
=== TerritoryID

[cols="d,8a,m,m,m"]
|===
|
|TerritoryID
|int
|NULL
|
|===


[#column-accountnumber]
=== AccountNumber

[cols="d,8a,m,m,m"]
|===
|
|AccountNumber

.Definition
[source,sql]
----
(isnull('AW'+[dbo].[ufnLeadingZeros]([CustomerID]),''))
----


|varchar(10)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.customer.adoc#column-customerid[+Sales.Customer.CustomerID+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-customerid>>*
|int
|NOT NULL
|(1,1)







// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-modifieddate>>

.Default: DF_Customer_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-personid>>
|int
|NULL
|

|
|<<column-rowguid>>

.Default: DF_Customer_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|

|
|<<column-storeid>>
|int
|NULL
|

|
|<<column-territoryid>>
|int
|NULL
|

|
|<<column-accountnumber>>

.Definition
[source,sql]
----
(isnull('AW'+[dbo].[ufnLeadingZeros]([CustomerID]),''))
----


|varchar(10)
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinecustomerunderlinecustomerid]
=== PK_Customer_CustomerID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-CustomerID>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:sales.customer.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:dbo.ufngetcontactinformation.adoc[]
* xref:aw:sqldb:sales.idusalesorderdetail.adoc[]
* xref:aw:sqldb:sales.vindividualcustomer.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
17CDEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
CustomerID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
  - **CustomerID** : (int)
  - ModifiedDate : (datetime)
  PersonID : (int)
  - rowguid : (uniqueidentifier)
  StoreID : (int)
  TerritoryID : (int)
  ~ AccountNumber : (varchar(10))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
**PK_Customer_CustomerID**

..
CustomerID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  --
}

entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
  - **CustomerID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  --
}

Sales.Customer <.. dbo.ufnGetContactInformation
Sales.Customer <.. Sales.iduSalesOrderDetail
Sales.Customer <.. Sales.vIndividualCustomer

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
  - **CustomerID** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  --
}

entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
  - **CustomerID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  --
}

Sales.Customer <.. dbo.ufnGetContactInformation
Sales.Customer <.. Sales.iduSalesOrderDetail
Sales.Customer <.. Sales.vIndividualCustomer

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  BusinessEntityType : (nvarchar(50))
  FirstName : (nvarchar(50))
  JobTitle : (nvarchar(50))
  LastName : (nvarchar(50))
  - PersonID : (int)
  --
}

entity "puml-link:aw:sqldb:sales.customer.adoc[]" as Sales.Customer << U >> {
  - **CustomerID** : (int)
  - ModifiedDate : (datetime)
  PersonID : (int)
  - rowguid : (uniqueidentifier)
  StoreID : (int)
  TerritoryID : (int)
  ~ AccountNumber : (varchar(10))
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.vindividualcustomer.adoc[]" as Sales.vIndividualCustomer << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  Demographics : (xml)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

Sales.Customer <.. dbo.ufnGetContactInformation
Sales.Customer <.. Sales.iduSalesOrderDetail
Sales.Customer <.. Sales.vIndividualCustomer
"Sales.Customer::CustomerID" <-- "Sales.Customer::AccountNumber"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


