// tag::HeaderFullDisplayName[]
= Production.vProductModelCatalogDescription - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
D1CBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Production.vProductModelCatalogDescription
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
567673070
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-bikeframe]
=== BikeFrame

[cols="d,8a,m,m,m"]
|===
|
|BikeFrame
|nvarchar(max)
|NULL
|
|===


[#column-color]
=== Color

[cols="d,8a,m,m,m"]
|===
|
|Color
|nvarchar(256)
|NULL
|
|===


[#column-copyright]
=== Copyright

[cols="d,8a,m,m,m"]
|===
|
|Copyright
|nvarchar(30)
|NULL
|
|===


[#column-crankset]
=== Crankset

[cols="d,8a,m,m,m"]
|===
|
|Crankset
|nvarchar(256)
|NULL
|
|===


[#column-maintenancedescription]
=== MaintenanceDescription

[cols="d,8a,m,m,m"]
|===
|
|MaintenanceDescription
|nvarchar(256)
|NULL
|
|===


[#column-manufacturer]
=== Manufacturer

[cols="d,8a,m,m,m"]
|===
|
|Manufacturer
|nvarchar(max)
|NULL
|
|===


[#column-material]
=== Material

[cols="d,8a,m,m,m"]
|===
|
|Material
|nvarchar(256)
|NULL
|
|===


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate
|datetime
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-modifieddate[+Production.ProductModel.ModifiedDate+]
--


[#column-name]
=== Name

[cols="d,8a,m,m,m"]
|===
|
|Name
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-name[+Production.ProductModel.Name+]
--


[#column-noofyears]
=== NoOfYears

[cols="d,8a,m,m,m"]
|===
|
|NoOfYears
|nvarchar(256)
|NULL
|
|===


[#column-pedal]
=== Pedal

[cols="d,8a,m,m,m"]
|===
|
|Pedal
|nvarchar(256)
|NULL
|
|===


[#column-pictureangle]
=== PictureAngle

[cols="d,8a,m,m,m"]
|===
|
|PictureAngle
|nvarchar(256)
|NULL
|
|===


[#column-picturesize]
=== PictureSize

[cols="d,8a,m,m,m"]
|===
|
|PictureSize
|nvarchar(256)
|NULL
|
|===


[#column-productline]
=== ProductLine

[cols="d,8a,m,m,m"]
|===
|
|ProductLine
|nvarchar(256)
|NULL
|
|===


[#column-productmodelid]
=== ProductModelID

[cols="d,8a,m,m,m"]
|===
|
|ProductModelID
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-productmodelid[+Production.ProductModel.ProductModelID+]
--


[#column-productphotoid]
=== ProductPhotoID

[cols="d,8a,m,m,m"]
|===
|
|ProductPhotoID
|nvarchar(256)
|NULL
|
|===


[#column-producturl]
=== ProductURL

[cols="d,8a,m,m,m"]
|===
|
|ProductURL
|nvarchar(256)
|NULL
|
|===


[#column-riderexperience]
=== RiderExperience

[cols="d,8a,m,m,m"]
|===
|
|RiderExperience
|nvarchar(1024)
|NULL
|
|===


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid
|uniqueidentifier
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-rowguid[+Production.ProductModel.rowguid+]
--


[#column-saddle]
=== Saddle

[cols="d,8a,m,m,m"]
|===
|
|Saddle
|nvarchar(256)
|NULL
|
|===


[#column-style]
=== Style

[cols="d,8a,m,m,m"]
|===
|
|Style
|nvarchar(256)
|NULL
|
|===


[#column-summary]
=== Summary

[cols="d,8a,m,m,m"]
|===
|
|Summary
|nvarchar(max)
|NULL
|
|===


[#column-warrantydescription]
=== WarrantyDescription

[cols="d,8a,m,m,m"]
|===
|
|WarrantyDescription
|nvarchar(256)
|NULL
|
|===


[#column-warrantyperiod]
=== WarrantyPeriod

[cols="d,8a,m,m,m"]
|===
|
|WarrantyPeriod
|nvarchar(256)
|NULL
|
|===


[#column-wheel]
=== Wheel

[cols="d,8a,m,m,m"]
|===
|
|Wheel
|nvarchar(256)
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]

























// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-bikeframe>>
|nvarchar(max)
|NULL
|

|
|<<column-color>>
|nvarchar(256)
|NULL
|

|
|<<column-copyright>>
|nvarchar(30)
|NULL
|

|
|<<column-crankset>>
|nvarchar(256)
|NULL
|

|
|<<column-maintenancedescription>>
|nvarchar(256)
|NULL
|

|
|<<column-manufacturer>>
|nvarchar(max)
|NULL
|

|
|<<column-material>>
|nvarchar(256)
|NULL
|

|
|<<column-modifieddate>>
|datetime
|NOT NULL
|

|
|<<column-name>>
|Name
|NOT NULL
|

|
|<<column-noofyears>>
|nvarchar(256)
|NULL
|

|
|<<column-pedal>>
|nvarchar(256)
|NULL
|

|
|<<column-pictureangle>>
|nvarchar(256)
|NULL
|

|
|<<column-picturesize>>
|nvarchar(256)
|NULL
|

|
|<<column-productline>>
|nvarchar(256)
|NULL
|

|
|<<column-productmodelid>>
|int
|NOT NULL
|

|
|<<column-productphotoid>>
|nvarchar(256)
|NULL
|

|
|<<column-producturl>>
|nvarchar(256)
|NULL
|

|
|<<column-riderexperience>>
|nvarchar(1024)
|NULL
|

|
|<<column-rowguid>>
|uniqueidentifier
|NOT NULL
|

|
|<<column-saddle>>
|nvarchar(256)
|NULL
|

|
|<<column-style>>
|nvarchar(256)
|NULL
|

|
|<<column-summary>>
|nvarchar(max)
|NULL
|

|
|<<column-warrantydescription>>
|nvarchar(256)
|NULL
|

|
|<<column-warrantyperiod>>
|nvarchar(256)
|NULL
|

|
|<<column-wheel>>
|nvarchar(256)
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevproductmodelcatalogdescriptionunderlineunderline1]
=== idx_vProductModelCatalogDescription++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-ProductModelID>>; int
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:production.vproductmodelcatalogdescription.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:production.productmodel.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
  BikeFrame : (nvarchar(max))
  Color : (nvarchar(256))
  Copyright : (nvarchar(30))
  Crankset : (nvarchar(256))
  MaintenanceDescription : (nvarchar(256))
  Manufacturer : (nvarchar(max))
  Material : (nvarchar(256))
  - ModifiedDate : (datetime)
  - Name : (Name)
  NoOfYears : (nvarchar(256))
  Pedal : (nvarchar(256))
  PictureAngle : (nvarchar(256))
  PictureSize : (nvarchar(256))
  ProductLine : (nvarchar(256))
  - ProductModelID : (int)
  ProductPhotoID : (nvarchar(256))
  ProductURL : (nvarchar(256))
  RiderExperience : (nvarchar(1024))
  - rowguid : (uniqueidentifier)
  Saddle : (nvarchar(256))
  Style : (nvarchar(256))
  Summary : (nvarchar(max))
  WarrantyDescription : (nvarchar(256))
  WarrantyPeriod : (nvarchar(256))
  Wheel : (nvarchar(256))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
- idx_vProductModelCatalogDescription__1

..
ProductModelID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
  --
}

Production.ProductModel <.. Production.vProductModelCatalogDescription

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
  --
}

Production.ProductModel <.. Production.vProductModelCatalogDescription

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  CatalogDescription : (xml)
  Instructions : (xml)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelcatalogdescription.adoc[]" as Production.vProductModelCatalogDescription << V >> {
  BikeFrame : (nvarchar(max))
  Color : (nvarchar(256))
  Copyright : (nvarchar(30))
  Crankset : (nvarchar(256))
  MaintenanceDescription : (nvarchar(256))
  Manufacturer : (nvarchar(max))
  Material : (nvarchar(256))
  - ModifiedDate : (datetime)
  - Name : (Name)
  NoOfYears : (nvarchar(256))
  Pedal : (nvarchar(256))
  PictureAngle : (nvarchar(256))
  PictureSize : (nvarchar(256))
  ProductLine : (nvarchar(256))
  - ProductModelID : (int)
  ProductPhotoID : (nvarchar(256))
  ProductURL : (nvarchar(256))
  RiderExperience : (nvarchar(1024))
  - rowguid : (uniqueidentifier)
  Saddle : (nvarchar(256))
  Style : (nvarchar(256))
  Summary : (nvarchar(max))
  WarrantyDescription : (nvarchar(256))
  WarrantyPeriod : (nvarchar(256))
  Wheel : (nvarchar(256))
  --
}

Production.ProductModel <.. Production.vProductModelCatalogDescription
"Production.ProductModel::ModifiedDate" <-- "Production.vProductModelCatalogDescription::ModifiedDate"
"Production.ProductModel::Name" <-- "Production.vProductModelCatalogDescription::Name"
"Production.ProductModel::ProductModelID" <-- "Production.vProductModelCatalogDescription::ProductModelID"
"Production.ProductModel::rowguid" <-- "Production.vProductModelCatalogDescription::rowguid"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE VIEW [Production].[vProductModelCatalogDescription] 
AS 
SELECT 
    [ProductModelID] 
    ,[Name] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace html="http://www.w3.org/1999/xhtml"; 
        (/p1:ProductDescription/p1:Summary/html:p)[1]', 'nvarchar(max)') AS [Summary] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Manufacturer/p1:Name)[1]', 'nvarchar(max)') AS [Manufacturer] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Manufacturer/p1:Copyright)[1]', 'nvarchar(30)') AS [Copyright] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Manufacturer/p1:ProductURL)[1]', 'nvarchar(256)') AS [ProductURL] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wm="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelWarrAndMain"; 
        (/p1:ProductDescription/p1:Features/wm:Warranty/wm:WarrantyPeriod)[1]', 'nvarchar(256)') AS [WarrantyPeriod] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wm="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelWarrAndMain"; 
        (/p1:ProductDescription/p1:Features/wm:Warranty/wm:Description)[1]', 'nvarchar(256)') AS [WarrantyDescription] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wm="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelWarrAndMain"; 
        (/p1:ProductDescription/p1:Features/wm:Maintenance/wm:NoOfYears)[1]', 'nvarchar(256)') AS [NoOfYears] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wm="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelWarrAndMain"; 
        (/p1:ProductDescription/p1:Features/wm:Maintenance/wm:Description)[1]', 'nvarchar(256)') AS [MaintenanceDescription] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wf="http://www.adventure-works.com/schemas/OtherFeatures"; 
        (/p1:ProductDescription/p1:Features/wf:wheel)[1]', 'nvarchar(256)') AS [Wheel] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wf="http://www.adventure-works.com/schemas/OtherFeatures"; 
        (/p1:ProductDescription/p1:Features/wf:saddle)[1]', 'nvarchar(256)') AS [Saddle] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wf="http://www.adventure-works.com/schemas/OtherFeatures"; 
        (/p1:ProductDescription/p1:Features/wf:pedal)[1]', 'nvarchar(256)') AS [Pedal] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wf="http://www.adventure-works.com/schemas/OtherFeatures"; 
        (/p1:ProductDescription/p1:Features/wf:BikeFrame)[1]', 'nvarchar(max)') AS [BikeFrame] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        declare namespace wf="http://www.adventure-works.com/schemas/OtherFeatures"; 
        (/p1:ProductDescription/p1:Features/wf:crankset)[1]', 'nvarchar(256)') AS [Crankset] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Picture/p1:Angle)[1]', 'nvarchar(256)') AS [PictureAngle] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Picture/p1:Size)[1]', 'nvarchar(256)') AS [PictureSize] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Picture/p1:ProductPhotoID)[1]', 'nvarchar(256)') AS [ProductPhotoID] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Specifications/Material)[1]', 'nvarchar(256)') AS [Material] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Specifications/Color)[1]', 'nvarchar(256)') AS [Color] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Specifications/ProductLine)[1]', 'nvarchar(256)') AS [ProductLine] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Specifications/Style)[1]', 'nvarchar(256)') AS [Style] 
    ,[CatalogDescription].value(N'declare namespace p1="http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelDescription"; 
        (/p1:ProductDescription/p1:Specifications/RiderExperience)[1]', 'nvarchar(1024)') AS [RiderExperience] 
    ,[rowguid] 
    ,[ModifiedDate]
FROM [Production].[ProductModel] 
WHERE [CatalogDescription] IS NOT NULL;

----
=======
// end::sql_modules_definition[]


