// tag::HeaderFullDisplayName[]
= Sales.SalesOrderDetail - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
FCCCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Sales.SalesOrderDetail
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1810105489
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:14
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-salesorderid]
=== SalesOrderID

[cols="d,8a,m,m,m"]
|===
|1
|*SalesOrderID*
|int
|NOT NULL
|
|===


[#column-salesorderdetailid]
=== SalesOrderDetailID

[cols="d,8a,m,m,m"]
|===
|2
|*SalesOrderDetailID*
|int
|NOT NULL
|(1,1)
|===


[#column-carriertrackingnumber]
=== CarrierTrackingNumber

[cols="d,8a,m,m,m"]
|===
|
|CarrierTrackingNumber
|nvarchar(25)
|NULL
|
|===


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_SalesOrderDetail_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-orderqty]
=== OrderQty

[cols="d,8a,m,m,m"]
|===
|
|OrderQty
|smallint
|NOT NULL
|
|===

.Referencing Columns
--
* xref:sales.salesorderdetail.adoc#column-linetotal[+Sales.SalesOrderDetail.LineTotal+]
--


[#column-productid]
=== ProductID

[cols="d,8a,m,m,m"]
|===
|
|ProductID
|int
|NOT NULL
|
|===


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid

.Default: DF_SalesOrderDetail_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|
|===


[#column-specialofferid]
=== SpecialOfferID

[cols="d,8a,m,m,m"]
|===
|
|SpecialOfferID
|int
|NOT NULL
|
|===


[#column-unitprice]
=== UnitPrice

[cols="d,8a,m,m,m"]
|===
|
|UnitPrice
|money
|NOT NULL
|
|===

.Referencing Columns
--
* xref:sales.salesorderdetail.adoc#column-linetotal[+Sales.SalesOrderDetail.LineTotal+]
--


[#column-unitpricediscount]
=== UnitPriceDiscount

[cols="d,8a,m,m,m"]
|===
|
|UnitPriceDiscount

.Default: DF_SalesOrderDetail_UnitPriceDiscount
[source,sql]
----
((0.0))
----


|money
|NOT NULL
|
|===

.Referencing Columns
--
* xref:sales.salesorderdetail.adoc#column-linetotal[+Sales.SalesOrderDetail.LineTotal+]
--


[#column-linetotal]
=== LineTotal

[cols="d,8a,m,m,m"]
|===
|
|LineTotal

.Definition
[source,sql]
----
(isnull(([UnitPrice]*((1.0)-[UnitPriceDiscount]))*[OrderQty],(0.0)))
----


|numeric(38, 6)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.salesorderdetail.adoc#column-orderqty[+Sales.SalesOrderDetail.OrderQty+]
* xref:sales.salesorderdetail.adoc#column-unitprice[+Sales.SalesOrderDetail.UnitPrice+]
* xref:sales.salesorderdetail.adoc#column-unitpricediscount[+Sales.SalesOrderDetail.UnitPriceDiscount+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-salesorderid>>*
|int
|NOT NULL
|

|2
|*<<column-salesorderdetailid>>*
|int
|NOT NULL
|(1,1)










// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]


|
|<<column-carriertrackingnumber>>
|nvarchar(25)
|NULL
|

|
|<<column-modifieddate>>

.Default: DF_SalesOrderDetail_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-orderqty>>
|smallint
|NOT NULL
|

|
|<<column-productid>>
|int
|NOT NULL
|

|
|<<column-rowguid>>

.Default: DF_SalesOrderDetail_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|

|
|<<column-specialofferid>>
|int
|NOT NULL
|

|
|<<column-unitprice>>
|money
|NOT NULL
|

|
|<<column-unitpricediscount>>

.Default: DF_SalesOrderDetail_UnitPriceDiscount
[source,sql]
----
((0.0))
----


|money
|NOT NULL
|

|
|<<column-linetotal>>

.Definition
[source,sql]
----
(isnull(([UnitPrice]*((1.0)-[UnitPriceDiscount]))*[OrderQty],(0.0)))
----


|numeric(38, 6)
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinesalesorderdetailunderlinesalesorderidunderlinesalesorderdetailid]
=== PK_SalesOrderDetail_SalesOrderID_SalesOrderDetailID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-SalesOrderID>>; int
* <<column-SalesOrderDetailID>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:sales.salesorderdetail.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:sales.idusalesorderdetail.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
8ECBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int,int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
SalesOrderID,SalesOrderDetailID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
  - **SalesOrderID** : (int)
  - **SalesOrderDetailID** : (int)
  CarrierTrackingNumber : (nvarchar(25))
  - ModifiedDate : (datetime)
  - OrderQty : (smallint)
  - ProductID : (int)
  - rowguid : (uniqueidentifier)
  - SpecialOfferID : (int)
  - UnitPrice : (money)
  - UnitPriceDiscount : (money)
  ~ LineTotal : (numeric(38, 6))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
**PK_SalesOrderDetail_SalesOrderID_SalesOrderDetailID**

..
SalesOrderID; int
SalesOrderDetailID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
  - **SalesOrderID** : (int)
  - **SalesOrderDetailID** : (int)
  --
}

Sales.SalesOrderDetail <.. Sales.iduSalesOrderDetail

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
  - **SalesOrderID** : (int)
  - **SalesOrderDetailID** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
  - **SalesOrderID** : (int)
  - **SalesOrderDetailID** : (int)
  --
}

Sales.SalesOrderDetail <.. Sales.iduSalesOrderDetail

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.salesorderdetail.adoc[]" as Sales.SalesOrderDetail << U >> {
  - **SalesOrderID** : (int)
  - **SalesOrderDetailID** : (int)
  CarrierTrackingNumber : (nvarchar(25))
  - ModifiedDate : (datetime)
  - OrderQty : (smallint)
  - ProductID : (int)
  - rowguid : (uniqueidentifier)
  - SpecialOfferID : (int)
  - UnitPrice : (money)
  - UnitPriceDiscount : (money)
  ~ LineTotal : (numeric(38, 6))
  --
}

Sales.SalesOrderDetail <.. Sales.iduSalesOrderDetail
"Sales.SalesOrderDetail::OrderQty" <-- "Sales.SalesOrderDetail::LineTotal"
"Sales.SalesOrderDetail::UnitPrice" <-- "Sales.SalesOrderDetail::LineTotal"
"Sales.SalesOrderDetail::UnitPriceDiscount" <-- "Sales.SalesOrderDetail::LineTotal"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


