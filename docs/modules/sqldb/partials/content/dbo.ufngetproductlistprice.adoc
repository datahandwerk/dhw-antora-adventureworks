// tag::HeaderFullDisplayName[]
= dbo.ufnGetProductListPrice - FN
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
01CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.ufnGetProductListPrice
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
FN
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
scalar function
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
791673868
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2020-06-15 10:36:59
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]

// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]

// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:dbo.ufngetproductlistprice.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:production.product.adoc[]
* xref:aw:sqldb:production.productlistpricehistory.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetproductlistprice.adoc[]" as dbo.ufnGetProductListPrice << FN >> {
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}





footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetproductlistprice.adoc[]" as dbo.ufnGetProductListPrice << FN >> {
  --
}

entity "puml-link:aw:sqldb:production.product.adoc[]" as Production.Product << U >> {
  - **ProductID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.productlistpricehistory.adoc[]" as Production.ProductListPriceHistory << U >> {
  - **ProductID** : (int)
  - **StartDate** : (datetime)
  --
}

Production.Product <.. dbo.ufnGetProductListPrice
Production.ProductListPriceHistory <.. dbo.ufnGetProductListPrice

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetproductlistprice.adoc[]" as dbo.ufnGetProductListPrice << FN >> {
  --
}

entity "puml-link:aw:sqldb:production.product.adoc[]" as Production.Product << U >> {
  - **ProductID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.productlistpricehistory.adoc[]" as Production.ProductListPriceHistory << U >> {
  - **ProductID** : (int)
  - **StartDate** : (datetime)
  --
}

Production.Product <.. dbo.ufnGetProductListPrice
Production.ProductListPriceHistory <.. dbo.ufnGetProductListPrice

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetproductlistprice.adoc[]" as dbo.ufnGetProductListPrice << FN >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetproductlistprice.adoc[]" as dbo.ufnGetProductListPrice << FN >> {
  --
}

entity "puml-link:aw:sqldb:production.product.adoc[]" as Production.Product << U >> {
  - **ProductID** : (int)
  Class : (nchar(2))
  Color : (nvarchar(15))
  - DaysToManufacture : (int)
  DiscontinuedDate : (datetime)
  - FinishedGoodsFlag : (Flag)
  - ListPrice : (money)
  - MakeFlag : (Flag)
  - ModifiedDate : (datetime)
  - Name : (Name)
  ProductLine : (nchar(2))
  ProductModelID : (int)
  - ProductNumber : (nvarchar(25))
  ProductSubcategoryID : (int)
  - ReorderPoint : (smallint)
  - rowguid : (uniqueidentifier)
  - SafetyStockLevel : (smallint)
  SellEndDate : (datetime)
  - SellStartDate : (datetime)
  Size : (nvarchar(5))
  SizeUnitMeasureCode : (nchar(3))
  - StandardCost : (money)
  Style : (nchar(2))
  Weight : (decimal(8, 2))
  WeightUnitMeasureCode : (nchar(3))
  --
}

entity "puml-link:aw:sqldb:production.productlistpricehistory.adoc[]" as Production.ProductListPriceHistory << U >> {
  - **ProductID** : (int)
  - **StartDate** : (datetime)
  EndDate : (datetime)
  - ListPrice : (money)
  - ModifiedDate : (datetime)
  --
}

Production.Product <.. dbo.ufnGetProductListPrice
Production.ProductListPriceHistory <.. dbo.ufnGetProductListPrice


footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE FUNCTION [dbo].[ufnGetProductListPrice](@ProductID [int], @OrderDate [datetime])
RETURNS [money] 
AS 
BEGIN
    DECLARE @ListPrice money;

    SELECT @ListPrice = plph.[ListPrice] 
    FROM [Production].[Product] p 
        INNER JOIN [Production].[ProductListPriceHistory] plph 
        ON p.[ProductID] = plph.[ProductID] 
            AND p.[ProductID] = @ProductID 
            AND @OrderDate BETWEEN plph.[StartDate] AND COALESCE(plph.[EndDate], CONVERT(datetime, '99991231', 112)); -- Make sure we get all the prices!

    RETURN @ListPrice;
END;

----
=======
// end::sql_modules_definition[]


