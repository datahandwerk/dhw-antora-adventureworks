// tag::HeaderFullDisplayName[]
= Production.vProductModelInstructions - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
D5CBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Production.vProductModelInstructions
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
583673127
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-instructions]
=== Instructions

[cols="d,8a,m,m,m"]
|===
|
|Instructions
|nvarchar(max)
|NULL
|
|===


[#column-laborhours]
=== LaborHours

[cols="d,8a,m,m,m"]
|===
|
|LaborHours
|decimal(9, 4)
|NULL
|
|===


[#column-locationid]
=== LocationID

[cols="d,8a,m,m,m"]
|===
|
|LocationID
|int
|NULL
|
|===


[#column-lotsize]
=== LotSize

[cols="d,8a,m,m,m"]
|===
|
|LotSize
|int
|NULL
|
|===


[#column-machinehours]
=== MachineHours

[cols="d,8a,m,m,m"]
|===
|
|MachineHours
|decimal(9, 4)
|NULL
|
|===


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate
|datetime
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-modifieddate[+Production.ProductModel.ModifiedDate+]
--


[#column-name]
=== Name

[cols="d,8a,m,m,m"]
|===
|
|Name
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-name[+Production.ProductModel.Name+]
--


[#column-productmodelid]
=== ProductModelID

[cols="d,8a,m,m,m"]
|===
|
|ProductModelID
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-productmodelid[+Production.ProductModel.ProductModelID+]
--


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid
|uniqueidentifier
|NOT NULL
|
|===

.Referenced Columns
--
* xref:production.productmodel.adoc#column-rowguid[+Production.ProductModel.rowguid+]
--


[#column-setuphours]
=== SetupHours

[cols="d,8a,m,m,m"]
|===
|
|SetupHours
|decimal(9, 4)
|NULL
|
|===


[#column-step]
=== Step

[cols="d,8a,m,m,m"]
|===
|
|Step
|nvarchar(1024)
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]











// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-instructions>>
|nvarchar(max)
|NULL
|

|
|<<column-laborhours>>
|decimal(9, 4)
|NULL
|

|
|<<column-locationid>>
|int
|NULL
|

|
|<<column-lotsize>>
|int
|NULL
|

|
|<<column-machinehours>>
|decimal(9, 4)
|NULL
|

|
|<<column-modifieddate>>
|datetime
|NOT NULL
|

|
|<<column-name>>
|Name
|NOT NULL
|

|
|<<column-productmodelid>>
|int
|NOT NULL
|

|
|<<column-rowguid>>
|uniqueidentifier
|NOT NULL
|

|
|<<column-setuphours>>
|decimal(9, 4)
|NULL
|

|
|<<column-step>>
|nvarchar(1024)
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevproductmodelinstructionsunderlineunderline1]
=== idx_vProductModelInstructions++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-ProductModelID>>; int
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:production.vproductmodelinstructions.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:production.productmodel.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
  Instructions : (nvarchar(max))
  LaborHours : (decimal(9, 4))
  LocationID : (int)
  LotSize : (int)
  MachineHours : (decimal(9, 4))
  - ModifiedDate : (datetime)
  - Name : (Name)
  - ProductModelID : (int)
  - rowguid : (uniqueidentifier)
  SetupHours : (decimal(9, 4))
  Step : (nvarchar(1024))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
- idx_vProductModelInstructions__1

..
ProductModelID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
  --
}

Production.ProductModel <.. Production.vProductModelInstructions

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
  --
}

Production.ProductModel <.. Production.vProductModelInstructions

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodel.adoc[]" as Production.ProductModel << U >> {
  - **ProductModelID** : (int)
  CatalogDescription : (xml)
  Instructions : (xml)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:production.vproductmodelinstructions.adoc[]" as Production.vProductModelInstructions << V >> {
  Instructions : (nvarchar(max))
  LaborHours : (decimal(9, 4))
  LocationID : (int)
  LotSize : (int)
  MachineHours : (decimal(9, 4))
  - ModifiedDate : (datetime)
  - Name : (Name)
  - ProductModelID : (int)
  - rowguid : (uniqueidentifier)
  SetupHours : (decimal(9, 4))
  Step : (nvarchar(1024))
  --
}

Production.ProductModel <.. Production.vProductModelInstructions
"Production.ProductModel::ModifiedDate" <-- "Production.vProductModelInstructions::ModifiedDate"
"Production.ProductModel::Name" <-- "Production.vProductModelInstructions::Name"
"Production.ProductModel::ProductModelID" <-- "Production.vProductModelInstructions::ProductModelID"
"Production.ProductModel::rowguid" <-- "Production.vProductModelInstructions::rowguid"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE VIEW [Production].[vProductModelInstructions] 
AS 
SELECT 
    [ProductModelID] 
    ,[Name] 
    ,[Instructions].value(N'declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions"; 
        (/root/text())[1]', 'nvarchar(max)') AS [Instructions] 
    ,[MfgInstructions].ref.value('@LocationID[1]', 'int') AS [LocationID] 
    ,[MfgInstructions].ref.value('@SetupHours[1]', 'decimal(9, 4)') AS [SetupHours] 
    ,[MfgInstructions].ref.value('@MachineHours[1]', 'decimal(9, 4)') AS [MachineHours] 
    ,[MfgInstructions].ref.value('@LaborHours[1]', 'decimal(9, 4)') AS [LaborHours] 
    ,[MfgInstructions].ref.value('@LotSize[1]', 'int') AS [LotSize] 
    ,[Steps].ref.value('string(.)[1]', 'nvarchar(1024)') AS [Step] 
    ,[rowguid] 
    ,[ModifiedDate]
FROM [Production].[ProductModel] 
CROSS APPLY [Instructions].nodes(N'declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions"; 
    /root/Location') MfgInstructions(ref)
CROSS APPLY [MfgInstructions].ref.nodes('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions"; 
    step') Steps(ref);

----
=======
// end::sql_modules_definition[]


