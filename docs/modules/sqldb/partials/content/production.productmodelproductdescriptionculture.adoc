// tag::HeaderFullDisplayName[]
= Production.ProductModelProductDescriptionCulture - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
58CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Production.ProductModelProductDescriptionCulture
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1154103152
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-productmodelid]
=== ProductModelID

[cols="d,8a,m,m,m"]
|===
|1
|*ProductModelID*
|int
|NOT NULL
|
|===


[#column-productdescriptionid]
=== ProductDescriptionID

[cols="d,8a,m,m,m"]
|===
|2
|*ProductDescriptionID*
|int
|NOT NULL
|
|===


[#column-cultureid]
=== CultureID

[cols="d,8a,m,m,m"]
|===
|3
|*CultureID*
|nchar(6)
|NOT NULL
|
|===


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_ProductModelProductDescriptionCulture_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-productmodelid>>*
|int
|NOT NULL
|

|2
|*<<column-productdescriptionid>>*
|int
|NOT NULL
|

|3
|*<<column-cultureid>>*
|nchar(6)
|NOT NULL
|


// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]



|
|<<column-modifieddate>>

.Default: DF_ProductModelProductDescriptionCulture_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlineproductmodelproductdescriptioncultureunderlineproductmodelidunderlineproductdescriptionidunderlinecultureid]
=== PK_ProductModelProductDescriptionCulture_ProductModelID_ProductDescriptionID_CultureID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-ProductModelID>>; int
* <<column-ProductDescriptionID>>; int
* <<column-CultureID>>; nchar(6)
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:production.productmodelproductdescriptionculture.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:production.vproductanddescription.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
6ECBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int,int,nchar(6)
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
ProductModelID,ProductDescriptionID,CultureID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
  - **ProductModelID** : (int)
  - **ProductDescriptionID** : (int)
  - **CultureID** : (nchar(6))
  - ModifiedDate : (datetime)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
**PK_ProductModelProductDescriptionCulture_ProductModelID_ProductDescriptionID_CultureID**

..
ProductModelID; int
ProductDescriptionID; int
CultureID; nchar(6)
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
  - **ProductModelID** : (int)
  - **ProductDescriptionID** : (int)
  - **CultureID** : (nchar(6))
  --
}

entity "puml-link:aw:sqldb:production.vproductanddescription.adoc[]" as Production.vProductAndDescription << V >> {
  --
}

Production.ProductModelProductDescriptionCulture <.. Production.vProductAndDescription

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
  - **ProductModelID** : (int)
  - **ProductDescriptionID** : (int)
  - **CultureID** : (nchar(6))
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
  - **ProductModelID** : (int)
  - **ProductDescriptionID** : (int)
  - **CultureID** : (nchar(6))
  --
}

entity "puml-link:aw:sqldb:production.vproductanddescription.adoc[]" as Production.vProductAndDescription << V >> {
  --
}

Production.ProductModelProductDescriptionCulture <.. Production.vProductAndDescription

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:production.productmodelproductdescriptionculture.adoc[]" as Production.ProductModelProductDescriptionCulture << U >> {
  - **ProductModelID** : (int)
  - **ProductDescriptionID** : (int)
  - **CultureID** : (nchar(6))
  - ModifiedDate : (datetime)
  --
}

entity "puml-link:aw:sqldb:production.vproductanddescription.adoc[]" as Production.vProductAndDescription << V >> {
  - CultureID : (nchar(6))
  - Description : (nvarchar(400))
  - Name : (Name)
  - ProductID : (int)
  - ProductModel : (Name)
  --
}

Production.ProductModelProductDescriptionCulture <.. Production.vProductAndDescription


footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


