// tag::HeaderFullDisplayName[]
= HumanResources.Employee - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
11CDEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
HumanResources.Employee
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1893581784
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:14
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-businessentityid]
=== BusinessEntityID

[cols="d,8a,m,m,m"]
|===
|1
|*BusinessEntityID*
|int
|NOT NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-businessentityid[+HumanResources.vEmployee.BusinessEntityID+]
* xref:humanresources.vemployeedepartment.adoc#column-businessentityid[+HumanResources.vEmployeeDepartment.BusinessEntityID+]
* xref:humanresources.vemployeedepartmenthistory.adoc#column-businessentityid[+HumanResources.vEmployeeDepartmentHistory.BusinessEntityID+]
--


[#column-birthdate]
=== BirthDate

[cols="d,8a,m,m,m"]
|===
|
|BirthDate
|date
|NOT NULL
|
|===


[#column-currentflag]
=== CurrentFlag

[cols="d,8a,m,m,m"]
|===
|
|CurrentFlag

.Default: DF_Employee_CurrentFlag
[source,sql]
----
((1))
----


|Flag
|NOT NULL
|
|===


[#column-gender]
=== Gender

[cols="d,8a,m,m,m"]
|===
|
|Gender
|nchar(1)
|NOT NULL
|
|===


[#column-hiredate]
=== HireDate

[cols="d,8a,m,m,m"]
|===
|
|HireDate
|date
|NOT NULL
|
|===


[#column-jobtitle]
=== JobTitle

[cols="d,8a,m,m,m"]
|===
|
|JobTitle
|nvarchar(50)
|NOT NULL
|
|===

.Referencing Columns
--
* xref:humanresources.vemployee.adoc#column-jobtitle[+HumanResources.vEmployee.JobTitle+]
* xref:humanresources.vemployeedepartment.adoc#column-jobtitle[+HumanResources.vEmployeeDepartment.JobTitle+]
* xref:sales.vsalesperson.adoc#column-jobtitle[+Sales.vSalesPerson.JobTitle+]
--


[#column-loginid]
=== LoginID

[cols="d,8a,m,m,m"]
|===
|
|LoginID
|nvarchar(256)
|NOT NULL
|
|===


[#column-maritalstatus]
=== MaritalStatus

[cols="d,8a,m,m,m"]
|===
|
|MaritalStatus
|nchar(1)
|NOT NULL
|
|===


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_Employee_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-nationalidnumber]
=== NationalIDNumber

[cols="d,8a,m,m,m"]
|===
|
|NationalIDNumber
|nvarchar(15)
|NOT NULL
|
|===


[#column-organizationnode]
=== OrganizationNode

[cols="d,8a,m,m,m"]
|===
|
|OrganizationNode
|hierarchyid
|NULL
|
|===

.Referencing Columns
--
* xref:humanresources.employee.adoc#column-organizationlevel[+HumanResources.Employee.OrganizationLevel+]
--


[#column-rowguid]
=== rowguid

[cols="d,8a,m,m,m"]
|===
|
|rowguid

.Default: DF_Employee_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|
|===


[#column-salariedflag]
=== SalariedFlag

[cols="d,8a,m,m,m"]
|===
|
|SalariedFlag

.Default: DF_Employee_SalariedFlag
[source,sql]
----
((1))
----


|Flag
|NOT NULL
|
|===


[#column-sickleavehours]
=== SickLeaveHours

[cols="d,8a,m,m,m"]
|===
|
|SickLeaveHours

.Default: DF_Employee_SickLeaveHours
[source,sql]
----
((0))
----


|smallint
|NOT NULL
|
|===


[#column-vacationhours]
=== VacationHours

[cols="d,8a,m,m,m"]
|===
|
|VacationHours

.Default: DF_Employee_VacationHours
[source,sql]
----
((0))
----


|smallint
|NOT NULL
|
|===


[#column-organizationlevel]
=== OrganizationLevel

[cols="d,8a,m,m,m"]
|===
|
|OrganizationLevel

.Definition
[source,sql]
----
([OrganizationNode].[GetLevel]())
----


|smallint
|NULL
|
|===

.Referenced Columns
--
* xref:humanresources.employee.adoc#column-organizationnode[+HumanResources.Employee.OrganizationNode+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-businessentityid>>*
|int
|NOT NULL
|
















// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-birthdate>>
|date
|NOT NULL
|

|
|<<column-currentflag>>

.Default: DF_Employee_CurrentFlag
[source,sql]
----
((1))
----


|Flag
|NOT NULL
|

|
|<<column-gender>>
|nchar(1)
|NOT NULL
|

|
|<<column-hiredate>>
|date
|NOT NULL
|

|
|<<column-jobtitle>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-loginid>>
|nvarchar(256)
|NOT NULL
|

|
|<<column-maritalstatus>>
|nchar(1)
|NOT NULL
|

|
|<<column-modifieddate>>

.Default: DF_Employee_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-nationalidnumber>>
|nvarchar(15)
|NOT NULL
|

|
|<<column-organizationnode>>
|hierarchyid
|NULL
|

|
|<<column-rowguid>>

.Default: DF_Employee_rowguid
[source,sql]
----
(newid())
----


|uniqueidentifier
|NOT NULL
|

|
|<<column-salariedflag>>

.Default: DF_Employee_SalariedFlag
[source,sql]
----
((1))
----


|Flag
|NOT NULL
|

|
|<<column-sickleavehours>>

.Default: DF_Employee_SickLeaveHours
[source,sql]
----
((0))
----


|smallint
|NOT NULL
|

|
|<<column-vacationhours>>

.Default: DF_Employee_VacationHours
[source,sql]
----
((0))
----


|smallint
|NOT NULL
|

|
|<<column-organizationlevel>>

.Definition
[source,sql]
----
([OrganizationNode].[GetLevel]())
----


|smallint
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlineemployeeunderlinebusinessentityid]
=== PK_Employee_BusinessEntityID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-BusinessEntityID>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:humanresources.employee.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:dbo.ufngetcontactinformation.adoc[]
* xref:aw:sqldb:dbo.uspgetemployeemanagers.adoc[]
* xref:aw:sqldb:dbo.uspgetmanageremployees.adoc[]
* xref:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]
* xref:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]
* xref:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]
* xref:aw:sqldb:humanresources.vemployee.adoc[]
* xref:aw:sqldb:humanresources.vemployeedepartment.adoc[]
* xref:aw:sqldb:humanresources.vemployeedepartmenthistory.adoc[]
* xref:aw:sqldb:sales.vsalesperson.adoc[]
* xref:aw:sqldb:sales.vsalespersonsalesbyfiscalyears.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
2CCDEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
BusinessEntityID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  - BirthDate : (date)
  - CurrentFlag : (Flag)
  - Gender : (nchar(1))
  - HireDate : (date)
  - JobTitle : (nvarchar(50))
  - LoginID : (nvarchar(256))
  - MaritalStatus : (nchar(1))
  - ModifiedDate : (datetime)
  - NationalIDNumber : (nvarchar(15))
  OrganizationNode : (hierarchyid)
  - rowguid : (uniqueidentifier)
  - SalariedFlag : (Flag)
  - SickLeaveHours : (smallint)
  - VacationHours : (smallint)
  ~ OrganizationLevel : (smallint)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
**PK_Employee_BusinessEntityID**

..
BusinessEntityID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetemployeemanagers.adoc[]" as dbo.uspGetEmployeeManagers << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetmanageremployees.adoc[]" as dbo.uspGetManagerEmployees << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartment.adoc[]" as HumanResources.vEmployeeDepartment << V >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartmenthistory.adoc[]" as HumanResources.vEmployeeDepartmentHistory << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalespersonsalesbyfiscalyears.adoc[]" as Sales.vSalesPersonSalesByFiscalYears << V >> {
  --
}

HumanResources.Employee <.. dbo.ufnGetContactInformation
HumanResources.Employee <.. dbo.uspGetEmployeeManagers
HumanResources.Employee <.. dbo.uspGetManagerEmployees
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeLogin
HumanResources.Employee <.. HumanResources.uspUpdateEmployeePersonalInfo
HumanResources.Employee <.. HumanResources.vEmployee
HumanResources.Employee <.. HumanResources.vEmployeeDepartment
HumanResources.Employee <.. HumanResources.vEmployeeDepartmentHistory
HumanResources.Employee <.. Sales.vSalesPerson
HumanResources.Employee <.. Sales.vSalesPersonSalesByFiscalYears

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetemployeemanagers.adoc[]" as dbo.uspGetEmployeeManagers << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetmanageremployees.adoc[]" as dbo.uspGetManagerEmployees << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartment.adoc[]" as HumanResources.vEmployeeDepartment << V >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartmenthistory.adoc[]" as HumanResources.vEmployeeDepartmentHistory << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  --
}

entity "puml-link:aw:sqldb:sales.vsalespersonsalesbyfiscalyears.adoc[]" as Sales.vSalesPersonSalesByFiscalYears << V >> {
  --
}

HumanResources.Employee <.. dbo.ufnGetContactInformation
HumanResources.Employee <.. dbo.uspGetEmployeeManagers
HumanResources.Employee <.. dbo.uspGetManagerEmployees
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeLogin
HumanResources.Employee <.. HumanResources.uspUpdateEmployeePersonalInfo
HumanResources.Employee <.. HumanResources.vEmployee
HumanResources.Employee <.. HumanResources.vEmployeeDepartment
HumanResources.Employee <.. HumanResources.vEmployeeDepartmentHistory
HumanResources.Employee <.. Sales.vSalesPerson
HumanResources.Employee <.. Sales.vSalesPersonSalesByFiscalYears

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.ufngetcontactinformation.adoc[]" as dbo.ufnGetContactInformation << TF >> {
  BusinessEntityType : (nvarchar(50))
  FirstName : (nvarchar(50))
  JobTitle : (nvarchar(50))
  LastName : (nvarchar(50))
  - PersonID : (int)
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetemployeemanagers.adoc[]" as dbo.uspGetEmployeeManagers << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspgetmanageremployees.adoc[]" as dbo.uspGetManagerEmployees << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.employee.adoc[]" as HumanResources.Employee << U >> {
  - **BusinessEntityID** : (int)
  - BirthDate : (date)
  - CurrentFlag : (Flag)
  - Gender : (nchar(1))
  - HireDate : (date)
  - JobTitle : (nvarchar(50))
  - LoginID : (nvarchar(256))
  - MaritalStatus : (nchar(1))
  - ModifiedDate : (datetime)
  - NationalIDNumber : (nvarchar(15))
  OrganizationNode : (hierarchyid)
  - rowguid : (uniqueidentifier)
  - SalariedFlag : (Flag)
  - SickLeaveHours : (smallint)
  - VacationHours : (smallint)
  ~ OrganizationLevel : (smallint)
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployee.adoc[]" as HumanResources.vEmployee << V >> {
  AdditionalContactInfo : (xml)
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - JobTitle : (nvarchar(50))
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartment.adoc[]" as HumanResources.vEmployeeDepartment << V >> {
  - BusinessEntityID : (int)
  - Department : (Name)
  - FirstName : (Name)
  - GroupName : (Name)
  - JobTitle : (nvarchar(50))
  - LastName : (Name)
  MiddleName : (Name)
  - StartDate : (date)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:humanresources.vemployeedepartmenthistory.adoc[]" as HumanResources.vEmployeeDepartmentHistory << V >> {
  - BusinessEntityID : (int)
  - Department : (Name)
  EndDate : (date)
  - FirstName : (Name)
  - GroupName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  - Shift : (Name)
  - StartDate : (date)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:sales.vsalesperson.adoc[]" as Sales.vSalesPerson << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - JobTitle : (nvarchar(50))
  - LastName : (Name)
  MiddleName : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  - PostalCode : (nvarchar(15))
  - SalesLastYear : (money)
  SalesQuota : (money)
  - SalesYTD : (money)
  - StateProvinceName : (Name)
  Suffix : (nvarchar(10))
  TerritoryGroup : (nvarchar(50))
  TerritoryName : (Name)
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:sales.vsalespersonsalesbyfiscalyears.adoc[]" as Sales.vSalesPersonSalesByFiscalYears << V >> {
  2002 : (money)
  2003 : (money)
  2004 : (money)
  FullName : (nvarchar(152))
  - JobTitle : (nvarchar(50))
  SalesPersonID : (int)
  - SalesTerritory : (Name)
  --
}

HumanResources.Employee <.. dbo.ufnGetContactInformation
HumanResources.Employee <.. dbo.uspGetEmployeeManagers
HumanResources.Employee <.. dbo.uspGetManagerEmployees
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeHireInfo
HumanResources.Employee <.. HumanResources.uspUpdateEmployeeLogin
HumanResources.Employee <.. HumanResources.uspUpdateEmployeePersonalInfo
HumanResources.Employee <.. HumanResources.vEmployee
HumanResources.Employee <.. HumanResources.vEmployeeDepartment
HumanResources.Employee <.. HumanResources.vEmployeeDepartmentHistory
HumanResources.Employee <.. Sales.vSalesPerson
HumanResources.Employee <.. Sales.vSalesPersonSalesByFiscalYears
"HumanResources.Employee::BusinessEntityID" <-- "HumanResources.vEmployee::BusinessEntityID"
"HumanResources.Employee::BusinessEntityID" <-- "HumanResources.vEmployeeDepartment::BusinessEntityID"
"HumanResources.Employee::BusinessEntityID" <-- "HumanResources.vEmployeeDepartmentHistory::BusinessEntityID"
"HumanResources.Employee::JobTitle" <-- "HumanResources.vEmployee::JobTitle"
"HumanResources.Employee::JobTitle" <-- "HumanResources.vEmployeeDepartment::JobTitle"
"HumanResources.Employee::JobTitle" <-- "Sales.vSalesPerson::JobTitle"
"HumanResources.Employee::OrganizationNode" <-- "HumanResources.Employee::OrganizationLevel"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


