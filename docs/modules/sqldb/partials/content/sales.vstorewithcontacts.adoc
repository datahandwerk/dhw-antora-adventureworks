// tag::HeaderFullDisplayName[]
= Sales.vStoreWithContacts - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
E9CBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Sales.vStoreWithContacts
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
663673412
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-businessentityid]
=== BusinessEntityID

[cols="d,8a,m,m,m"]
|===
|
|BusinessEntityID
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.store.adoc#column-businessentityid[+Sales.Store.BusinessEntityID+]
--


[#column-contacttype]
=== ContactType

[cols="d,8a,m,m,m"]
|===
|
|ContactType
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.contacttype.adoc#column-name[+Person.ContactType.Name+]
--


[#column-emailaddress]
=== EmailAddress

[cols="d,8a,m,m,m"]
|===
|
|EmailAddress
|nvarchar(50)
|NULL
|
|===

.Referenced Columns
--
* xref:person.emailaddress.adoc#column-emailaddress[+Person.EmailAddress.EmailAddress+]
--


[#column-emailpromotion]
=== EmailPromotion

[cols="d,8a,m,m,m"]
|===
|
|EmailPromotion
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-emailpromotion[+Person.Person.EmailPromotion+]
--


[#column-firstname]
=== FirstName

[cols="d,8a,m,m,m"]
|===
|
|FirstName
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-firstname[+Person.Person.FirstName+]
--


[#column-lastname]
=== LastName

[cols="d,8a,m,m,m"]
|===
|
|LastName
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-lastname[+Person.Person.LastName+]
--


[#column-middlename]
=== MiddleName

[cols="d,8a,m,m,m"]
|===
|
|MiddleName
|Name
|NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-middlename[+Person.Person.MiddleName+]
--


[#column-name]
=== Name

[cols="d,8a,m,m,m"]
|===
|
|Name
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.store.adoc#column-name[+Sales.Store.Name+]
--


[#column-phonenumber]
=== PhoneNumber

[cols="d,8a,m,m,m"]
|===
|
|PhoneNumber
|Phone
|NULL
|
|===

.Referenced Columns
--
* xref:person.personphone.adoc#column-phonenumber[+Person.PersonPhone.PhoneNumber+]
--


[#column-phonenumbertype]
=== PhoneNumberType

[cols="d,8a,m,m,m"]
|===
|
|PhoneNumberType
|Name
|NULL
|
|===

.Referenced Columns
--
* xref:person.phonenumbertype.adoc#column-name[+Person.PhoneNumberType.Name+]
--


[#column-suffix]
=== Suffix

[cols="d,8a,m,m,m"]
|===
|
|Suffix
|nvarchar(10)
|NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-suffix[+Person.Person.Suffix+]
--


[#column-title]
=== Title

[cols="d,8a,m,m,m"]
|===
|
|Title
|nvarchar(8)
|NULL
|
|===

.Referenced Columns
--
* xref:person.person.adoc#column-title[+Person.Person.Title+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]












// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-businessentityid>>
|int
|NOT NULL
|

|
|<<column-contacttype>>
|Name
|NOT NULL
|

|
|<<column-emailaddress>>
|nvarchar(50)
|NULL
|

|
|<<column-emailpromotion>>
|int
|NOT NULL
|

|
|<<column-firstname>>
|Name
|NOT NULL
|

|
|<<column-lastname>>
|Name
|NOT NULL
|

|
|<<column-middlename>>
|Name
|NULL
|

|
|<<column-name>>
|Name
|NOT NULL
|

|
|<<column-phonenumber>>
|Phone
|NULL
|

|
|<<column-phonenumbertype>>
|Name
|NULL
|

|
|<<column-suffix>>
|nvarchar(10)
|NULL
|

|
|<<column-title>>
|nvarchar(8)
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevstorewithcontactsunderlineunderline1]
=== idx_vStoreWithContacts++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-BusinessEntityID>>; int
--
* PK, Unique, Real: 0, 0, 0


[#index-idxunderlinevstorewithcontactsunderlineunderline2]
=== idx_vStoreWithContacts++__++2

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-PhoneNumber>>; Phone
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:sales.vstorewithcontacts.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:person.businessentitycontact.adoc[]
* xref:aw:sqldb:person.contacttype.adoc[]
* xref:aw:sqldb:person.emailaddress.adoc[]
* xref:aw:sqldb:person.person.adoc[]
* xref:aw:sqldb:person.personphone.adoc[]
* xref:aw:sqldb:person.phonenumbertype.adoc[]
* xref:aw:sqldb:sales.store.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
  - BusinessEntityID : (int)
  - ContactType : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  - Name : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
- idx_vStoreWithContacts__1

..
BusinessEntityID; int
--
- idx_vStoreWithContacts__2

..
PhoneNumber; Phone
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.businessentitycontact.adoc[]" as Person.BusinessEntityContact << U >> {
  - **BusinessEntityID** : (int)
  - **PersonID** : (int)
  - **ContactTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.contacttype.adoc[]" as Person.ContactType << U >> {
  - **ContactTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.emailaddress.adoc[]" as Person.EmailAddress << U >> {
  - **BusinessEntityID** : (int)
  - **EmailAddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.person.adoc[]" as Person.Person << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.personphone.adoc[]" as Person.PersonPhone << U >> {
  - **BusinessEntityID** : (int)
  - **PhoneNumber** : (Phone)
  - **PhoneNumberTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.phonenumbertype.adoc[]" as Person.PhoneNumberType << U >> {
  - **PhoneNumberTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
  --
}

Person.BusinessEntityContact <.. Sales.vStoreWithContacts
Person.ContactType <.. Sales.vStoreWithContacts
Person.EmailAddress <.. Sales.vStoreWithContacts
Person.Person <.. Sales.vStoreWithContacts
Person.PersonPhone <.. Sales.vStoreWithContacts
Person.PhoneNumberType <.. Sales.vStoreWithContacts
Sales.Store <.. Sales.vStoreWithContacts

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.businessentitycontact.adoc[]" as Person.BusinessEntityContact << U >> {
  - **BusinessEntityID** : (int)
  - **PersonID** : (int)
  - **ContactTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.contacttype.adoc[]" as Person.ContactType << U >> {
  - **ContactTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.emailaddress.adoc[]" as Person.EmailAddress << U >> {
  - **BusinessEntityID** : (int)
  - **EmailAddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.person.adoc[]" as Person.Person << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.personphone.adoc[]" as Person.PersonPhone << U >> {
  - **BusinessEntityID** : (int)
  - **PhoneNumber** : (Phone)
  - **PhoneNumberTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.phonenumbertype.adoc[]" as Person.PhoneNumberType << U >> {
  - **PhoneNumberTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
  --
}

Person.BusinessEntityContact <.. Sales.vStoreWithContacts
Person.ContactType <.. Sales.vStoreWithContacts
Person.EmailAddress <.. Sales.vStoreWithContacts
Person.Person <.. Sales.vStoreWithContacts
Person.PersonPhone <.. Sales.vStoreWithContacts
Person.PhoneNumberType <.. Sales.vStoreWithContacts
Sales.Store <.. Sales.vStoreWithContacts

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.businessentitycontact.adoc[]" as Person.BusinessEntityContact << U >> {
  - **BusinessEntityID** : (int)
  - **PersonID** : (int)
  - **ContactTypeID** : (int)
  - ModifiedDate : (datetime)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:person.contacttype.adoc[]" as Person.ContactType << U >> {
  - **ContactTypeID** : (int)
  - ModifiedDate : (datetime)
  - Name : (Name)
  --
}

entity "puml-link:aw:sqldb:person.emailaddress.adoc[]" as Person.EmailAddress << U >> {
  - **BusinessEntityID** : (int)
  - **EmailAddressID** : (int)
  EmailAddress : (nvarchar(50))
  - ModifiedDate : (datetime)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:person.person.adoc[]" as Person.Person << U >> {
  - **BusinessEntityID** : (int)
  AdditionalContactInfo : (xml)
  Demographics : (xml)
  - EmailPromotion : (int)
  - FirstName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  - ModifiedDate : (datetime)
  - NameStyle : (NameStyle)
  - PersonType : (nchar(2))
  - rowguid : (uniqueidentifier)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

entity "puml-link:aw:sqldb:person.personphone.adoc[]" as Person.PersonPhone << U >> {
  - **BusinessEntityID** : (int)
  - **PhoneNumber** : (Phone)
  - **PhoneNumberTypeID** : (int)
  - ModifiedDate : (datetime)
  --
}

entity "puml-link:aw:sqldb:person.phonenumbertype.adoc[]" as Person.PhoneNumberType << U >> {
  - **PhoneNumberTypeID** : (int)
  - ModifiedDate : (datetime)
  - Name : (Name)
  --
}

entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  Demographics : (xml)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  SalesPersonID : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithcontacts.adoc[]" as Sales.vStoreWithContacts << V >> {
  - BusinessEntityID : (int)
  - ContactType : (Name)
  EmailAddress : (nvarchar(50))
  - EmailPromotion : (int)
  - FirstName : (Name)
  - LastName : (Name)
  MiddleName : (Name)
  - Name : (Name)
  PhoneNumber : (Phone)
  PhoneNumberType : (Name)
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  --
}

Person.BusinessEntityContact <.. Sales.vStoreWithContacts
Person.ContactType <.. Sales.vStoreWithContacts
Person.EmailAddress <.. Sales.vStoreWithContacts
Person.Person <.. Sales.vStoreWithContacts
Person.PersonPhone <.. Sales.vStoreWithContacts
Person.PhoneNumberType <.. Sales.vStoreWithContacts
Sales.Store <.. Sales.vStoreWithContacts
"Person.ContactType::Name" <-- "Sales.vStoreWithContacts::ContactType"
"Person.EmailAddress::EmailAddress" <-- "Sales.vStoreWithContacts::EmailAddress"
"Person.Person::EmailPromotion" <-- "Sales.vStoreWithContacts::EmailPromotion"
"Person.Person::FirstName" <-- "Sales.vStoreWithContacts::FirstName"
"Person.Person::LastName" <-- "Sales.vStoreWithContacts::LastName"
"Person.Person::MiddleName" <-- "Sales.vStoreWithContacts::MiddleName"
"Person.Person::Suffix" <-- "Sales.vStoreWithContacts::Suffix"
"Person.Person::Title" <-- "Sales.vStoreWithContacts::Title"
"Person.PersonPhone::PhoneNumber" <-- "Sales.vStoreWithContacts::PhoneNumber"
"Person.PhoneNumberType::Name" <-- "Sales.vStoreWithContacts::PhoneNumberType"
"Sales.Store::BusinessEntityID" <-- "Sales.vStoreWithContacts::BusinessEntityID"
"Sales.Store::Name" <-- "Sales.vStoreWithContacts::Name"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE VIEW [Sales].[vStoreWithContacts] AS 
SELECT 
    s.[BusinessEntityID] 
    ,s.[Name] 
    ,ct.[Name] AS [ContactType] 
    ,p.[Title] 
    ,p.[FirstName] 
    ,p.[MiddleName] 
    ,p.[LastName] 
    ,p.[Suffix] 
    ,pp.[PhoneNumber] 
	,pnt.[Name] AS [PhoneNumberType]
    ,ea.[EmailAddress] 
    ,p.[EmailPromotion] 
FROM [Sales].[Store] s
    INNER JOIN [Person].[BusinessEntityContact] bec 
    ON bec.[BusinessEntityID] = s.[BusinessEntityID]
	INNER JOIN [Person].[ContactType] ct
	ON ct.[ContactTypeID] = bec.[ContactTypeID]
	INNER JOIN [Person].[Person] p
	ON p.[BusinessEntityID] = bec.[PersonID]
	LEFT OUTER JOIN [Person].[EmailAddress] ea
	ON ea.[BusinessEntityID] = p.[BusinessEntityID]
	LEFT OUTER JOIN [Person].[PersonPhone] pp
	ON pp.[BusinessEntityID] = p.[BusinessEntityID]
	LEFT OUTER JOIN [Person].[PhoneNumberType] pnt
	ON pnt.[PhoneNumberTypeID] = pp.[PhoneNumberTypeID];

----
=======
// end::sql_modules_definition[]


