// tag::HeaderFullDisplayName[]
= dbo.uspLogError - P
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
30CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.uspLogError
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
P 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
stored procedure
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
997578592
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:01
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]

// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]

// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:dbo.usplogerror.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:dbo.errorlog.adoc[]
* xref:aw:sqldb:dbo.uspprinterror.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]
* xref:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]
* xref:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]
* xref:aw:sqldb:production.iworkorder.adoc[]
* xref:aw:sqldb:production.uworkorder.adoc[]
* xref:aw:sqldb:purchasing.dvendor.adoc[]
* xref:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]
* xref:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]
* xref:aw:sqldb:purchasing.upurchaseorderheader.adoc[]
* xref:aw:sqldb:sales.idusalesorderdetail.adoc[]
* xref:aw:sqldb:sales.usalesorderheader.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}





footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.errorlog.adoc[]" as dbo.ErrorLog << U >> {
  - **ErrorLogID** : (int)
  --
}

entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspprinterror.adoc[]" as dbo.uspPrintError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:production.iworkorder.adoc[]" as Production.iWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:production.uworkorder.adoc[]" as Production.uWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.dvendor.adoc[]" as Purchasing.dVendor << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.usalesorderheader.adoc[]" as Sales.uSalesOrderHeader << TR >> {
  --
}

dbo.ErrorLog <.. dbo.uspLogError
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeLogin
dbo.uspLogError <.. HumanResources.uspUpdateEmployeePersonalInfo
dbo.uspLogError <.. Production.iWorkOrder
dbo.uspLogError <.. Production.uWorkOrder
dbo.uspLogError <.. Purchasing.dVendor
dbo.uspLogError <.. Purchasing.iPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderHeader
dbo.uspLogError <.. Sales.iduSalesOrderDetail
dbo.uspLogError <.. Sales.uSalesOrderHeader
dbo.uspPrintError <.. dbo.uspLogError

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.errorlog.adoc[]" as dbo.ErrorLog << U >> {
  - **ErrorLogID** : (int)
  --
}

entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspprinterror.adoc[]" as dbo.uspPrintError << P >> {
  --
}

dbo.ErrorLog <.. dbo.uspLogError
dbo.uspPrintError <.. dbo.uspLogError

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:production.iworkorder.adoc[]" as Production.iWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:production.uworkorder.adoc[]" as Production.uWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.dvendor.adoc[]" as Purchasing.dVendor << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.usalesorderheader.adoc[]" as Sales.uSalesOrderHeader << TR >> {
  --
}

dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeLogin
dbo.uspLogError <.. HumanResources.uspUpdateEmployeePersonalInfo
dbo.uspLogError <.. Production.iWorkOrder
dbo.uspLogError <.. Production.uWorkOrder
dbo.uspLogError <.. Purchasing.dVendor
dbo.uspLogError <.. Purchasing.iPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderHeader
dbo.uspLogError <.. Sales.iduSalesOrderDetail
dbo.uspLogError <.. Sales.uSalesOrderHeader

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:dbo.errorlog.adoc[]" as dbo.ErrorLog << U >> {
  - **ErrorLogID** : (int)
  ErrorLine : (int)
  - ErrorMessage : (nvarchar(4000))
  - ErrorNumber : (int)
  ErrorProcedure : (nvarchar(126))
  ErrorSeverity : (int)
  ErrorState : (int)
  - ErrorTime : (datetime)
  - UserName : (sysname)
  --
}

entity "puml-link:aw:sqldb:dbo.usplogerror.adoc[]" as dbo.uspLogError << P >> {
  --
}

entity "puml-link:aw:sqldb:dbo.uspprinterror.adoc[]" as dbo.uspPrintError << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeehireinfo.adoc[]" as HumanResources.uspUpdateEmployeeHireInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeelogin.adoc[]" as HumanResources.uspUpdateEmployeeLogin << P >> {
  --
}

entity "puml-link:aw:sqldb:humanresources.uspupdateemployeepersonalinfo.adoc[]" as HumanResources.uspUpdateEmployeePersonalInfo << P >> {
  --
}

entity "puml-link:aw:sqldb:production.iworkorder.adoc[]" as Production.iWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:production.uworkorder.adoc[]" as Production.uWorkOrder << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.dvendor.adoc[]" as Purchasing.dVendor << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.idusalesorderdetail.adoc[]" as Sales.iduSalesOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:sales.usalesorderheader.adoc[]" as Sales.uSalesOrderHeader << TR >> {
  --
}

dbo.ErrorLog <.. dbo.uspLogError
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeHireInfo
dbo.uspLogError <.. HumanResources.uspUpdateEmployeeLogin
dbo.uspLogError <.. HumanResources.uspUpdateEmployeePersonalInfo
dbo.uspLogError <.. Production.iWorkOrder
dbo.uspLogError <.. Production.uWorkOrder
dbo.uspLogError <.. Purchasing.dVendor
dbo.uspLogError <.. Purchasing.iPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderDetail
dbo.uspLogError <.. Purchasing.uPurchaseOrderHeader
dbo.uspLogError <.. Sales.iduSalesOrderDetail
dbo.uspLogError <.. Sales.uSalesOrderHeader
dbo.uspPrintError <.. dbo.uspLogError


footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

-- uspLogError logs error information in the ErrorLog table about the 
-- error that caused execution to jump to the CATCH block of a 
-- TRY...CATCH construct. This should be executed from within the scope 
-- of a CATCH block otherwise it will return without inserting error 
-- information. 
CREATE PROCEDURE [dbo].[uspLogError] 
    @ErrorLogID [int] = 0 OUTPUT -- contains the ErrorLogID of the row inserted
AS                               -- by uspLogError in the ErrorLog table
BEGIN
    SET NOCOUNT ON;

    -- Output parameter value of 0 indicates that error 
    -- information was not logged
    SET @ErrorLogID = 0;

    BEGIN TRY
        -- Return if there is no error information to log
        IF ERROR_NUMBER() IS NULL
            RETURN;

        -- Return if inside an uncommittable transaction.
        -- Data insertion/modification is not allowed when 
        -- a transaction is in an uncommittable state.
        IF XACT_STATE() = -1
        BEGIN
            PRINT 'Cannot log error since the current transaction is in an uncommittable state. ' 
                + 'Rollback the transaction before executing uspLogError in order to successfully log error information.';
            RETURN;
        END

        INSERT [dbo].[ErrorLog] 
            (
            [UserName], 
            [ErrorNumber], 
            [ErrorSeverity], 
            [ErrorState], 
            [ErrorProcedure], 
            [ErrorLine], 
            [ErrorMessage]
            ) 
        VALUES 
            (
            CONVERT(sysname, CURRENT_USER), 
            ERROR_NUMBER(),
            ERROR_SEVERITY(),
            ERROR_STATE(),
            ERROR_PROCEDURE(),
            ERROR_LINE(),
            ERROR_MESSAGE()
            );

        -- Pass back the ErrorLogID of the row inserted
        SET @ErrorLogID = @@IDENTITY;
    END TRY
    BEGIN CATCH
        PRINT 'An error occurred in stored procedure uspLogError: ';
        EXECUTE [dbo].[uspPrintError];
        RETURN -1;
    END CATCH
END;

----
=======
// end::sql_modules_definition[]


