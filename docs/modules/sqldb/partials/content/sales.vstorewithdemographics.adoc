// tag::HeaderFullDisplayName[]
= Sales.vStoreWithDemographics - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
E5CBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Sales.vStoreWithDemographics
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
647673355
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-annualrevenue]
=== AnnualRevenue

[cols="d,8a,m,m,m"]
|===
|
|AnnualRevenue
|money
|NULL
|
|===


[#column-annualsales]
=== AnnualSales

[cols="d,8a,m,m,m"]
|===
|
|AnnualSales
|money
|NULL
|
|===


[#column-bankname]
=== BankName

[cols="d,8a,m,m,m"]
|===
|
|BankName
|nvarchar(50)
|NULL
|
|===


[#column-brands]
=== Brands

[cols="d,8a,m,m,m"]
|===
|
|Brands
|nvarchar(30)
|NULL
|
|===


[#column-businessentityid]
=== BusinessEntityID

[cols="d,8a,m,m,m"]
|===
|
|BusinessEntityID
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.store.adoc#column-businessentityid[+Sales.Store.BusinessEntityID+]
--


[#column-businesstype]
=== BusinessType

[cols="d,8a,m,m,m"]
|===
|
|BusinessType
|nvarchar(5)
|NULL
|
|===


[#column-internet]
=== Internet

[cols="d,8a,m,m,m"]
|===
|
|Internet
|nvarchar(30)
|NULL
|
|===


[#column-name]
=== Name

[cols="d,8a,m,m,m"]
|===
|
|Name
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:sales.store.adoc#column-name[+Sales.Store.Name+]
--


[#column-numberemployees]
=== NumberEmployees

[cols="d,8a,m,m,m"]
|===
|
|NumberEmployees
|int
|NULL
|
|===


[#column-specialty]
=== Specialty

[cols="d,8a,m,m,m"]
|===
|
|Specialty
|nvarchar(50)
|NULL
|
|===


[#column-squarefeet]
=== SquareFeet

[cols="d,8a,m,m,m"]
|===
|
|SquareFeet
|int
|NULL
|
|===


[#column-yearopened]
=== YearOpened

[cols="d,8a,m,m,m"]
|===
|
|YearOpened
|int
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]












// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-annualrevenue>>
|money
|NULL
|

|
|<<column-annualsales>>
|money
|NULL
|

|
|<<column-bankname>>
|nvarchar(50)
|NULL
|

|
|<<column-brands>>
|nvarchar(30)
|NULL
|

|
|<<column-businessentityid>>
|int
|NOT NULL
|

|
|<<column-businesstype>>
|nvarchar(5)
|NULL
|

|
|<<column-internet>>
|nvarchar(30)
|NULL
|

|
|<<column-name>>
|Name
|NOT NULL
|

|
|<<column-numberemployees>>
|int
|NULL
|

|
|<<column-specialty>>
|nvarchar(50)
|NULL
|

|
|<<column-squarefeet>>
|int
|NULL
|

|
|<<column-yearopened>>
|int
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevstorewithdemographicsunderlineunderline1]
=== idx_vStoreWithDemographics++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-BusinessEntityID>>; int
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:sales.vstorewithdemographics.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:sales.store.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
  AnnualRevenue : (money)
  AnnualSales : (money)
  BankName : (nvarchar(50))
  Brands : (nvarchar(30))
  - BusinessEntityID : (int)
  BusinessType : (nvarchar(5))
  Internet : (nvarchar(30))
  - Name : (Name)
  NumberEmployees : (int)
  Specialty : (nvarchar(50))
  SquareFeet : (int)
  YearOpened : (int)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
- idx_vStoreWithDemographics__1

..
BusinessEntityID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
  --
}

Sales.Store <.. Sales.vStoreWithDemographics

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
  --
}

Sales.Store <.. Sales.vStoreWithDemographics

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:sales.store.adoc[]" as Sales.Store << U >> {
  - **BusinessEntityID** : (int)
  Demographics : (xml)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  SalesPersonID : (int)
  --
}

entity "puml-link:aw:sqldb:sales.vstorewithdemographics.adoc[]" as Sales.vStoreWithDemographics << V >> {
  AnnualRevenue : (money)
  AnnualSales : (money)
  BankName : (nvarchar(50))
  Brands : (nvarchar(30))
  - BusinessEntityID : (int)
  BusinessType : (nvarchar(5))
  Internet : (nvarchar(30))
  - Name : (Name)
  NumberEmployees : (int)
  Specialty : (nvarchar(50))
  SquareFeet : (int)
  YearOpened : (int)
  --
}

Sales.Store <.. Sales.vStoreWithDemographics
"Sales.Store::BusinessEntityID" <-- "Sales.vStoreWithDemographics::BusinessEntityID"
"Sales.Store::Name" <-- "Sales.vStoreWithDemographics::Name"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE VIEW [Sales].[vStoreWithDemographics] AS 
SELECT 
    s.[BusinessEntityID] 
    ,s.[Name] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/AnnualSales)[1]', 'money') AS [AnnualSales] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/AnnualRevenue)[1]', 'money') AS [AnnualRevenue] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/BankName)[1]', 'nvarchar(50)') AS [BankName] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/BusinessType)[1]', 'nvarchar(5)') AS [BusinessType] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/YearOpened)[1]', 'integer') AS [YearOpened] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/Specialty)[1]', 'nvarchar(50)') AS [Specialty] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/SquareFeet)[1]', 'integer') AS [SquareFeet] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/Brands)[1]', 'nvarchar(30)') AS [Brands] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/Internet)[1]', 'nvarchar(30)') AS [Internet] 
    ,s.[Demographics].value('declare default element namespace "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/StoreSurvey"; 
        (/StoreSurvey/NumberEmployees)[1]', 'integer') AS [NumberEmployees] 
FROM [Sales].[Store] s;

----
=======
// end::sql_modules_definition[]


