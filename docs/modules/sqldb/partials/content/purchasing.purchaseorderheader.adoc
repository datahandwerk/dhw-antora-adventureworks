// tag::HeaderFullDisplayName[]
= Purchasing.PurchaseOrderHeader - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
C8CCEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Purchasing.PurchaseOrderHeader
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1602104748
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:14
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-purchaseorderid]
=== PurchaseOrderID

[cols="d,8a,m,m,m"]
|===
|1
|*PurchaseOrderID*
|int
|NOT NULL
|(1,1)
|===


[#column-employeeid]
=== EmployeeID

[cols="d,8a,m,m,m"]
|===
|
|EmployeeID
|int
|NOT NULL
|
|===


[#column-freight]
=== Freight

[cols="d,8a,m,m,m"]
|===
|
|Freight

.Default: DF_PurchaseOrderHeader_Freight
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|
|===

.Referencing Columns
--
* xref:purchasing.purchaseorderheader.adoc#column-totaldue[+Purchasing.PurchaseOrderHeader.TotalDue+]
--


[#column-modifieddate]
=== ModifiedDate

[cols="d,8a,m,m,m"]
|===
|
|ModifiedDate

.Default: DF_PurchaseOrderHeader_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-orderdate]
=== OrderDate

[cols="d,8a,m,m,m"]
|===
|
|OrderDate

.Default: DF_PurchaseOrderHeader_OrderDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|
|===


[#column-revisionnumber]
=== RevisionNumber

[cols="d,8a,m,m,m"]
|===
|
|RevisionNumber

.Default: DF_PurchaseOrderHeader_RevisionNumber
[source,sql]
----
((0))
----


|tinyint
|NOT NULL
|
|===


[#column-shipdate]
=== ShipDate

[cols="d,8a,m,m,m"]
|===
|
|ShipDate
|datetime
|NULL
|
|===


[#column-shipmethodid]
=== ShipMethodID

[cols="d,8a,m,m,m"]
|===
|
|ShipMethodID
|int
|NOT NULL
|
|===


[#column-status]
=== Status

[cols="d,8a,m,m,m"]
|===
|
|Status

.Default: DF_PurchaseOrderHeader_Status
[source,sql]
----
((1))
----


|tinyint
|NOT NULL
|
|===


[#column-subtotal]
=== SubTotal

[cols="d,8a,m,m,m"]
|===
|
|SubTotal

.Default: DF_PurchaseOrderHeader_SubTotal
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|
|===

.Referencing Columns
--
* xref:purchasing.purchaseorderheader.adoc#column-totaldue[+Purchasing.PurchaseOrderHeader.TotalDue+]
--


[#column-taxamt]
=== TaxAmt

[cols="d,8a,m,m,m"]
|===
|
|TaxAmt

.Default: DF_PurchaseOrderHeader_TaxAmt
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|
|===

.Referencing Columns
--
* xref:purchasing.purchaseorderheader.adoc#column-totaldue[+Purchasing.PurchaseOrderHeader.TotalDue+]
--


[#column-vendorid]
=== VendorID

[cols="d,8a,m,m,m"]
|===
|
|VendorID
|int
|NOT NULL
|
|===


[#column-totaldue]
=== TotalDue

[cols="d,8a,m,m,m"]
|===
|
|TotalDue

.Definition (PERSISTED)
[source,sql]
----
(isnull(([SubTotal]+[TaxAmt])+[Freight],(0)))
----


|money
|NOT NULL
|
|===

.Referenced Columns
--
* xref:purchasing.purchaseorderheader.adoc#column-taxamt[+Purchasing.PurchaseOrderHeader.TaxAmt+]
* xref:purchasing.purchaseorderheader.adoc#column-freight[+Purchasing.PurchaseOrderHeader.Freight+]
* xref:purchasing.purchaseorderheader.adoc#column-subtotal[+Purchasing.PurchaseOrderHeader.SubTotal+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-purchaseorderid>>*
|int
|NOT NULL
|(1,1)













// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-employeeid>>
|int
|NOT NULL
|

|
|<<column-freight>>

.Default: DF_PurchaseOrderHeader_Freight
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|

|
|<<column-modifieddate>>

.Default: DF_PurchaseOrderHeader_ModifiedDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-orderdate>>

.Default: DF_PurchaseOrderHeader_OrderDate
[source,sql]
----
(getdate())
----


|datetime
|NOT NULL
|

|
|<<column-revisionnumber>>

.Default: DF_PurchaseOrderHeader_RevisionNumber
[source,sql]
----
((0))
----


|tinyint
|NOT NULL
|

|
|<<column-shipdate>>
|datetime
|NULL
|

|
|<<column-shipmethodid>>
|int
|NOT NULL
|

|
|<<column-status>>

.Default: DF_PurchaseOrderHeader_Status
[source,sql]
----
((1))
----


|tinyint
|NOT NULL
|

|
|<<column-subtotal>>

.Default: DF_PurchaseOrderHeader_SubTotal
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|

|
|<<column-taxamt>>

.Default: DF_PurchaseOrderHeader_TaxAmt
[source,sql]
----
((0.00))
----


|money
|NOT NULL
|

|
|<<column-vendorid>>
|int
|NOT NULL
|

|
|<<column-totaldue>>

.Definition (PERSISTED)
[source,sql]
----
(isnull(([SubTotal]+[TaxAmt])+[Freight],(0)))
----


|money
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinepurchaseorderheaderunderlinepurchaseorderid]
=== PK_PurchaseOrderHeader_PurchaseOrderID

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-PurchaseOrderID>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:purchasing.purchaseorderheader.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]
* xref:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]
* xref:aw:sqldb:purchasing.upurchaseorderheader.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
8ACBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
PurchaseOrderID
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
  - **PurchaseOrderID** : (int)
  - EmployeeID : (int)
  - Freight : (money)
  - ModifiedDate : (datetime)
  - OrderDate : (datetime)
  - RevisionNumber : (tinyint)
  ShipDate : (datetime)
  - ShipMethodID : (int)
  - Status : (tinyint)
  - SubTotal : (money)
  - TaxAmt : (money)
  - VendorID : (int)
  # TotalDue : (money)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
**PK_PurchaseOrderHeader_PurchaseOrderID**

..
PurchaseOrderID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
  - **PurchaseOrderID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

Purchasing.PurchaseOrderHeader <.. Purchasing.iPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderHeader

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
  - **PurchaseOrderID** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
  - **PurchaseOrderID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

Purchasing.PurchaseOrderHeader <.. Purchasing.iPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderHeader

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.ipurchaseorderdetail.adoc[]" as Purchasing.iPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.purchaseorderheader.adoc[]" as Purchasing.PurchaseOrderHeader << U >> {
  - **PurchaseOrderID** : (int)
  - EmployeeID : (int)
  - Freight : (money)
  - ModifiedDate : (datetime)
  - OrderDate : (datetime)
  - RevisionNumber : (tinyint)
  ShipDate : (datetime)
  - ShipMethodID : (int)
  - Status : (tinyint)
  - SubTotal : (money)
  - TaxAmt : (money)
  - VendorID : (int)
  # TotalDue : (money)
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderdetail.adoc[]" as Purchasing.uPurchaseOrderDetail << TR >> {
  --
}

entity "puml-link:aw:sqldb:purchasing.upurchaseorderheader.adoc[]" as Purchasing.uPurchaseOrderHeader << TR >> {
  --
}

Purchasing.PurchaseOrderHeader <.. Purchasing.iPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderDetail
Purchasing.PurchaseOrderHeader <.. Purchasing.uPurchaseOrderHeader
"Purchasing.PurchaseOrderHeader::Freight" <-- "Purchasing.PurchaseOrderHeader::TotalDue"
"Purchasing.PurchaseOrderHeader::SubTotal" <-- "Purchasing.PurchaseOrderHeader::TotalDue"
"Purchasing.PurchaseOrderHeader::TaxAmt" <-- "Purchasing.PurchaseOrderHeader::TotalDue"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


