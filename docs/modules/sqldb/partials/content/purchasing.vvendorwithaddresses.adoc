// tag::HeaderFullDisplayName[]
= Purchasing.vVendorWithAddresses - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
F2CBEA31-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
Purchasing.vVendorWithAddresses
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
711673583
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:33:15
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-addressline1]
=== AddressLine1

[cols="d,8a,m,m,m"]
|===
|
|AddressLine1
|nvarchar(60)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.address.adoc#column-addressline1[+Person.Address.AddressLine1+]
--


[#column-addressline2]
=== AddressLine2

[cols="d,8a,m,m,m"]
|===
|
|AddressLine2
|nvarchar(60)
|NULL
|
|===

.Referenced Columns
--
* xref:person.address.adoc#column-addressline2[+Person.Address.AddressLine2+]
--


[#column-addresstype]
=== AddressType

[cols="d,8a,m,m,m"]
|===
|
|AddressType
|Name
|NOT NULL
|
|===


[#column-businessentityid]
=== BusinessEntityID

[cols="d,8a,m,m,m"]
|===
|
|BusinessEntityID
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:purchasing.vendor.adoc#column-businessentityid[+Purchasing.Vendor.BusinessEntityID+]
--


[#column-city]
=== City

[cols="d,8a,m,m,m"]
|===
|
|City
|nvarchar(30)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.address.adoc#column-city[+Person.Address.City+]
--


[#column-countryregionname]
=== CountryRegionName

[cols="d,8a,m,m,m"]
|===
|
|CountryRegionName
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.countryregion.adoc#column-name[+Person.CountryRegion.Name+]
--


[#column-name]
=== Name

[cols="d,8a,m,m,m"]
|===
|
|Name
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:purchasing.vendor.adoc#column-name[+Purchasing.Vendor.Name+]
--


[#column-postalcode]
=== PostalCode

[cols="d,8a,m,m,m"]
|===
|
|PostalCode
|nvarchar(15)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.address.adoc#column-postalcode[+Person.Address.PostalCode+]
--


[#column-stateprovincename]
=== StateProvinceName

[cols="d,8a,m,m,m"]
|===
|
|StateProvinceName
|Name
|NOT NULL
|
|===

.Referenced Columns
--
* xref:person.stateprovince.adoc#column-name[+Person.StateProvince.Name+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]









// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-addressline1>>
|nvarchar(60)
|NOT NULL
|

|
|<<column-addressline2>>
|nvarchar(60)
|NULL
|

|
|<<column-addresstype>>
|Name
|NOT NULL
|

|
|<<column-businessentityid>>
|int
|NOT NULL
|

|
|<<column-city>>
|nvarchar(30)
|NOT NULL
|

|
|<<column-countryregionname>>
|Name
|NOT NULL
|

|
|<<column-name>>
|Name
|NOT NULL
|

|
|<<column-postalcode>>
|nvarchar(15)
|NOT NULL
|

|
|<<column-stateprovincename>>
|Name
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevvendorwithaddressesunderlineunderline1]
=== idx_vVendorWithAddresses++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-BusinessEntityID>>; int
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldb:purchasing.vvendorwithaddresses.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldb:person.address.adoc[]
* xref:aw:sqldb:person.addresstype.adoc[]
* xref:aw:sqldb:person.businessentityaddress.adoc[]
* xref:aw:sqldb:person.countryregion.adoc[]
* xref:aw:sqldb:person.stateprovince.adoc[]
* xref:aw:sqldb:purchasing.vendor.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  - Name : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
- idx_vVendorWithAddresses__1

..
BusinessEntityID; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.addresstype.adoc[]" as Person.AddressType << U >> {
  - **AddressTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.businessentityaddress.adoc[]" as Person.BusinessEntityAddress << U >> {
  - **BusinessEntityID** : (int)
  - **AddressID** : (int)
  - **AddressTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.countryregion.adoc[]" as Person.CountryRegion << U >> {
  - **CountryRegionCode** : (nvarchar(3))
  --
}

entity "puml-link:aw:sqldb:person.stateprovince.adoc[]" as Person.StateProvince << U >> {
  - **StateProvinceID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vendor.adoc[]" as Purchasing.Vendor << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  --
}

Person.Address <.. Purchasing.vVendorWithAddresses
Person.AddressType <.. Purchasing.vVendorWithAddresses
Person.BusinessEntityAddress <.. Purchasing.vVendorWithAddresses
Person.CountryRegion <.. Purchasing.vVendorWithAddresses
Person.StateProvince <.. Purchasing.vVendorWithAddresses
Purchasing.Vendor <.. Purchasing.vVendorWithAddresses

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.addresstype.adoc[]" as Person.AddressType << U >> {
  - **AddressTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.businessentityaddress.adoc[]" as Person.BusinessEntityAddress << U >> {
  - **BusinessEntityID** : (int)
  - **AddressID** : (int)
  - **AddressTypeID** : (int)
  --
}

entity "puml-link:aw:sqldb:person.countryregion.adoc[]" as Person.CountryRegion << U >> {
  - **CountryRegionCode** : (nvarchar(3))
  --
}

entity "puml-link:aw:sqldb:person.stateprovince.adoc[]" as Person.StateProvince << U >> {
  - **StateProvinceID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vendor.adoc[]" as Purchasing.Vendor << U >> {
  - **BusinessEntityID** : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  --
}

Person.Address <.. Purchasing.vVendorWithAddresses
Person.AddressType <.. Purchasing.vVendorWithAddresses
Person.BusinessEntityAddress <.. Purchasing.vVendorWithAddresses
Person.CountryRegion <.. Purchasing.vVendorWithAddresses
Person.StateProvince <.. Purchasing.vVendorWithAddresses
Purchasing.Vendor <.. Purchasing.vVendorWithAddresses

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldb:person.address.adoc[]" as Person.Address << U >> {
  - **AddressID** : (int)
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - City : (nvarchar(30))
  - ModifiedDate : (datetime)
  - PostalCode : (nvarchar(15))
  - rowguid : (uniqueidentifier)
  SpatialLocation : (geography)
  - StateProvinceID : (int)
  --
}

entity "puml-link:aw:sqldb:person.addresstype.adoc[]" as Person.AddressType << U >> {
  - **AddressTypeID** : (int)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:person.businessentityaddress.adoc[]" as Person.BusinessEntityAddress << U >> {
  - **BusinessEntityID** : (int)
  - **AddressID** : (int)
  - **AddressTypeID** : (int)
  - ModifiedDate : (datetime)
  - rowguid : (uniqueidentifier)
  --
}

entity "puml-link:aw:sqldb:person.countryregion.adoc[]" as Person.CountryRegion << U >> {
  - **CountryRegionCode** : (nvarchar(3))
  - ModifiedDate : (datetime)
  - Name : (Name)
  --
}

entity "puml-link:aw:sqldb:person.stateprovince.adoc[]" as Person.StateProvince << U >> {
  - **StateProvinceID** : (int)
  - CountryRegionCode : (nvarchar(3))
  - IsOnlyStateProvinceFlag : (Flag)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - rowguid : (uniqueidentifier)
  - StateProvinceCode : (nchar(3))
  - TerritoryID : (int)
  --
}

entity "puml-link:aw:sqldb:purchasing.vendor.adoc[]" as Purchasing.Vendor << U >> {
  - **BusinessEntityID** : (int)
  - AccountNumber : (AccountNumber)
  - ActiveFlag : (Flag)
  - CreditRating : (tinyint)
  - ModifiedDate : (datetime)
  - Name : (Name)
  - PreferredVendorStatus : (Flag)
  PurchasingWebServiceURL : (nvarchar(1024))
  --
}

entity "puml-link:aw:sqldb:purchasing.vvendorwithaddresses.adoc[]" as Purchasing.vVendorWithAddresses << V >> {
  - AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  - AddressType : (Name)
  - BusinessEntityID : (int)
  - City : (nvarchar(30))
  - CountryRegionName : (Name)
  - Name : (Name)
  - PostalCode : (nvarchar(15))
  - StateProvinceName : (Name)
  --
}

Person.Address <.. Purchasing.vVendorWithAddresses
Person.AddressType <.. Purchasing.vVendorWithAddresses
Person.BusinessEntityAddress <.. Purchasing.vVendorWithAddresses
Person.CountryRegion <.. Purchasing.vVendorWithAddresses
Person.StateProvince <.. Purchasing.vVendorWithAddresses
Purchasing.Vendor <.. Purchasing.vVendorWithAddresses
"Person.Address::AddressLine1" <-- "Purchasing.vVendorWithAddresses::AddressLine1"
"Person.Address::AddressLine2" <-- "Purchasing.vVendorWithAddresses::AddressLine2"
"Person.Address::City" <-- "Purchasing.vVendorWithAddresses::City"
"Person.Address::PostalCode" <-- "Purchasing.vVendorWithAddresses::PostalCode"
"Person.CountryRegion::Name" <-- "Purchasing.vVendorWithAddresses::CountryRegionName"
"Person.StateProvince::Name" <-- "Purchasing.vVendorWithAddresses::StateProvinceName"
"Purchasing.Vendor::BusinessEntityID" <-- "Purchasing.vVendorWithAddresses::BusinessEntityID"
"Purchasing.Vendor::Name" <-- "Purchasing.vVendorWithAddresses::Name"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

CREATE VIEW [Purchasing].[vVendorWithAddresses] AS 
SELECT 
    v.[BusinessEntityID]
    ,v.[Name]
    ,at.[Name] AS [AddressType]
    ,a.[AddressLine1] 
    ,a.[AddressLine2] 
    ,a.[City] 
    ,sp.[Name] AS [StateProvinceName] 
    ,a.[PostalCode] 
    ,cr.[Name] AS [CountryRegionName] 
FROM [Purchasing].[Vendor] v
    INNER JOIN [Person].[BusinessEntityAddress] bea 
    ON bea.[BusinessEntityID] = v.[BusinessEntityID] 
    INNER JOIN [Person].[Address] a 
    ON a.[AddressID] = bea.[AddressID]
    INNER JOIN [Person].[StateProvince] sp 
    ON sp.[StateProvinceID] = a.[StateProvinceID]
    INNER JOIN [Person].[CountryRegion] cr 
    ON cr.[CountryRegionCode] = sp.[CountryRegionCode]
    INNER JOIN [Person].[AddressType] at 
    ON at.[AddressTypeID] = bea.[AddressTypeID];

----
=======
// end::sql_modules_definition[]


