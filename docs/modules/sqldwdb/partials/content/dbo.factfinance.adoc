// tag::HeaderFullDisplayName[]
= dbo.FactFinance - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--FK:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
9E6AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.FactFinance
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1301579675
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:33
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-accountkey]
=== AccountKey

[cols="d,8a,m,m,m"]
|===
|
|AccountKey
|int
|NOT NULL
|
|===


[#column-amount]
=== Amount

[cols="d,8a,m,m,m"]
|===
|
|Amount
|float
|NOT NULL
|
|===


[#column-date]
=== Date

[cols="d,8a,m,m,m"]
|===
|
|Date
|datetime
|NULL
|
|===


[#column-datekey]
=== DateKey

[cols="d,8a,m,m,m"]
|===
|
|DateKey
|int
|NOT NULL
|
|===


[#column-departmentgroupkey]
=== DepartmentGroupKey

[cols="d,8a,m,m,m"]
|===
|
|DepartmentGroupKey
|int
|NOT NULL
|
|===


[#column-financekey]
=== FinanceKey

[cols="d,8a,m,m,m"]
|===
|
|FinanceKey
|int
|NOT NULL
|(1,1)
|===


[#column-organizationkey]
=== OrganizationKey

[cols="d,8a,m,m,m"]
|===
|
|OrganizationKey
|int
|NOT NULL
|
|===


[#column-scenariokey]
=== ScenarioKey

[cols="d,8a,m,m,m"]
|===
|
|ScenarioKey
|int
|NOT NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]








// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-accountkey>>
|int
|NOT NULL
|

|
|<<column-amount>>
|float
|NOT NULL
|

|
|<<column-date>>
|datetime
|NULL
|

|
|<<column-datekey>>
|int
|NOT NULL
|

|
|<<column-departmentgroupkey>>
|int
|NOT NULL
|

|
|<<column-financekey>>
|int
|NOT NULL
|(1,1)

|
|<<column-organizationkey>>
|int
|NOT NULL
|

|
|<<column-scenariokey>>
|int
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.factfinance.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {
  - AccountKey : (int)
  - Amount : (float)
  Date : (datetime)
  - DateKey : (int)
  - DepartmentGroupKey : (int)
  - FinanceKey : (int)
  - OrganizationKey : (int)
  - ScenarioKey : (int)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {

}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factfinance.adoc[]" as dbo.FactFinance << U >> {
  - AccountKey : (int)
  - Amount : (float)
  Date : (datetime)
  - DateKey : (int)
  - DepartmentGroupKey : (int)
  - FinanceKey : (int)
  - OrganizationKey : (int)
  - ScenarioKey : (int)
  --
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


