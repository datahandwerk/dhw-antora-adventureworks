// tag::HeaderFullDisplayName[]
= dbo.vAssocSeqOrders - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
846AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.vAssocSeqOrders
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
626101271
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:34
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-customerkey]
=== CustomerKey

[cols="d,8a,m,m,m"]
|===
|
|CustomerKey
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.vdmprep.adoc#column-customerkey[+dbo.vDMPrep.CustomerKey+]
--


[#column-incomegroup]
=== IncomeGroup

[cols="d,8a,m,m,m"]
|===
|
|IncomeGroup
|varchar(8)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.vdmprep.adoc#column-incomegroup[+dbo.vDMPrep.IncomeGroup+]
--


[#column-ordernumber]
=== OrderNumber

[cols="d,8a,m,m,m"]
|===
|
|OrderNumber
|nvarchar(20)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.vdmprep.adoc#column-ordernumber[+dbo.vDMPrep.OrderNumber+]
--


[#column-region]
=== Region

[cols="d,8a,m,m,m"]
|===
|
|Region
|nvarchar(50)
|NULL
|
|===

.Referenced Columns
--
* xref:dbo.vdmprep.adoc#column-region[+dbo.vDMPrep.Region+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]




// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-customerkey>>
|int
|NOT NULL
|

|
|<<column-incomegroup>>
|varchar(8)
|NOT NULL
|

|
|<<column-ordernumber>>
|nvarchar(20)
|NOT NULL
|

|
|<<column-region>>
|nvarchar(50)
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevassocseqordersunderlineunderline1]
=== idx_vAssocSeqOrders++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-CustomerKey>>; int
--
* PK, Unique, Real: 0, 0, 0


[#index-idxunderlinevassocseqordersunderlineunderline2]
=== idx_vAssocSeqOrders++__++2

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-OrderNumber>>; nvarchar(20)
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.vassocseqorders.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldwdb:dbo.vdmprep.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  - CustomerKey : (int)
  - IncomeGroup : (varchar(8))
  - OrderNumber : (nvarchar(20))
  Region : (nvarchar(50))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
- idx_vAssocSeqOrders__1

..
CustomerKey; int
--
- idx_vAssocSeqOrders__2

..
OrderNumber; nvarchar(20)
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

dbo.vDMPrep <.. dbo.vAssocSeqOrders

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimcustomer.adoc[]" as dbo.DimCustomer << U >> {
  - **CustomerKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as dbo.DimDate << U >> {
  - **DateKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimgeography.adoc[]" as dbo.DimGeography << U >> {
  - **GeographyKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as dbo.DimProduct << U >> {
  - **ProductKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as dbo.DimProductCategory << U >> {
  - **ProductCategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductsubcategory.adoc[]" as dbo.DimProductSubcategory << U >> {
  - **ProductSubcategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.factinternetsales.adoc[]" as dbo.FactInternetSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

dbo.DimCustomer <.. dbo.vDMPrep
dbo.DimDate <.. dbo.vDMPrep
dbo.DimGeography <.. dbo.vDMPrep
dbo.DimProduct <.. dbo.vDMPrep
dbo.DimProductCategory <.. dbo.vDMPrep
dbo.DimProductSubcategory <.. dbo.vDMPrep
dbo.DimSalesTerritory <.. dbo.vDMPrep
dbo.FactInternetSales <.. dbo.vDMPrep
dbo.vDMPrep <.. dbo.vAssocSeqOrders

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  - CustomerKey : (int)
  - IncomeGroup : (varchar(8))
  - OrderNumber : (nvarchar(20))
  Region : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  Age : (int)
  - Amount : (money)
  - CalendarYear : (smallint)
  - CustomerKey : (int)
  - EnglishProductCategoryName : (nvarchar(50))
  - FiscalYear : (smallint)
  - IncomeGroup : (varchar(8))
  - LineNumber : (tinyint)
  Model : (nvarchar(50))
  - Month : (tinyint)
  - OrderNumber : (nvarchar(20))
  - Quantity : (smallint)
  Region : (nvarchar(50))
  --
}

dbo.vDMPrep <.. dbo.vAssocSeqOrders
"dbo.vDMPrep::CustomerKey" <-- "dbo.vAssocSeqOrders::CustomerKey"
"dbo.vDMPrep::IncomeGroup" <-- "dbo.vAssocSeqOrders::IncomeGroup"
"dbo.vDMPrep::OrderNumber" <-- "dbo.vAssocSeqOrders::OrderNumber"
"dbo.vDMPrep::Region" <-- "dbo.vAssocSeqOrders::Region"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----
/* vAssocSeqOrders supports assocation and sequence clustering data mmining models.
      - Limits data to FY2004.
      - Creates order case table and line item nested table.*/
CREATE VIEW [dbo].[vAssocSeqOrders]
AS
SELECT DISTINCT OrderNumber, CustomerKey, Region, IncomeGroup
FROM         dbo.vDMPrep
WHERE     (FiscalYear = '2013')


----
=======
// end::sql_modules_definition[]


