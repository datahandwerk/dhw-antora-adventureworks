// tag::HeaderFullDisplayName[]
= dbo.vDMPrep - V
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--sql_modules_definition:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
816AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.vDMPrep
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
V 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
view
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
578101100
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:34
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-age]
=== Age

[cols="d,8a,m,m,m"]
|===
|
|Age
|int
|NULL
|
|===


[#column-amount]
=== Amount

[cols="d,8a,m,m,m"]
|===
|
|Amount
|money
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.factinternetsales.adoc#column-extendedamount[+dbo.FactInternetSales.ExtendedAmount+]
--


[#column-calendaryear]
=== CalendarYear

[cols="d,8a,m,m,m"]
|===
|
|CalendarYear
|smallint
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimdate.adoc#column-calendaryear[+dbo.DimDate.CalendarYear+]
--

.Referencing Columns
--
* xref:dbo.vtimeseries.adoc#column-calendaryear[+dbo.vTimeSeries.CalendarYear+]
--


[#column-customerkey]
=== CustomerKey

[cols="d,8a,m,m,m"]
|===
|
|CustomerKey
|int
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimcustomer.adoc#column-customerkey[+dbo.DimCustomer.CustomerKey+]
--

.Referencing Columns
--
* xref:dbo.vassocseqorders.adoc#column-customerkey[+dbo.vAssocSeqOrders.CustomerKey+]
--


[#column-englishproductcategoryname]
=== EnglishProductCategoryName

[cols="d,8a,m,m,m"]
|===
|
|EnglishProductCategoryName
|nvarchar(50)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimproductcategory.adoc#column-englishproductcategoryname[+dbo.DimProductCategory.EnglishProductCategoryName+]
--


[#column-fiscalyear]
=== FiscalYear

[cols="d,8a,m,m,m"]
|===
|
|FiscalYear
|smallint
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimdate.adoc#column-fiscalyear[+dbo.DimDate.FiscalYear+]
--


[#column-incomegroup]
=== IncomeGroup

[cols="d,8a,m,m,m"]
|===
|
|IncomeGroup
|varchar(8)
|NOT NULL
|
|===

.Referencing Columns
--
* xref:dbo.vassocseqorders.adoc#column-incomegroup[+dbo.vAssocSeqOrders.IncomeGroup+]
--


[#column-linenumber]
=== LineNumber

[cols="d,8a,m,m,m"]
|===
|
|LineNumber
|tinyint
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.factinternetsales.adoc#column-salesorderlinenumber[+dbo.FactInternetSales.SalesOrderLineNumber+]
--

.Referencing Columns
--
* xref:dbo.vassocseqlineitems.adoc#column-linenumber[+dbo.vAssocSeqLineItems.LineNumber+]
--


[#column-model]
=== Model

[cols="d,8a,m,m,m"]
|===
|
|Model
|nvarchar(50)
|NULL
|
|===

.Referencing Columns
--
* xref:dbo.vassocseqlineitems.adoc#column-model[+dbo.vAssocSeqLineItems.Model+]
--


[#column-month]
=== Month

[cols="d,8a,m,m,m"]
|===
|
|Month
|tinyint
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimdate.adoc#column-monthnumberofyear[+dbo.DimDate.MonthNumberOfYear+]
--

.Referencing Columns
--
* xref:dbo.vtimeseries.adoc#column-month[+dbo.vTimeSeries.Month+]
--


[#column-ordernumber]
=== OrderNumber

[cols="d,8a,m,m,m"]
|===
|
|OrderNumber
|nvarchar(20)
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.factinternetsales.adoc#column-salesordernumber[+dbo.FactInternetSales.SalesOrderNumber+]
--

.Referencing Columns
--
* xref:dbo.vassocseqlineitems.adoc#column-ordernumber[+dbo.vAssocSeqLineItems.OrderNumber+]
* xref:dbo.vassocseqorders.adoc#column-ordernumber[+dbo.vAssocSeqOrders.OrderNumber+]
--


[#column-quantity]
=== Quantity

[cols="d,8a,m,m,m"]
|===
|
|Quantity
|smallint
|NOT NULL
|
|===

.Referenced Columns
--
* xref:dbo.factinternetsales.adoc#column-orderquantity[+dbo.FactInternetSales.OrderQuantity+]
--


[#column-region]
=== Region

[cols="d,8a,m,m,m"]
|===
|
|Region
|nvarchar(50)
|NULL
|
|===

.Referenced Columns
--
* xref:dbo.dimsalesterritory.adoc#column-salesterritorygroup[+dbo.DimSalesTerritory.SalesTerritoryGroup+]
--

.Referencing Columns
--
* xref:dbo.vassocseqorders.adoc#column-region[+dbo.vAssocSeqOrders.Region+]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]













// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-age>>
|int
|NULL
|

|
|<<column-amount>>
|money
|NOT NULL
|

|
|<<column-calendaryear>>
|smallint
|NOT NULL
|

|
|<<column-customerkey>>
|int
|NOT NULL
|

|
|<<column-englishproductcategoryname>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-fiscalyear>>
|smallint
|NOT NULL
|

|
|<<column-incomegroup>>
|varchar(8)
|NOT NULL
|

|
|<<column-linenumber>>
|tinyint
|NOT NULL
|

|
|<<column-model>>
|nvarchar(50)
|NULL
|

|
|<<column-month>>
|tinyint
|NOT NULL
|

|
|<<column-ordernumber>>
|nvarchar(20)
|NOT NULL
|

|
|<<column-quantity>>
|smallint
|NOT NULL
|

|
|<<column-region>>
|nvarchar(50)
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-idxunderlinevdmprepunderlineunderline1]
=== idx_vDMPrep++__++1

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-CustomerKey>>; int
--
* PK, Unique, Real: 0, 0, 0


[#index-idxunderlinevdmprepunderlineunderline2]
=== idx_vDMPrep++__++2

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-OrderNumber>>; nvarchar(20)
* <<column-LineNumber>>; tinyint
--
* PK, Unique, Real: 0, 0, 0

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.vdmprep.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldwdb:dbo.dimcustomer.adoc[]
* xref:aw:sqldwdb:dbo.dimdate.adoc[]
* xref:aw:sqldwdb:dbo.dimgeography.adoc[]
* xref:aw:sqldwdb:dbo.dimproduct.adoc[]
* xref:aw:sqldwdb:dbo.dimproductcategory.adoc[]
* xref:aw:sqldwdb:dbo.dimproductsubcategory.adoc[]
* xref:aw:sqldwdb:dbo.dimsalesterritory.adoc[]
* xref:aw:sqldwdb:dbo.factinternetsales.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldwdb:dbo.vassocseqlineitems.adoc[]
* xref:aw:sqldwdb:dbo.vassocseqorders.adoc[]
* xref:aw:sqldwdb:dbo.vtargetmail.adoc[]
* xref:aw:sqldwdb:dbo.vtimeseries.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  Age : (int)
  - Amount : (money)
  - CalendarYear : (smallint)
  - CustomerKey : (int)
  - EnglishProductCategoryName : (nvarchar(50))
  - FiscalYear : (smallint)
  - IncomeGroup : (varchar(8))
  - LineNumber : (tinyint)
  Model : (nvarchar(50))
  - Month : (tinyint)
  - OrderNumber : (nvarchar(20))
  - Quantity : (smallint)
  Region : (nvarchar(50))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
- idx_vDMPrep__1

..
CustomerKey; int
--
- idx_vDMPrep__2

..
OrderNumber; nvarchar(20)
LineNumber; tinyint
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimcustomer.adoc[]" as dbo.DimCustomer << U >> {
  - **CustomerKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as dbo.DimDate << U >> {
  - **DateKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimgeography.adoc[]" as dbo.DimGeography << U >> {
  - **GeographyKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as dbo.DimProduct << U >> {
  - **ProductKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as dbo.DimProductCategory << U >> {
  - **ProductCategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductsubcategory.adoc[]" as dbo.DimProductSubcategory << U >> {
  - **ProductSubcategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.factinternetsales.adoc[]" as dbo.FactInternetSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqlineitems.adoc[]" as dbo.vAssocSeqLineItems << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtargetmail.adoc[]" as dbo.vTargetMail << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtimeseries.adoc[]" as dbo.vTimeSeries << V >> {
  --
}

dbo.DimCustomer <.. dbo.vDMPrep
dbo.DimDate <.. dbo.vDMPrep
dbo.DimGeography <.. dbo.vDMPrep
dbo.DimProduct <.. dbo.vDMPrep
dbo.DimProductCategory <.. dbo.vDMPrep
dbo.DimProductSubcategory <.. dbo.vDMPrep
dbo.DimSalesTerritory <.. dbo.vDMPrep
dbo.FactInternetSales <.. dbo.vDMPrep
dbo.vDMPrep <.. dbo.vAssocSeqLineItems
dbo.vDMPrep <.. dbo.vAssocSeqOrders
dbo.vDMPrep <.. dbo.vTargetMail
dbo.vDMPrep <.. dbo.vTimeSeries

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimcustomer.adoc[]" as dbo.DimCustomer << U >> {
  - **CustomerKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as dbo.DimDate << U >> {
  - **DateKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimgeography.adoc[]" as dbo.DimGeography << U >> {
  - **GeographyKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as dbo.DimProduct << U >> {
  - **ProductKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as dbo.DimProductCategory << U >> {
  - **ProductCategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductsubcategory.adoc[]" as dbo.DimProductSubcategory << U >> {
  - **ProductSubcategoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.factinternetsales.adoc[]" as dbo.FactInternetSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

dbo.DimCustomer <.. dbo.vDMPrep
dbo.DimDate <.. dbo.vDMPrep
dbo.DimGeography <.. dbo.vDMPrep
dbo.DimProduct <.. dbo.vDMPrep
dbo.DimProductCategory <.. dbo.vDMPrep
dbo.DimProductSubcategory <.. dbo.vDMPrep
dbo.DimSalesTerritory <.. dbo.vDMPrep
dbo.FactInternetSales <.. dbo.vDMPrep

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.vassocseqlineitems.adoc[]" as dbo.vAssocSeqLineItems << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtargetmail.adoc[]" as dbo.vTargetMail << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtimeseries.adoc[]" as dbo.vTimeSeries << V >> {
  --
}

dbo.vDMPrep <.. dbo.vAssocSeqLineItems
dbo.vDMPrep <.. dbo.vAssocSeqOrders
dbo.vDMPrep <.. dbo.vTargetMail
dbo.vDMPrep <.. dbo.vTimeSeries

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimcustomer.adoc[]" as dbo.DimCustomer << U >> {
  - **CustomerKey** : (int)
  AddressLine1 : (nvarchar(120))
  AddressLine2 : (nvarchar(120))
  BirthDate : (date)
  CommuteDistance : (nvarchar(15))
  - CustomerAlternateKey : (nvarchar(15))
  DateFirstPurchase : (date)
  EmailAddress : (nvarchar(50))
  EnglishEducation : (nvarchar(40))
  EnglishOccupation : (nvarchar(100))
  FirstName : (nvarchar(50))
  FrenchEducation : (nvarchar(40))
  FrenchOccupation : (nvarchar(100))
  Gender : (nvarchar(1))
  GeographyKey : (int)
  HouseOwnerFlag : (nchar(1))
  LastName : (nvarchar(50))
  MaritalStatus : (nchar(1))
  MiddleName : (nvarchar(50))
  NameStyle : (bit)
  NumberCarsOwned : (tinyint)
  NumberChildrenAtHome : (tinyint)
  Phone : (nvarchar(20))
  SpanishEducation : (nvarchar(40))
  SpanishOccupation : (nvarchar(100))
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  TotalChildren : (tinyint)
  YearlyIncome : (money)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as dbo.DimDate << U >> {
  - **DateKey** : (int)
  - CalendarQuarter : (tinyint)
  - CalendarSemester : (tinyint)
  - CalendarYear : (smallint)
  - DayNumberOfMonth : (tinyint)
  - DayNumberOfWeek : (tinyint)
  - DayNumberOfYear : (smallint)
  - EnglishDayNameOfWeek : (nvarchar(10))
  - EnglishMonthName : (nvarchar(10))
  - FiscalQuarter : (tinyint)
  - FiscalSemester : (tinyint)
  - FiscalYear : (smallint)
  - FrenchDayNameOfWeek : (nvarchar(10))
  - FrenchMonthName : (nvarchar(10))
  - FullDateAlternateKey : (date)
  - MonthNumberOfYear : (tinyint)
  - SpanishDayNameOfWeek : (nvarchar(10))
  - SpanishMonthName : (nvarchar(10))
  - WeekNumberOfYear : (tinyint)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimgeography.adoc[]" as dbo.DimGeography << U >> {
  - **GeographyKey** : (int)
  City : (nvarchar(30))
  CountryRegionCode : (nvarchar(3))
  EnglishCountryRegionName : (nvarchar(50))
  FrenchCountryRegionName : (nvarchar(50))
  IpAddressLocator : (nvarchar(15))
  PostalCode : (nvarchar(15))
  SalesTerritoryKey : (int)
  SpanishCountryRegionName : (nvarchar(50))
  StateProvinceCode : (nvarchar(3))
  StateProvinceName : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as dbo.DimProduct << U >> {
  - **ProductKey** : (int)
  ArabicDescription : (nvarchar(400))
  ChineseDescription : (nvarchar(400))
  Class : (nchar(2))
  - Color : (nvarchar(15))
  DaysToManufacture : (int)
  DealerPrice : (money)
  EndDate : (datetime)
  EnglishDescription : (nvarchar(400))
  - EnglishProductName : (nvarchar(50))
  - FinishedGoodsFlag : (bit)
  FrenchDescription : (nvarchar(400))
  - FrenchProductName : (nvarchar(50))
  GermanDescription : (nvarchar(400))
  HebrewDescription : (nvarchar(400))
  JapaneseDescription : (nvarchar(400))
  LargePhoto : (varbinary(max))
  ListPrice : (money)
  ModelName : (nvarchar(50))
  ProductAlternateKey : (nvarchar(25))
  ProductLine : (nchar(2))
  ProductSubcategoryKey : (int)
  ReorderPoint : (smallint)
  SafetyStockLevel : (smallint)
  Size : (nvarchar(50))
  SizeRange : (nvarchar(50))
  SizeUnitMeasureCode : (nchar(3))
  - SpanishProductName : (nvarchar(50))
  StandardCost : (money)
  StartDate : (datetime)
  Status : (nvarchar(7))
  Style : (nchar(2))
  ThaiDescription : (nvarchar(400))
  TurkishDescription : (nvarchar(400))
  Weight : (float)
  WeightUnitMeasureCode : (nchar(3))
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as dbo.DimProductCategory << U >> {
  - **ProductCategoryKey** : (int)
  - EnglishProductCategoryName : (nvarchar(50))
  - FrenchProductCategoryName : (nvarchar(50))
  ProductCategoryAlternateKey : (int)
  - SpanishProductCategoryName : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductsubcategory.adoc[]" as dbo.DimProductSubcategory << U >> {
  - **ProductSubcategoryKey** : (int)
  - EnglishProductSubcategoryName : (nvarchar(50))
  - FrenchProductSubcategoryName : (nvarchar(50))
  ProductCategoryKey : (int)
  ProductSubcategoryAlternateKey : (int)
  - SpanishProductSubcategoryName : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  SalesTerritoryAlternateKey : (int)
  - SalesTerritoryCountry : (nvarchar(50))
  SalesTerritoryGroup : (nvarchar(50))
  SalesTerritoryImage : (varbinary(max))
  - SalesTerritoryRegion : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.factinternetsales.adoc[]" as dbo.FactInternetSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  CarrierTrackingNumber : (nvarchar(25))
  - CurrencyKey : (int)
  - CustomerKey : (int)
  CustomerPONumber : (nvarchar(25))
  - DiscountAmount : (float)
  DueDate : (datetime)
  - DueDateKey : (int)
  - ExtendedAmount : (money)
  - Freight : (money)
  OrderDate : (datetime)
  - OrderDateKey : (int)
  - OrderQuantity : (smallint)
  - ProductKey : (int)
  - ProductStandardCost : (money)
  - PromotionKey : (int)
  - RevisionNumber : (tinyint)
  - SalesAmount : (money)
  - SalesTerritoryKey : (int)
  ShipDate : (datetime)
  - ShipDateKey : (int)
  - TaxAmt : (money)
  - TotalProductCost : (money)
  - UnitPrice : (money)
  - UnitPriceDiscountPct : (float)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqlineitems.adoc[]" as dbo.vAssocSeqLineItems << V >> {
  - LineNumber : (tinyint)
  Model : (nvarchar(50))
  - OrderNumber : (nvarchar(20))
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  - CustomerKey : (int)
  - IncomeGroup : (varchar(8))
  - OrderNumber : (nvarchar(20))
  Region : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  Age : (int)
  - Amount : (money)
  - CalendarYear : (smallint)
  - CustomerKey : (int)
  - EnglishProductCategoryName : (nvarchar(50))
  - FiscalYear : (smallint)
  - IncomeGroup : (varchar(8))
  - LineNumber : (tinyint)
  Model : (nvarchar(50))
  - Month : (tinyint)
  - OrderNumber : (nvarchar(20))
  - Quantity : (smallint)
  Region : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtargetmail.adoc[]" as dbo.vTargetMail << V >> {
  AddressLine1 : (nvarchar(120))
  AddressLine2 : (nvarchar(120))
  Age : (int)
  - BikeBuyer : (int)
  BirthDate : (date)
  CommuteDistance : (nvarchar(15))
  - CustomerAlternateKey : (nvarchar(15))
  - CustomerKey : (int)
  DateFirstPurchase : (date)
  EmailAddress : (nvarchar(50))
  EnglishEducation : (nvarchar(40))
  EnglishOccupation : (nvarchar(100))
  FirstName : (nvarchar(50))
  FrenchEducation : (nvarchar(40))
  FrenchOccupation : (nvarchar(100))
  Gender : (nvarchar(1))
  GeographyKey : (int)
  HouseOwnerFlag : (nchar(1))
  LastName : (nvarchar(50))
  MaritalStatus : (nchar(1))
  MiddleName : (nvarchar(50))
  NameStyle : (bit)
  NumberCarsOwned : (tinyint)
  NumberChildrenAtHome : (tinyint)
  Phone : (nvarchar(20))
  Region : (nvarchar(50))
  SpanishEducation : (nvarchar(40))
  SpanishOccupation : (nvarchar(100))
  Suffix : (nvarchar(10))
  Title : (nvarchar(8))
  TotalChildren : (tinyint)
  YearlyIncome : (money)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtimeseries.adoc[]" as dbo.vTimeSeries << V >> {
  Amount : (money)
  - CalendarYear : (smallint)
  ModelRegion : (nvarchar(56))
  - Month : (tinyint)
  Quantity : (int)
  ReportingDate : (datetime)
  TimeIndex : (int)
  --
}

dbo.DimCustomer <.. dbo.vDMPrep
dbo.DimDate <.. dbo.vDMPrep
dbo.DimGeography <.. dbo.vDMPrep
dbo.DimProduct <.. dbo.vDMPrep
dbo.DimProductCategory <.. dbo.vDMPrep
dbo.DimProductSubcategory <.. dbo.vDMPrep
dbo.DimSalesTerritory <.. dbo.vDMPrep
dbo.FactInternetSales <.. dbo.vDMPrep
dbo.vDMPrep <.. dbo.vAssocSeqLineItems
dbo.vDMPrep <.. dbo.vAssocSeqOrders
dbo.vDMPrep <.. dbo.vTargetMail
dbo.vDMPrep <.. dbo.vTimeSeries
"dbo.DimCustomer::CustomerKey" <-- "dbo.vDMPrep::CustomerKey"
"dbo.DimDate::CalendarYear" <-- "dbo.vDMPrep::CalendarYear"
"dbo.DimDate::FiscalYear" <-- "dbo.vDMPrep::FiscalYear"
"dbo.DimDate::MonthNumberOfYear" <-- "dbo.vDMPrep::Month"
"dbo.DimProductCategory::EnglishProductCategoryName" <-- "dbo.vDMPrep::EnglishProductCategoryName"
"dbo.DimSalesTerritory::SalesTerritoryGroup" <-- "dbo.vDMPrep::Region"
"dbo.FactInternetSales::ExtendedAmount" <-- "dbo.vDMPrep::Amount"
"dbo.FactInternetSales::OrderQuantity" <-- "dbo.vDMPrep::Quantity"
"dbo.FactInternetSales::SalesOrderLineNumber" <-- "dbo.vDMPrep::LineNumber"
"dbo.FactInternetSales::SalesOrderNumber" <-- "dbo.vDMPrep::OrderNumber"
"dbo.vDMPrep::CalendarYear" <-- "dbo.vTimeSeries::CalendarYear"
"dbo.vDMPrep::CustomerKey" <-- "dbo.vAssocSeqOrders::CustomerKey"
"dbo.vDMPrep::IncomeGroup" <-- "dbo.vAssocSeqOrders::IncomeGroup"
"dbo.vDMPrep::LineNumber" <-- "dbo.vAssocSeqLineItems::LineNumber"
"dbo.vDMPrep::Model" <-- "dbo.vAssocSeqLineItems::Model"
"dbo.vDMPrep::Month" <-- "dbo.vTimeSeries::Month"
"dbo.vDMPrep::OrderNumber" <-- "dbo.vAssocSeqLineItems::OrderNumber"
"dbo.vDMPrep::OrderNumber" <-- "dbo.vAssocSeqOrders::OrderNumber"
"dbo.vDMPrep::Region" <-- "dbo.vAssocSeqOrders::Region"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----


-- vDMPrep will be used as a data source by the other data mining views.  
-- Uses DW data at customer, product, day, etc. granularity and
-- gets region, model, year, month, etc.
CREATE VIEW [dbo].[vDMPrep]
AS
    SELECT
        pc.[EnglishProductCategoryName]
        ,Coalesce(p.[ModelName], p.[EnglishProductName]) AS [Model]
        ,c.[CustomerKey]
        ,s.[SalesTerritoryGroup] AS [Region]
        ,CASE
            WHEN Month(GetDate()) < Month(c.[BirthDate])
                THEN DateDiff(yy,c.[BirthDate],GetDate()) - 1
            WHEN Month(GetDate()) = Month(c.[BirthDate])
            AND Day(GetDate()) < Day(c.[BirthDate])
                THEN DateDiff(yy,c.[BirthDate],GetDate()) - 1
            ELSE DateDiff(yy,c.[BirthDate],GetDate())
        END AS [Age]
        ,CASE
            WHEN c.[YearlyIncome] < 40000 THEN 'Low'
            WHEN c.[YearlyIncome] > 60000 THEN 'High'
            ELSE 'Moderate'
        END AS [IncomeGroup]
        ,d.[CalendarYear]
        ,d.[FiscalYear]
        ,d.[MonthNumberOfYear] AS [Month]
        ,f.[SalesOrderNumber] AS [OrderNumber]
        ,f.SalesOrderLineNumber AS LineNumber
        ,f.OrderQuantity AS Quantity
        ,f.ExtendedAmount AS Amount  
    FROM
        [dbo].[FactInternetSales] f
    INNER JOIN [dbo].[DimDate] d
        ON f.[OrderDateKey] = d.[DateKey]
    INNER JOIN [dbo].[DimProduct] p
        ON f.[ProductKey] = p.[ProductKey]
    INNER JOIN [dbo].[DimProductSubcategory] psc
        ON p.[ProductSubcategoryKey] = psc.[ProductSubcategoryKey]
    INNER JOIN [dbo].[DimProductCategory] pc
        ON psc.[ProductCategoryKey] = pc.[ProductCategoryKey]
    INNER JOIN [dbo].[DimCustomer] c
        ON f.[CustomerKey] = c.[CustomerKey]
    INNER JOIN [dbo].[DimGeography] g
        ON c.[GeographyKey] = g.[GeographyKey]
    INNER JOIN [dbo].[DimSalesTerritory] s
        ON g.[SalesTerritoryKey] = s.[SalesTerritoryKey] 
;


----
=======
// end::sql_modules_definition[]


