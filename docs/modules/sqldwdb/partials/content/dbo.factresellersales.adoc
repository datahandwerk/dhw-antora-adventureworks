// tag::HeaderFullDisplayName[]
= dbo.FactResellerSales - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
A26AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.FactResellerSales
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1365579903
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:33
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-salesordernumber]
=== SalesOrderNumber

[cols="d,8a,m,m,m"]
|===
|1
|*SalesOrderNumber*
|nvarchar(20)
|NOT NULL
|
|===


[#column-salesorderlinenumber]
=== SalesOrderLineNumber

[cols="d,8a,m,m,m"]
|===
|2
|*SalesOrderLineNumber*
|tinyint
|NOT NULL
|
|===


[#column-carriertrackingnumber]
=== CarrierTrackingNumber

[cols="d,8a,m,m,m"]
|===
|
|CarrierTrackingNumber
|nvarchar(25)
|NULL
|
|===


[#column-currencykey]
=== CurrencyKey

[cols="d,8a,m,m,m"]
|===
|
|CurrencyKey
|int
|NOT NULL
|
|===


[#column-customerponumber]
=== CustomerPONumber

[cols="d,8a,m,m,m"]
|===
|
|CustomerPONumber
|nvarchar(25)
|NULL
|
|===


[#column-discountamount]
=== DiscountAmount

[cols="d,8a,m,m,m"]
|===
|
|DiscountAmount
|float
|NULL
|
|===


[#column-duedate]
=== DueDate

[cols="d,8a,m,m,m"]
|===
|
|DueDate
|datetime
|NULL
|
|===


[#column-duedatekey]
=== DueDateKey

[cols="d,8a,m,m,m"]
|===
|
|DueDateKey
|int
|NOT NULL
|
|===


[#column-employeekey]
=== EmployeeKey

[cols="d,8a,m,m,m"]
|===
|
|EmployeeKey
|int
|NOT NULL
|
|===


[#column-extendedamount]
=== ExtendedAmount

[cols="d,8a,m,m,m"]
|===
|
|ExtendedAmount
|money
|NULL
|
|===


[#column-freight]
=== Freight

[cols="d,8a,m,m,m"]
|===
|
|Freight
|money
|NULL
|
|===


[#column-orderdate]
=== OrderDate

[cols="d,8a,m,m,m"]
|===
|
|OrderDate
|datetime
|NULL
|
|===


[#column-orderdatekey]
=== OrderDateKey

[cols="d,8a,m,m,m"]
|===
|
|OrderDateKey
|int
|NOT NULL
|
|===


[#column-orderquantity]
=== OrderQuantity

[cols="d,8a,m,m,m"]
|===
|
|OrderQuantity
|smallint
|NULL
|
|===


[#column-productkey]
=== ProductKey

[cols="d,8a,m,m,m"]
|===
|
|ProductKey
|int
|NOT NULL
|
|===


[#column-productstandardcost]
=== ProductStandardCost

[cols="d,8a,m,m,m"]
|===
|
|ProductStandardCost
|money
|NULL
|
|===


[#column-promotionkey]
=== PromotionKey

[cols="d,8a,m,m,m"]
|===
|
|PromotionKey
|int
|NOT NULL
|
|===


[#column-resellerkey]
=== ResellerKey

[cols="d,8a,m,m,m"]
|===
|
|ResellerKey
|int
|NOT NULL
|
|===


[#column-revisionnumber]
=== RevisionNumber

[cols="d,8a,m,m,m"]
|===
|
|RevisionNumber
|tinyint
|NULL
|
|===


[#column-salesamount]
=== SalesAmount

[cols="d,8a,m,m,m"]
|===
|
|SalesAmount
|money
|NULL
|
|===


[#column-salesterritorykey]
=== SalesTerritoryKey

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryKey
|int
|NOT NULL
|
|===


[#column-shipdate]
=== ShipDate

[cols="d,8a,m,m,m"]
|===
|
|ShipDate
|datetime
|NULL
|
|===


[#column-shipdatekey]
=== ShipDateKey

[cols="d,8a,m,m,m"]
|===
|
|ShipDateKey
|int
|NOT NULL
|
|===


[#column-taxamt]
=== TaxAmt

[cols="d,8a,m,m,m"]
|===
|
|TaxAmt
|money
|NULL
|
|===


[#column-totalproductcost]
=== TotalProductCost

[cols="d,8a,m,m,m"]
|===
|
|TotalProductCost
|money
|NULL
|
|===


[#column-unitprice]
=== UnitPrice

[cols="d,8a,m,m,m"]
|===
|
|UnitPrice
|money
|NULL
|
|===


[#column-unitpricediscountpct]
=== UnitPriceDiscountPct

[cols="d,8a,m,m,m"]
|===
|
|UnitPriceDiscountPct
|float
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-salesordernumber>>*
|nvarchar(20)
|NOT NULL
|

|2
|*<<column-salesorderlinenumber>>*
|tinyint
|NOT NULL
|


























// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]


|
|<<column-carriertrackingnumber>>
|nvarchar(25)
|NULL
|

|
|<<column-currencykey>>
|int
|NOT NULL
|

|
|<<column-customerponumber>>
|nvarchar(25)
|NULL
|

|
|<<column-discountamount>>
|float
|NULL
|

|
|<<column-duedate>>
|datetime
|NULL
|

|
|<<column-duedatekey>>
|int
|NOT NULL
|

|
|<<column-employeekey>>
|int
|NOT NULL
|

|
|<<column-extendedamount>>
|money
|NULL
|

|
|<<column-freight>>
|money
|NULL
|

|
|<<column-orderdate>>
|datetime
|NULL
|

|
|<<column-orderdatekey>>
|int
|NOT NULL
|

|
|<<column-orderquantity>>
|smallint
|NULL
|

|
|<<column-productkey>>
|int
|NOT NULL
|

|
|<<column-productstandardcost>>
|money
|NULL
|

|
|<<column-promotionkey>>
|int
|NOT NULL
|

|
|<<column-resellerkey>>
|int
|NOT NULL
|

|
|<<column-revisionnumber>>
|tinyint
|NULL
|

|
|<<column-salesamount>>
|money
|NULL
|

|
|<<column-salesterritorykey>>
|int
|NOT NULL
|

|
|<<column-shipdate>>
|datetime
|NULL
|

|
|<<column-shipdatekey>>
|int
|NOT NULL
|

|
|<<column-taxamt>>
|money
|NULL
|

|
|<<column-totalproductcost>>
|money
|NULL
|

|
|<<column-unitprice>>
|money
|NULL
|

|
|<<column-unitpricediscountpct>>
|float
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinefactresellersalesunderlinesalesordernumberunderlinesalesorderlinenumber]
=== PK_FactResellerSales_SalesOrderNumber_SalesOrderLineNumber

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-SalesOrderNumber>>; nvarchar(20)
* <<column-SalesOrderLineNumber>>; tinyint
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.factresellersales.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]
C06AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
nvarchar(20),tinyint
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
SalesOrderNumber,SalesOrderLineNumber
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  CarrierTrackingNumber : (nvarchar(25))
  - CurrencyKey : (int)
  CustomerPONumber : (nvarchar(25))
  DiscountAmount : (float)
  DueDate : (datetime)
  - DueDateKey : (int)
  - EmployeeKey : (int)
  ExtendedAmount : (money)
  Freight : (money)
  OrderDate : (datetime)
  - OrderDateKey : (int)
  OrderQuantity : (smallint)
  - ProductKey : (int)
  ProductStandardCost : (money)
  - PromotionKey : (int)
  - ResellerKey : (int)
  RevisionNumber : (tinyint)
  SalesAmount : (money)
  - SalesTerritoryKey : (int)
  ShipDate : (datetime)
  - ShipDateKey : (int)
  TaxAmt : (money)
  TotalProductCost : (money)
  UnitPrice : (money)
  UnitPriceDiscountPct : (float)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
**PK_FactResellerSales_SalesOrderNumber_SalesOrderLineNumber**

..
SalesOrderNumber; nvarchar(20)
SalesOrderLineNumber; tinyint
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.factresellersales.adoc[]" as dbo.FactResellerSales << U >> {
  - **SalesOrderNumber** : (nvarchar(20))
  - **SalesOrderLineNumber** : (tinyint)
  CarrierTrackingNumber : (nvarchar(25))
  - CurrencyKey : (int)
  CustomerPONumber : (nvarchar(25))
  DiscountAmount : (float)
  DueDate : (datetime)
  - DueDateKey : (int)
  - EmployeeKey : (int)
  ExtendedAmount : (money)
  Freight : (money)
  OrderDate : (datetime)
  - OrderDateKey : (int)
  OrderQuantity : (smallint)
  - ProductKey : (int)
  ProductStandardCost : (money)
  - PromotionKey : (int)
  - ResellerKey : (int)
  RevisionNumber : (tinyint)
  SalesAmount : (money)
  - SalesTerritoryKey : (int)
  ShipDate : (datetime)
  - ShipDateKey : (int)
  TaxAmt : (money)
  TotalProductCost : (money)
  UnitPrice : (money)
  UnitPriceDiscountPct : (float)
  --
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


