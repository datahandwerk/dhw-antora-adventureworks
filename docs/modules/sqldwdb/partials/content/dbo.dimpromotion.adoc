// tag::HeaderFullDisplayName[]
= dbo.DimPromotion - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
966AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.DimPromotion
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1173579219
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:33
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-promotionkey]
=== PromotionKey

[cols="d,8a,m,m,m"]
|===
|1
|*PromotionKey*
|int
|NOT NULL
|(1,1)
|===


[#column-discountpct]
=== DiscountPct

[cols="d,8a,m,m,m"]
|===
|
|DiscountPct
|float
|NULL
|
|===


[#column-enddate]
=== EndDate

[cols="d,8a,m,m,m"]
|===
|
|EndDate
|datetime
|NULL
|
|===


[#column-englishpromotioncategory]
=== EnglishPromotionCategory

[cols="d,8a,m,m,m"]
|===
|
|EnglishPromotionCategory
|nvarchar(50)
|NULL
|
|===


[#column-englishpromotionname]
=== EnglishPromotionName

[cols="d,8a,m,m,m"]
|===
|
|EnglishPromotionName
|nvarchar(255)
|NULL
|
|===


[#column-englishpromotiontype]
=== EnglishPromotionType

[cols="d,8a,m,m,m"]
|===
|
|EnglishPromotionType
|nvarchar(50)
|NULL
|
|===


[#column-frenchpromotioncategory]
=== FrenchPromotionCategory

[cols="d,8a,m,m,m"]
|===
|
|FrenchPromotionCategory
|nvarchar(50)
|NULL
|
|===


[#column-frenchpromotionname]
=== FrenchPromotionName

[cols="d,8a,m,m,m"]
|===
|
|FrenchPromotionName
|nvarchar(255)
|NULL
|
|===


[#column-frenchpromotiontype]
=== FrenchPromotionType

[cols="d,8a,m,m,m"]
|===
|
|FrenchPromotionType
|nvarchar(50)
|NULL
|
|===


[#column-maxqty]
=== MaxQty

[cols="d,8a,m,m,m"]
|===
|
|MaxQty
|int
|NULL
|
|===


[#column-minqty]
=== MinQty

[cols="d,8a,m,m,m"]
|===
|
|MinQty
|int
|NULL
|
|===


[#column-promotionalternatekey]
=== PromotionAlternateKey

[cols="d,8a,m,m,m"]
|===
|
|PromotionAlternateKey
|int
|NULL
|
|===


[#column-spanishpromotioncategory]
=== SpanishPromotionCategory

[cols="d,8a,m,m,m"]
|===
|
|SpanishPromotionCategory
|nvarchar(50)
|NULL
|
|===


[#column-spanishpromotionname]
=== SpanishPromotionName

[cols="d,8a,m,m,m"]
|===
|
|SpanishPromotionName
|nvarchar(255)
|NULL
|
|===


[#column-spanishpromotiontype]
=== SpanishPromotionType

[cols="d,8a,m,m,m"]
|===
|
|SpanishPromotionType
|nvarchar(50)
|NULL
|
|===


[#column-startdate]
=== StartDate

[cols="d,8a,m,m,m"]
|===
|
|StartDate
|datetime
|NOT NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-promotionkey>>*
|int
|NOT NULL
|(1,1)
















// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-discountpct>>
|float
|NULL
|

|
|<<column-enddate>>
|datetime
|NULL
|

|
|<<column-englishpromotioncategory>>
|nvarchar(50)
|NULL
|

|
|<<column-englishpromotionname>>
|nvarchar(255)
|NULL
|

|
|<<column-englishpromotiontype>>
|nvarchar(50)
|NULL
|

|
|<<column-frenchpromotioncategory>>
|nvarchar(50)
|NULL
|

|
|<<column-frenchpromotionname>>
|nvarchar(255)
|NULL
|

|
|<<column-frenchpromotiontype>>
|nvarchar(50)
|NULL
|

|
|<<column-maxqty>>
|int
|NULL
|

|
|<<column-minqty>>
|int
|NULL
|

|
|<<column-promotionalternatekey>>
|int
|NULL
|

|
|<<column-spanishpromotioncategory>>
|nvarchar(50)
|NULL
|

|
|<<column-spanishpromotionname>>
|nvarchar(255)
|NULL
|

|
|<<column-spanishpromotiontype>>
|nvarchar(50)
|NULL
|

|
|<<column-startdate>>
|datetime
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinedimpromotionunderlinepromotionkey]
=== PK_DimPromotion_PromotionKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-PromotionKey>>; int
--
* PK, Unique, Real: 1, 1, 1


[#index-akunderlinedimpromotionunderlinepromotionalternatekey]
=== AK_DimPromotion_PromotionAlternateKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-PromotionAlternateKey>>; int
--
* PK, Unique, Real: 0, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.dimpromotion.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]
B46AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
PromotionKey
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
  - **PromotionKey** : (int)
  DiscountPct : (float)
  EndDate : (datetime)
  EnglishPromotionCategory : (nvarchar(50))
  EnglishPromotionName : (nvarchar(255))
  EnglishPromotionType : (nvarchar(50))
  FrenchPromotionCategory : (nvarchar(50))
  FrenchPromotionName : (nvarchar(255))
  FrenchPromotionType : (nvarchar(50))
  MaxQty : (int)
  MinQty : (int)
  PromotionAlternateKey : (int)
  SpanishPromotionCategory : (nvarchar(50))
  SpanishPromotionName : (nvarchar(255))
  SpanishPromotionType : (nvarchar(50))
  - StartDate : (datetime)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
**PK_DimPromotion_PromotionKey**

..
PromotionKey; int
--
AK_DimPromotion_PromotionAlternateKey

..
PromotionAlternateKey; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
  - **PromotionKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
  - **PromotionKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
  - **PromotionKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimpromotion.adoc[]" as dbo.DimPromotion << U >> {
  - **PromotionKey** : (int)
  DiscountPct : (float)
  EndDate : (datetime)
  EnglishPromotionCategory : (nvarchar(50))
  EnglishPromotionName : (nvarchar(255))
  EnglishPromotionType : (nvarchar(50))
  FrenchPromotionCategory : (nvarchar(50))
  FrenchPromotionName : (nvarchar(255))
  FrenchPromotionType : (nvarchar(50))
  MaxQty : (int)
  MinQty : (int)
  PromotionAlternateKey : (int)
  SpanishPromotionCategory : (nvarchar(50))
  SpanishPromotionName : (nvarchar(255))
  SpanishPromotionType : (nvarchar(50))
  - StartDate : (datetime)
  --
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


