// tag::HeaderFullDisplayName[]
= dbo.DimEmployee - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
906AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.DimEmployee
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1077578877
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:34
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-employeekey]
=== EmployeeKey

[cols="d,8a,m,m,m"]
|===
|1
|*EmployeeKey*
|int
|NOT NULL
|(1,1)
|===


[#column-baserate]
=== BaseRate

[cols="d,8a,m,m,m"]
|===
|
|BaseRate
|money
|NULL
|
|===


[#column-birthdate]
=== BirthDate

[cols="d,8a,m,m,m"]
|===
|
|BirthDate
|date
|NULL
|
|===


[#column-currentflag]
=== CurrentFlag

[cols="d,8a,m,m,m"]
|===
|
|CurrentFlag
|bit
|NOT NULL
|
|===


[#column-departmentname]
=== DepartmentName

[cols="d,8a,m,m,m"]
|===
|
|DepartmentName
|nvarchar(50)
|NULL
|
|===


[#column-emailaddress]
=== EmailAddress

[cols="d,8a,m,m,m"]
|===
|
|EmailAddress
|nvarchar(50)
|NULL
|
|===


[#column-emergencycontactname]
=== EmergencyContactName

[cols="d,8a,m,m,m"]
|===
|
|EmergencyContactName
|nvarchar(50)
|NULL
|
|===


[#column-emergencycontactphone]
=== EmergencyContactPhone

[cols="d,8a,m,m,m"]
|===
|
|EmergencyContactPhone
|nvarchar(25)
|NULL
|
|===


[#column-employeenationalidalternatekey]
=== EmployeeNationalIDAlternateKey

[cols="d,8a,m,m,m"]
|===
|
|EmployeeNationalIDAlternateKey
|nvarchar(15)
|NULL
|
|===


[#column-employeephoto]
=== EmployeePhoto

[cols="d,8a,m,m,m"]
|===
|
|EmployeePhoto
|varbinary(max)
|NULL
|
|===


[#column-enddate]
=== EndDate

[cols="d,8a,m,m,m"]
|===
|
|EndDate
|date
|NULL
|
|===


[#column-firstname]
=== FirstName

[cols="d,8a,m,m,m"]
|===
|
|FirstName
|nvarchar(50)
|NOT NULL
|
|===


[#column-gender]
=== Gender

[cols="d,8a,m,m,m"]
|===
|
|Gender
|nchar(1)
|NULL
|
|===


[#column-hiredate]
=== HireDate

[cols="d,8a,m,m,m"]
|===
|
|HireDate
|date
|NULL
|
|===


[#column-lastname]
=== LastName

[cols="d,8a,m,m,m"]
|===
|
|LastName
|nvarchar(50)
|NOT NULL
|
|===


[#column-loginid]
=== LoginID

[cols="d,8a,m,m,m"]
|===
|
|LoginID
|nvarchar(256)
|NULL
|
|===


[#column-maritalstatus]
=== MaritalStatus

[cols="d,8a,m,m,m"]
|===
|
|MaritalStatus
|nchar(1)
|NULL
|
|===


[#column-middlename]
=== MiddleName

[cols="d,8a,m,m,m"]
|===
|
|MiddleName
|nvarchar(50)
|NULL
|
|===


[#column-namestyle]
=== NameStyle

[cols="d,8a,m,m,m"]
|===
|
|NameStyle
|bit
|NOT NULL
|
|===


[#column-parentemployeekey]
=== ParentEmployeeKey

[cols="d,8a,m,m,m"]
|===
|
|ParentEmployeeKey
|int
|NULL
|
|===


[#column-parentemployeenationalidalternatekey]
=== ParentEmployeeNationalIDAlternateKey

[cols="d,8a,m,m,m"]
|===
|
|ParentEmployeeNationalIDAlternateKey
|nvarchar(15)
|NULL
|
|===


[#column-payfrequency]
=== PayFrequency

[cols="d,8a,m,m,m"]
|===
|
|PayFrequency
|tinyint
|NULL
|
|===


[#column-phone]
=== Phone

[cols="d,8a,m,m,m"]
|===
|
|Phone
|nvarchar(25)
|NULL
|
|===


[#column-salariedflag]
=== SalariedFlag

[cols="d,8a,m,m,m"]
|===
|
|SalariedFlag
|bit
|NULL
|
|===


[#column-salespersonflag]
=== SalesPersonFlag

[cols="d,8a,m,m,m"]
|===
|
|SalesPersonFlag
|bit
|NOT NULL
|
|===


[#column-salesterritorykey]
=== SalesTerritoryKey

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryKey
|int
|NULL
|
|===


[#column-sickleavehours]
=== SickLeaveHours

[cols="d,8a,m,m,m"]
|===
|
|SickLeaveHours
|smallint
|NULL
|
|===


[#column-startdate]
=== StartDate

[cols="d,8a,m,m,m"]
|===
|
|StartDate
|date
|NULL
|
|===


[#column-status]
=== Status

[cols="d,8a,m,m,m"]
|===
|
|Status
|nvarchar(50)
|NULL
|
|===


[#column-title]
=== Title

[cols="d,8a,m,m,m"]
|===
|
|Title
|nvarchar(50)
|NULL
|
|===


[#column-vacationhours]
=== VacationHours

[cols="d,8a,m,m,m"]
|===
|
|VacationHours
|smallint
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-employeekey>>*
|int
|NOT NULL
|(1,1)































// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-baserate>>
|money
|NULL
|

|
|<<column-birthdate>>
|date
|NULL
|

|
|<<column-currentflag>>
|bit
|NOT NULL
|

|
|<<column-departmentname>>
|nvarchar(50)
|NULL
|

|
|<<column-emailaddress>>
|nvarchar(50)
|NULL
|

|
|<<column-emergencycontactname>>
|nvarchar(50)
|NULL
|

|
|<<column-emergencycontactphone>>
|nvarchar(25)
|NULL
|

|
|<<column-employeenationalidalternatekey>>
|nvarchar(15)
|NULL
|

|
|<<column-employeephoto>>
|varbinary(max)
|NULL
|

|
|<<column-enddate>>
|date
|NULL
|

|
|<<column-firstname>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-gender>>
|nchar(1)
|NULL
|

|
|<<column-hiredate>>
|date
|NULL
|

|
|<<column-lastname>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-loginid>>
|nvarchar(256)
|NULL
|

|
|<<column-maritalstatus>>
|nchar(1)
|NULL
|

|
|<<column-middlename>>
|nvarchar(50)
|NULL
|

|
|<<column-namestyle>>
|bit
|NOT NULL
|

|
|<<column-parentemployeekey>>
|int
|NULL
|

|
|<<column-parentemployeenationalidalternatekey>>
|nvarchar(15)
|NULL
|

|
|<<column-payfrequency>>
|tinyint
|NULL
|

|
|<<column-phone>>
|nvarchar(25)
|NULL
|

|
|<<column-salariedflag>>
|bit
|NULL
|

|
|<<column-salespersonflag>>
|bit
|NOT NULL
|

|
|<<column-salesterritorykey>>
|int
|NULL
|

|
|<<column-sickleavehours>>
|smallint
|NULL
|

|
|<<column-startdate>>
|date
|NULL
|

|
|<<column-status>>
|nvarchar(50)
|NULL
|

|
|<<column-title>>
|nvarchar(50)
|NULL
|

|
|<<column-vacationhours>>
|smallint
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinedimemployeeunderlineemployeekey]
=== PK_DimEmployee_EmployeeKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-EmployeeKey>>; int
--
* PK, Unique, Real: 1, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.dimemployee.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]
AE6AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
EmployeeKey
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
  - **EmployeeKey** : (int)
  BaseRate : (money)
  BirthDate : (date)
  - CurrentFlag : (bit)
  DepartmentName : (nvarchar(50))
  EmailAddress : (nvarchar(50))
  EmergencyContactName : (nvarchar(50))
  EmergencyContactPhone : (nvarchar(25))
  EmployeeNationalIDAlternateKey : (nvarchar(15))
  EmployeePhoto : (varbinary(max))
  EndDate : (date)
  - FirstName : (nvarchar(50))
  Gender : (nchar(1))
  HireDate : (date)
  - LastName : (nvarchar(50))
  LoginID : (nvarchar(256))
  MaritalStatus : (nchar(1))
  MiddleName : (nvarchar(50))
  - NameStyle : (bit)
  ParentEmployeeKey : (int)
  ParentEmployeeNationalIDAlternateKey : (nvarchar(15))
  PayFrequency : (tinyint)
  Phone : (nvarchar(25))
  SalariedFlag : (bit)
  - SalesPersonFlag : (bit)
  SalesTerritoryKey : (int)
  SickLeaveHours : (smallint)
  StartDate : (date)
  Status : (nvarchar(50))
  Title : (nvarchar(50))
  VacationHours : (smallint)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
**PK_DimEmployee_EmployeeKey**

..
EmployeeKey; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
  - **EmployeeKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
  - **EmployeeKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
  - **EmployeeKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimemployee.adoc[]" as dbo.DimEmployee << U >> {
  - **EmployeeKey** : (int)
  BaseRate : (money)
  BirthDate : (date)
  - CurrentFlag : (bit)
  DepartmentName : (nvarchar(50))
  EmailAddress : (nvarchar(50))
  EmergencyContactName : (nvarchar(50))
  EmergencyContactPhone : (nvarchar(25))
  EmployeeNationalIDAlternateKey : (nvarchar(15))
  EmployeePhoto : (varbinary(max))
  EndDate : (date)
  - FirstName : (nvarchar(50))
  Gender : (nchar(1))
  HireDate : (date)
  - LastName : (nvarchar(50))
  LoginID : (nvarchar(256))
  MaritalStatus : (nchar(1))
  MiddleName : (nvarchar(50))
  - NameStyle : (bit)
  ParentEmployeeKey : (int)
  ParentEmployeeNationalIDAlternateKey : (nvarchar(15))
  PayFrequency : (tinyint)
  Phone : (nvarchar(25))
  SalariedFlag : (bit)
  - SalesPersonFlag : (bit)
  SalesTerritoryKey : (int)
  SickLeaveHours : (smallint)
  StartDate : (date)
  Status : (nvarchar(50))
  Title : (nvarchar(50))
  VacationHours : (smallint)
  --
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


