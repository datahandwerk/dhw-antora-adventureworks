// tag::HeaderFullDisplayName[]
= dbo.DimSalesTerritory - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencinglist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
996AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.DimSalesTerritory
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1221579390
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:33
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-salesterritorykey]
=== SalesTerritoryKey

[cols="d,8a,m,m,m"]
|===
|1
|*SalesTerritoryKey*
|int
|NOT NULL
|(1,1)
|===


[#column-salesterritoryalternatekey]
=== SalesTerritoryAlternateKey

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryAlternateKey
|int
|NULL
|
|===


[#column-salesterritorycountry]
=== SalesTerritoryCountry

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryCountry
|nvarchar(50)
|NOT NULL
|
|===


[#column-salesterritorygroup]
=== SalesTerritoryGroup

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryGroup
|nvarchar(50)
|NULL
|
|===

.Referencing Columns
--
* xref:dbo.vdmprep.adoc#column-region[+dbo.vDMPrep.Region+]
--


[#column-salesterritoryimage]
=== SalesTerritoryImage

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryImage
|varbinary(max)
|NULL
|
|===


[#column-salesterritoryregion]
=== SalesTerritoryRegion

[cols="d,8a,m,m,m"]
|===
|
|SalesTerritoryRegion
|nvarchar(50)
|NOT NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-salesterritorykey>>*
|int
|NOT NULL
|(1,1)






// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-salesterritoryalternatekey>>
|int
|NULL
|

|
|<<column-salesterritorycountry>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-salesterritorygroup>>
|nvarchar(50)
|NULL
|

|
|<<column-salesterritoryimage>>
|varbinary(max)
|NULL
|

|
|<<column-salesterritoryregion>>
|nvarchar(50)
|NOT NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinedimsalesterritoryunderlinesalesterritorykey]
=== PK_DimSalesTerritory_SalesTerritoryKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-SalesTerritoryKey>>; int
--
* PK, Unique, Real: 1, 1, 1


[#index-akunderlinedimsalesterritoryunderlinesalesterritoryalternatekey]
=== AK_DimSalesTerritory_SalesTerritoryAlternateKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-SalesTerritoryAlternateKey>>; int
--
* PK, Unique, Real: 0, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.dimsalesterritory.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]
* xref:aw:sqldwdb:dbo.vdmprep.adoc[]
// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]
B86AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
SalesTerritoryKey
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  SalesTerritoryAlternateKey : (int)
  - SalesTerritoryCountry : (nvarchar(50))
  SalesTerritoryGroup : (nvarchar(50))
  SalesTerritoryImage : (varbinary(max))
  - SalesTerritoryRegion : (nvarchar(50))
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
**PK_DimSalesTerritory_SalesTerritoryKey**

..
SalesTerritoryKey; int
--
AK_DimSalesTerritory_SalesTerritoryAlternateKey

..
SalesTerritoryAlternateKey; int
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

dbo.DimSalesTerritory <.. dbo.vDMPrep

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqlineitems.adoc[]" as dbo.vAssocSeqLineItems << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vassocseqorders.adoc[]" as dbo.vAssocSeqOrders << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtargetmail.adoc[]" as dbo.vTargetMail << V >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.vtimeseries.adoc[]" as dbo.vTimeSeries << V >> {
  --
}

dbo.DimSalesTerritory <.. dbo.vDMPrep
dbo.vDMPrep <.. dbo.vAssocSeqLineItems
dbo.vDMPrep <.. dbo.vAssocSeqOrders
dbo.vDMPrep <.. dbo.vTargetMail
dbo.vDMPrep <.. dbo.vTimeSeries

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimsalesterritory.adoc[]" as dbo.DimSalesTerritory << U >> {
  - **SalesTerritoryKey** : (int)
  SalesTerritoryAlternateKey : (int)
  - SalesTerritoryCountry : (nvarchar(50))
  SalesTerritoryGroup : (nvarchar(50))
  SalesTerritoryImage : (varbinary(max))
  - SalesTerritoryRegion : (nvarchar(50))
  --
}

entity "puml-link:aw:sqldwdb:dbo.vdmprep.adoc[]" as dbo.vDMPrep << V >> {
  Age : (int)
  - Amount : (money)
  - CalendarYear : (smallint)
  - CustomerKey : (int)
  - EnglishProductCategoryName : (nvarchar(50))
  - FiscalYear : (smallint)
  - IncomeGroup : (varchar(8))
  - LineNumber : (tinyint)
  Model : (nvarchar(50))
  - Month : (tinyint)
  - OrderNumber : (nvarchar(20))
  - Quantity : (smallint)
  Region : (nvarchar(50))
  --
}

dbo.DimSalesTerritory <.. dbo.vDMPrep
"dbo.DimSalesTerritory::SalesTerritoryGroup" <-- "dbo.vDMPrep::Region"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


