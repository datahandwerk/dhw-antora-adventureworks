// tag::HeaderFullDisplayName[]
= dbo.DimReseller - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
976AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
dbo.DimReseller
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]
1189579276
// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]
2017-10-27 14:36:33
// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-resellerkey]
=== ResellerKey

[cols="d,8a,m,m,m"]
|===
|1
|*ResellerKey*
|int
|NOT NULL
|(1,1)
|===


[#column-addressline1]
=== AddressLine1

[cols="d,8a,m,m,m"]
|===
|
|AddressLine1
|nvarchar(60)
|NULL
|
|===


[#column-addressline2]
=== AddressLine2

[cols="d,8a,m,m,m"]
|===
|
|AddressLine2
|nvarchar(60)
|NULL
|
|===


[#column-annualrevenue]
=== AnnualRevenue

[cols="d,8a,m,m,m"]
|===
|
|AnnualRevenue
|money
|NULL
|
|===


[#column-annualsales]
=== AnnualSales

[cols="d,8a,m,m,m"]
|===
|
|AnnualSales
|money
|NULL
|
|===


[#column-bankname]
=== BankName

[cols="d,8a,m,m,m"]
|===
|
|BankName
|nvarchar(50)
|NULL
|
|===


[#column-businesstype]
=== BusinessType

[cols="d,8a,m,m,m"]
|===
|
|BusinessType
|varchar(20)
|NOT NULL
|
|===


[#column-firstorderyear]
=== FirstOrderYear

[cols="d,8a,m,m,m"]
|===
|
|FirstOrderYear
|int
|NULL
|
|===


[#column-geographykey]
=== GeographyKey

[cols="d,8a,m,m,m"]
|===
|
|GeographyKey
|int
|NULL
|
|===


[#column-lastorderyear]
=== LastOrderYear

[cols="d,8a,m,m,m"]
|===
|
|LastOrderYear
|int
|NULL
|
|===


[#column-minpaymentamount]
=== MinPaymentAmount

[cols="d,8a,m,m,m"]
|===
|
|MinPaymentAmount
|money
|NULL
|
|===


[#column-minpaymenttype]
=== MinPaymentType

[cols="d,8a,m,m,m"]
|===
|
|MinPaymentType
|tinyint
|NULL
|
|===


[#column-numberemployees]
=== NumberEmployees

[cols="d,8a,m,m,m"]
|===
|
|NumberEmployees
|int
|NULL
|
|===


[#column-orderfrequency]
=== OrderFrequency

[cols="d,8a,m,m,m"]
|===
|
|OrderFrequency
|char(1)
|NULL
|
|===


[#column-ordermonth]
=== OrderMonth

[cols="d,8a,m,m,m"]
|===
|
|OrderMonth
|tinyint
|NULL
|
|===


[#column-phone]
=== Phone

[cols="d,8a,m,m,m"]
|===
|
|Phone
|nvarchar(25)
|NULL
|
|===


[#column-productline]
=== ProductLine

[cols="d,8a,m,m,m"]
|===
|
|ProductLine
|nvarchar(50)
|NULL
|
|===


[#column-reselleralternatekey]
=== ResellerAlternateKey

[cols="d,8a,m,m,m"]
|===
|
|ResellerAlternateKey
|nvarchar(15)
|NULL
|
|===


[#column-resellername]
=== ResellerName

[cols="d,8a,m,m,m"]
|===
|
|ResellerName
|nvarchar(50)
|NOT NULL
|
|===


[#column-yearopened]
=== YearOpened

[cols="d,8a,m,m,m"]
|===
|
|YearOpened
|int
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
|1
|*<<column-resellerkey>>*
|int
|NOT NULL
|(1,1)




















// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]

|
|<<column-addressline1>>
|nvarchar(60)
|NULL
|

|
|<<column-addressline2>>
|nvarchar(60)
|NULL
|

|
|<<column-annualrevenue>>
|money
|NULL
|

|
|<<column-annualsales>>
|money
|NULL
|

|
|<<column-bankname>>
|nvarchar(50)
|NULL
|

|
|<<column-businesstype>>
|varchar(20)
|NOT NULL
|

|
|<<column-firstorderyear>>
|int
|NULL
|

|
|<<column-geographykey>>
|int
|NULL
|

|
|<<column-lastorderyear>>
|int
|NULL
|

|
|<<column-minpaymentamount>>
|money
|NULL
|

|
|<<column-minpaymenttype>>
|tinyint
|NULL
|

|
|<<column-numberemployees>>
|int
|NULL
|

|
|<<column-orderfrequency>>
|char(1)
|NULL
|

|
|<<column-ordermonth>>
|tinyint
|NULL
|

|
|<<column-phone>>
|nvarchar(25)
|NULL
|

|
|<<column-productline>>
|nvarchar(50)
|NULL
|

|
|<<column-reselleralternatekey>>
|nvarchar(15)
|NULL
|

|
|<<column-resellername>>
|nvarchar(50)
|NOT NULL
|

|
|<<column-yearopened>>
|int
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-pkunderlinedimresellerunderlineresellerkey]
=== PK_DimReseller_ResellerKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-ResellerKey>>; int
--
* PK, Unique, Real: 1, 1, 1


[#index-akunderlinedimresellerunderlinereselleralternatekey]
=== AK_DimReseller_ResellerAlternateKey

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-ResellerAlternateKey>>; nvarchar(15)
--
* PK, Unique, Real: 0, 1, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions



== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:sqldwdb:dbo.dimreseller.adoc[] - 
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
1
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
0
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
0
// end::is_ssas[]


=== microsoft_database_tools_support

// tag::microsoft_database_tools_support[]

// end::microsoft_database_tools_support[]


=== pk_index_guid

// tag::pk_index_guid[]
B56AB73D-AE4A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
int
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
ResellerKey
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]


// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
  - **ResellerKey** : (int)
  AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  AnnualRevenue : (money)
  AnnualSales : (money)
  BankName : (nvarchar(50))
  - BusinessType : (varchar(20))
  FirstOrderYear : (int)
  GeographyKey : (int)
  LastOrderYear : (int)
  MinPaymentAmount : (money)
  MinPaymentType : (tinyint)
  NumberEmployees : (int)
  OrderFrequency : (char(1))
  OrderMonth : (tinyint)
  Phone : (nvarchar(25))
  ProductLine : (nvarchar(50))
  ResellerAlternateKey : (nvarchar(15))
  - ResellerName : (nvarchar(50))
  YearOpened : (int)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
**PK_DimReseller_ResellerKey**

..
ResellerKey; int
--
AK_DimReseller_ResellerAlternateKey

..
ResellerAlternateKey; nvarchar(15)
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
  - **ResellerKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
  - **ResellerKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
  - **ResellerKey** : (int)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:sqldwdb:dbo.dimreseller.adoc[]" as dbo.DimReseller << U >> {
  - **ResellerKey** : (int)
  AddressLine1 : (nvarchar(60))
  AddressLine2 : (nvarchar(60))
  AnnualRevenue : (money)
  AnnualSales : (money)
  BankName : (nvarchar(50))
  - BusinessType : (varchar(20))
  FirstOrderYear : (int)
  GeographyKey : (int)
  LastOrderYear : (int)
  MinPaymentAmount : (money)
  MinPaymentType : (tinyint)
  NumberEmployees : (int)
  OrderFrequency : (char(1))
  OrderMonth : (tinyint)
  Phone : (nvarchar(25))
  ProductLine : (nvarchar(50))
  ResellerAlternateKey : (nvarchar(15))
  - ResellerName : (nvarchar(50))
  YearOpened : (int)
  --
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


