= AdventureWorks.Customer

// tag::description[]

// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description:
// end::description[]

== Measures


=== Days Current Quarter to Date

// tag::description-measure-daysblankcurrentblankquarterblanktoblankdate[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-daysblankcurrentblankquarterblanktoblankdate:
// end::description-measure-daysblankcurrentblankquarterblanktoblankdate[]


=== Days in Current Quarter

// tag::description-measure-daysblankinblankcurrentblankquarter[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-daysblankinblankcurrentblankquarter:
// end::description-measure-daysblankinblankcurrentblankquarter[]


=== Internet Current Quarter Margin

// tag::description-measure-internetblankcurrentblankquarterblankmargin[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankcurrentblankquarterblankmargin:
// end::description-measure-internetblankcurrentblankquarterblankmargin[]


=== Internet Current Quarter Margin Performance

// tag::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankcurrentblankquarterblankmarginblankperformance:
// end::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]


=== Internet Current Quarter Sales

// tag::description-measure-internetblankcurrentblankquarterblanksales[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankcurrentblankquarterblanksales:
// end::description-measure-internetblankcurrentblankquarterblanksales[]


=== Internet Current Quarter Sales Performance

// tag::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankcurrentblankquarterblanksalesblankperformance:
// end::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]


=== Internet Distinct Count Sales Order

// tag::description-measure-internetblankdistinctblankcountblanksalesblankorder[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankdistinctblankcountblanksalesblankorder:
// end::description-measure-internetblankdistinctblankcountblanksalesblankorder[]


=== Internet Order Lines Count

// tag::description-measure-internetblankorderblanklinesblankcount[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankorderblanklinesblankcount:
// end::description-measure-internetblankorderblanklinesblankcount[]


=== Internet Previous Quarter Margin

// tag::description-measure-internetblankpreviousblankquarterblankmargin[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankpreviousblankquarterblankmargin:
// end::description-measure-internetblankpreviousblankquarterblankmargin[]


=== Internet Previous Quarter Margin Proportion to QTD

// tag::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd:
// end::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]


=== Internet Previous Quarter Sales

// tag::description-measure-internetblankpreviousblankquarterblanksales[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankpreviousblankquarterblanksales:
// end::description-measure-internetblankpreviousblankquarterblanksales[]


=== Internet Previous Quarter Sales Proportion to QTD

// tag::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd:
// end::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]


=== Internet Total Discount Amount

// tag::description-measure-internetblanktotalblankdiscountblankamount[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblankdiscountblankamount:
// end::description-measure-internetblanktotalblankdiscountblankamount[]


=== Internet Total Freight

// tag::description-measure-internetblanktotalblankfreight[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblankfreight:
// end::description-measure-internetblanktotalblankfreight[]


=== Internet Total Margin

// tag::description-measure-internetblanktotalblankmargin[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblankmargin:
// end::description-measure-internetblanktotalblankmargin[]


=== Internet Total Product Cost

// tag::description-measure-internetblanktotalblankproductblankcost[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblankproductblankcost:
// end::description-measure-internetblanktotalblankproductblankcost[]


=== Internet Total Sales

// tag::description-measure-internetblanktotalblanksales[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblanksales:
// end::description-measure-internetblanktotalblanksales[]


=== Internet Total Tax Amt

// tag::description-measure-internetblanktotalblanktaxblankamt[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblanktaxblankamt:
// end::description-measure-internetblanktotalblanktaxblankamt[]


=== Internet Total Units

// tag::description-measure-internetblanktotalblankunits[]


// uncomment the following attribute, to hide exported (by AntoraExport) descriptions. Keep the empty line on top of the attribute!

//:hide-exported-description-measure-internetblanktotalblankunits:
// end::description-measure-internetblanktotalblankunits[]

