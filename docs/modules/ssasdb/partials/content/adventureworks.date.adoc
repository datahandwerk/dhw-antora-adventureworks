// tag::HeaderFullDisplayName[]
= AdventureWorks.Date - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--pk_index_guid:
:ExistsProperty--pk_indexpatterncolumndatatype:
:ExistsProperty--pk_indexpatterncolumnname:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
:ExistsProperty--Measures:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
AED25B38-184A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
AdventureWorks.Date
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]

// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]

// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-calendarblankquarter]
=== Calendar Quarter

[cols="d,8a,m,m,m"]
|===
|
|Calendar Quarter
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-calendarquarter[+dbo.DimDate.CalendarQuarter+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-calendarblanksemester]
=== Calendar Semester

[cols="d,8a,m,m,m"]
|===
|
|Calendar Semester
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-calendarsemester[+dbo.DimDate.CalendarSemester+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-calendarblankyear]
=== Calendar Year

[cols="d,8a,m,m,m"]
|===
|
|Calendar Year
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-calendaryear[+dbo.DimDate.CalendarYear+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-dayblankname]
=== Day Name

[cols="d,8a,m,m,m"]
|===
|
|Day Name
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-englishdaynameofweek[+dbo.DimDate.EnglishDayNameOfWeek+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-dayblanknumberblankofblankweek]
=== Day Number of Week

[cols="d,8a,m,m,m"]
|===
|
|Day Number of Week
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-daynumberofweek[+dbo.DimDate.DayNumberOfWeek+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-dayblankofblankmonth]
=== Day of Month

[cols="d,8a,m,m,m"]
|===
|
|Day of Month
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-daynumberofmonth[+dbo.DimDate.DayNumberOfMonth+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-dayblankofblankweek]
=== Day of Week

[cols="d,8a,m,m,m"]
|===
|
|Day of Week
|string
|NULL
|
|===


[#column-dayblankofblankyear]
=== Day of Year

[cols="d,8a,m,m,m"]
|===
|
|Day of Year
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-daynumberofyear[+dbo.DimDate.DayNumberOfYear+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-fiscalblankquarter]
=== Fiscal Quarter

[cols="d,8a,m,m,m"]
|===
|
|Fiscal Quarter
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-fiscalquarter[+dbo.DimDate.FiscalQuarter+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-fiscalblanksemester]
=== Fiscal Semester

[cols="d,8a,m,m,m"]
|===
|
|Fiscal Semester
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-fiscalsemester[+dbo.DimDate.FiscalSemester+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-fiscalblankyear]
=== Fiscal Year

[cols="d,8a,m,m,m"]
|===
|
|Fiscal Year
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-fiscalyear[+dbo.DimDate.FiscalYear+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-month]
=== Month

[cols="d,8a,m,m,m"]
|===
|
|Month
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-monthnumberofyear[+dbo.DimDate.MonthNumberOfYear+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-monthblankcalendar]
=== Month Calendar

[cols="d,8a,m,m,m"]
|===
|
|Month Calendar
|string
|NULL
|
|===


[#column-monthblankname]
=== Month Name

[cols="d,8a,m,m,m"]
|===
|
|Month Name
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-englishmonthname[+dbo.DimDate.EnglishMonthName+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-weekblanknumberblankofblankyear]
=== Week Number of Year

[cols="d,8a,m,m,m"]
|===
|
|Week Number of Year
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-weeknumberofyear[+dbo.DimDate.WeekNumberOfYear+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[discrete]
== Datum_aaa


[#column-date]
=== Date

[cols="d,8a,m,m,m"]
|===
|1
|*Date*
|dateTime
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimdate.adoc#column-fulldatealternatekey[+dbo.DimDate.FullDateAlternateKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]















|1
|*<<column-date>>*
|dateTime
|NULL
|

// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-calendarblankquarter>>
|int64
|NULL
|

|
|<<column-calendarblanksemester>>
|int64
|NULL
|

|
|<<column-calendarblankyear>>
|int64
|NULL
|

|
|<<column-dayblankname>>
|string
|NULL
|

|
|<<column-dayblanknumberblankofblankweek>>
|int64
|NULL
|

|
|<<column-dayblankofblankmonth>>
|int64
|NULL
|

|
|<<column-dayblankofblankweek>>
|string
|NULL
|

|
|<<column-dayblankofblankyear>>
|int64
|NULL
|

|
|<<column-fiscalblankquarter>>
|int64
|NULL
|

|
|<<column-fiscalblanksemester>>
|int64
|NULL
|

|
|<<column-fiscalblankyear>>
|int64
|NULL
|

|
|<<column-month>>
|int64
|NULL
|

|
|<<column-monthblankcalendar>>
|string
|NULL
|

|
|<<column-monthblankname>>
|string
|NULL
|

|
|<<column-weekblanknumberblankofblankyear>>
|int64
|NULL
|


// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-keyunderlinedateunderlinecolunderlinedate]
=== key_Date_col_Date

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Date>>; dateTime
--
* PK, Unique, Real: 1, 0, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]
[discrete]
== _


[#measure-daysblankcurrentblankquarterblanktoblankdate]
=== Days Current Quarter to Date

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-daysblankcurrentblankquarterblanktoblankdate,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-daysblankcurrentblankquarterblanktoblankdate[]

include::partial$adventureworks._measures.adoc[tag=description-measure-daysblankcurrentblankquarterblanktoblankdate,opts=optional]

endif::hide-exported-description-measure-daysblankcurrentblankquarterblanktoblankdate[]

--
{empty} +

.Expression
....
COUNTROWS( DATESQTD( 'Date'[Date]))
....




[#measure-daysblankinblankcurrentblankquarter]
=== Days in Current Quarter

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-daysblankinblankcurrentblankquarter,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-daysblankinblankcurrentblankquarter[]

include::partial$adventureworks._measures.adoc[tag=description-measure-daysblankinblankcurrentblankquarter,opts=optional]

endif::hide-exported-description-measure-daysblankinblankcurrentblankquarter[]

--
{empty} +

.Expression
....
COUNTROWS( DATESBETWEEN( 'Date'[Date], STARTOFQUARTER( LASTDATE('Date'[Date])), ENDOFQUARTER('Date'[Date])))
....




// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions


=== Days Current Quarter to Date - description

// tag::description-measure-daysblankcurrentblankquarterblanktoblankdate[]



// end::description-measure-daysblankcurrentblankquarterblanktoblankdate[]

=== Days in Current Quarter - description

// tag::description-measure-daysblankinblankcurrentblankquarter[]



// end::description-measure-daysblankinblankcurrentblankquarter[]

=== Internet Current Quarter Margin - description

// tag::description-measure-internetblankcurrentblankquarterblankmargin[]



// end::description-measure-internetblankcurrentblankquarterblankmargin[]

=== Internet Current Quarter Margin Performance - description

// tag::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

=== Internet Current Quarter Sales - description

// tag::description-measure-internetblankcurrentblankquarterblanksales[]



// end::description-measure-internetblankcurrentblankquarterblanksales[]

=== Internet Current Quarter Sales Performance - description

// tag::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

=== Internet Distinct Count Sales Order - description

// tag::description-measure-internetblankdistinctblankcountblanksalesblankorder[]



// end::description-measure-internetblankdistinctblankcountblanksalesblankorder[]

=== Internet Order Lines Count - description

// tag::description-measure-internetblankorderblanklinesblankcount[]



// end::description-measure-internetblankorderblanklinesblankcount[]

=== Internet Previous Quarter Margin - description

// tag::description-measure-internetblankpreviousblankquarterblankmargin[]



// end::description-measure-internetblankpreviousblankquarterblankmargin[]

=== Internet Previous Quarter Margin Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

=== Internet Previous Quarter Sales - description

// tag::description-measure-internetblankpreviousblankquarterblanksales[]



// end::description-measure-internetblankpreviousblankquarterblanksales[]

=== Internet Previous Quarter Sales Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

=== Internet Total Discount Amount - description

// tag::description-measure-internetblanktotalblankdiscountblankamount[]



// end::description-measure-internetblanktotalblankdiscountblankamount[]

=== Internet Total Freight - description

// tag::description-measure-internetblanktotalblankfreight[]



// end::description-measure-internetblanktotalblankfreight[]

=== Internet Total Margin - description

// tag::description-measure-internetblanktotalblankmargin[]



// end::description-measure-internetblanktotalblankmargin[]

=== Internet Total Product Cost - description

// tag::description-measure-internetblanktotalblankproductblankcost[]



// end::description-measure-internetblanktotalblankproductblankcost[]

=== Internet Total Sales - description

// tag::description-measure-internetblanktotalblanksales[]



// end::description-measure-internetblanktotalblanksales[]

=== Internet Total Tax Amt - description

// tag::description-measure-internetblanktotalblanktaxblankamt[]



// end::description-measure-internetblanktotalblanktaxblankamt[]

=== Internet Total Units - description

// tag::description-measure-internetblanktotalblankunits[]



// end::description-measure-internetblanktotalblankunits[]

== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:ssasdb:adventureworks.date.adoc[] - 
* xref:aw:ssasdb-de-DE:adventureworks.date.adoc[] - de-DE
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
2
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldwdb:dbo.dimdate.adoc[] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
1
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
1
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]
49D35B38-184A-EC11-8531-A81E8446D5B0
// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]
dateTime
// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]
Date
// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]

:is_repo_managed:
:is_ssas:

// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
  Calendar_Quarter : (int64)
  Calendar_Semester : (int64)
  Calendar_Year : (int64)
  Day_Name : (string)
  Day_Number_of_Week : (int64)
  Day_of_Month : (int64)
  Day_of_Week : (string)
  Day_of_Year : (int64)
  Fiscal_Quarter : (int64)
  Fiscal_Semester : (int64)
  Fiscal_Year : (int64)
  Month : (int64)
  Month_Calendar : (string)
  Month_Name : (string)
  Week_Number_of_Year : (int64)
  **Datum_aaa**
  **Date** : (dateTime)
  --
  ~ Days_Current_Quarter_to_Date
  ~ Days_in_Current_Quarter
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
**key_Date_col_Date**

..
Date; dateTime
}

entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
ix_Internet_Sales_col_Customer_Id

..
Customer Id; int64
--
ix_Internet_Sales_col_Due_Date

..
Due Date; dateTime
--
ix_Internet_Sales_col_Order_Date

..
Order Date; dateTime
--
ix_Internet_Sales_col_Product_Id

..
Product Id; int64
--
ix_Internet_Sales_col_Ship_Date

..
Ship Date; dateTime
}

"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Due_Date"
"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Order_Date"
"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Ship_Date"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
  **Datum_aaa**
  **Date** : (dateTime)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as aw.sqldwdb.dbo.DimDate <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimDate <.. AdventureWorks.Date

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
  **Datum_aaa**
  **Date** : (dateTime)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as aw.sqldwdb.dbo.DimDate <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimDate <.. AdventureWorks.Date

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
  **Datum_aaa**
  **Date** : (dateTime)
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
  Calendar_Quarter : (int64)
  Calendar_Semester : (int64)
  Calendar_Year : (int64)
  Day_Name : (string)
  Day_Number_of_Week : (int64)
  Day_of_Month : (int64)
  Day_of_Week : (string)
  Day_of_Year : (int64)
  Fiscal_Quarter : (int64)
  Fiscal_Semester : (int64)
  Fiscal_Year : (int64)
  Month : (int64)
  Month_Calendar : (string)
  Month_Name : (string)
  Week_Number_of_Year : (int64)
  **Datum_aaa**
  **Date** : (dateTime)
  --
  ~ Days_Current_Quarter_to_Date
  ~ Days_in_Current_Quarter
}

entity "puml-link:aw:sqldwdb:dbo.dimdate.adoc[]" as aw.sqldwdb.dbo.DimDate <AdventureWorksDW2019>  << external >> #line.dashed {
  CalendarQuarter
  CalendarSemester
  CalendarYear
  DayNumberOfMonth
  DayNumberOfWeek
  DayNumberOfYear
  EnglishDayNameOfWeek
  EnglishMonthName
  FiscalQuarter
  FiscalSemester
  FiscalYear
  FullDateAlternateKey
  MonthNumberOfYear
  WeekNumberOfYear
  --
}

aw.sqldwdb.dbo.DimDate <.. AdventureWorks.Date
"aw.sqldwdb.dbo.DimDate::CalendarQuarter" <-- "AdventureWorks.Date::Calendar_Quarter"
"aw.sqldwdb.dbo.DimDate::CalendarSemester" <-- "AdventureWorks.Date::Calendar_Semester"
"aw.sqldwdb.dbo.DimDate::CalendarYear" <-- "AdventureWorks.Date::Calendar_Year"
"aw.sqldwdb.dbo.DimDate::DayNumberOfMonth" <-- "AdventureWorks.Date::Day_of_Month"
"aw.sqldwdb.dbo.DimDate::DayNumberOfWeek" <-- "AdventureWorks.Date::Day_Number_of_Week"
"aw.sqldwdb.dbo.DimDate::DayNumberOfYear" <-- "AdventureWorks.Date::Day_of_Year"
"aw.sqldwdb.dbo.DimDate::EnglishDayNameOfWeek" <-- "AdventureWorks.Date::Day_Name"
"aw.sqldwdb.dbo.DimDate::EnglishMonthName" <-- "AdventureWorks.Date::Month_Name"
"aw.sqldwdb.dbo.DimDate::FiscalQuarter" <-- "AdventureWorks.Date::Fiscal_Quarter"
"aw.sqldwdb.dbo.DimDate::FiscalSemester" <-- "AdventureWorks.Date::Fiscal_Semester"
"aw.sqldwdb.dbo.DimDate::FiscalYear" <-- "AdventureWorks.Date::Fiscal_Year"
"aw.sqldwdb.dbo.DimDate::FullDateAlternateKey" <-- "AdventureWorks.Date::Date"
"aw.sqldwdb.dbo.DimDate::MonthNumberOfYear" <-- "AdventureWorks.Date::Month"
"aw.sqldwdb.dbo.DimDate::WeekNumberOfYear" <-- "AdventureWorks.Date::Week_Number_of_Year"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


