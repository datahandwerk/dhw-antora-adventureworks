// tag::HeaderFullDisplayName[]
= AdventureWorks.Product Category - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
B2D25B38-184A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
AdventureWorks.Product Category
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]

// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]

// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-productblankcategoryblankalternateblankid]
=== Product Category Alternate Id

[cols="d,8a,m,m,m"]
|===
|
|Product Category Alternate Id
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproductcategory.adoc#column-productcategoryalternatekey[+dbo.DimProductCategory.ProductCategoryAlternateKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankcategoryblankid]
=== Product Category Id

[cols="d,8a,m,m,m"]
|===
|
|Product Category Id
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproductcategory.adoc#column-productcategorykey[+dbo.DimProductCategory.ProductCategoryKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankcategoryblankname]
=== Product Category Name

[cols="d,8a,m,m,m"]
|===
|
|Product Category Name
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproductcategory.adoc#column-englishproductcategoryname[+dbo.DimProductCategory.EnglishProductCategoryName+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]



// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-productblankcategoryblankalternateblankid>>
|int64
|NULL
|

|
|<<column-productblankcategoryblankid>>
|int64
|NULL
|

|
|<<column-productblankcategoryblankname>>
|string
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-ixunderlineproductblankcategoryunderlinecolunderlineproductblankcategoryblankid]
=== ix_Product Category_col_Product Category Id

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Product Category Id>>; int64
--
* PK, Unique, Real: 0, 0, 1

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions


=== Days Current Quarter to Date - description

// tag::description-measure-daysblankcurrentblankquarterblanktoblankdate[]



// end::description-measure-daysblankcurrentblankquarterblanktoblankdate[]

=== Days in Current Quarter - description

// tag::description-measure-daysblankinblankcurrentblankquarter[]



// end::description-measure-daysblankinblankcurrentblankquarter[]

=== Internet Current Quarter Margin - description

// tag::description-measure-internetblankcurrentblankquarterblankmargin[]



// end::description-measure-internetblankcurrentblankquarterblankmargin[]

=== Internet Current Quarter Margin Performance - description

// tag::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

=== Internet Current Quarter Sales - description

// tag::description-measure-internetblankcurrentblankquarterblanksales[]



// end::description-measure-internetblankcurrentblankquarterblanksales[]

=== Internet Current Quarter Sales Performance - description

// tag::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

=== Internet Distinct Count Sales Order - description

// tag::description-measure-internetblankdistinctblankcountblanksalesblankorder[]



// end::description-measure-internetblankdistinctblankcountblanksalesblankorder[]

=== Internet Order Lines Count - description

// tag::description-measure-internetblankorderblanklinesblankcount[]



// end::description-measure-internetblankorderblanklinesblankcount[]

=== Internet Previous Quarter Margin - description

// tag::description-measure-internetblankpreviousblankquarterblankmargin[]



// end::description-measure-internetblankpreviousblankquarterblankmargin[]

=== Internet Previous Quarter Margin Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

=== Internet Previous Quarter Sales - description

// tag::description-measure-internetblankpreviousblankquarterblanksales[]



// end::description-measure-internetblankpreviousblankquarterblanksales[]

=== Internet Previous Quarter Sales Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

=== Internet Total Discount Amount - description

// tag::description-measure-internetblanktotalblankdiscountblankamount[]



// end::description-measure-internetblanktotalblankdiscountblankamount[]

=== Internet Total Freight - description

// tag::description-measure-internetblanktotalblankfreight[]



// end::description-measure-internetblanktotalblankfreight[]

=== Internet Total Margin - description

// tag::description-measure-internetblanktotalblankmargin[]



// end::description-measure-internetblanktotalblankmargin[]

=== Internet Total Product Cost - description

// tag::description-measure-internetblanktotalblankproductblankcost[]



// end::description-measure-internetblanktotalblankproductblankcost[]

=== Internet Total Sales - description

// tag::description-measure-internetblanktotalblanksales[]



// end::description-measure-internetblanktotalblanksales[]

=== Internet Total Tax Amt - description

// tag::description-measure-internetblanktotalblanktaxblankamt[]



// end::description-measure-internetblanktotalblanktaxblankamt[]

=== Internet Total Units - description

// tag::description-measure-internetblanktotalblankunits[]



// end::description-measure-internetblanktotalblankunits[]

== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:ssasdb:adventureworks.productblankcategory.adoc[] - 
* xref:aw:ssasdb-de-DE:adventureworks.productblankcategory.adoc[] - de-DE
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
2
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldwdb:dbo.dimproductcategory.adoc[] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
1
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
1
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]

:is_repo_managed:
:is_ssas:

// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
  Product_Category_Alternate_Id : (int64)
  Product_Category_Id : (int64)
  Product_Category_Name : (string)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
ix_Product_Category_col_Product_Category_Id

..
Product Category Id; int64
}

entity "puml-link:aw:ssasdb:adventureworks.productblanksubcategory.adoc[]" as AdventureWorks.Product_Subcategory << U >> {
ix_Product_Subcategory_col_Product_Category_Id

..
Product Category Id; int64
--
ix_Product_Subcategory_col_Product_Subcategory_Id

..
Product Subcategory Id; int64
}

"AdventureWorks.Product_Category::ix_Product_Category_col_Product_Category_Id" <-- "AdventureWorks.Product_Subcategory::ix_Product_Subcategory_col_Product_Category_Id"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as aw.sqldwdb.dbo.DimProductCategory <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimProductCategory <.. AdventureWorks.Product_Category

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as aw.sqldwdb.dbo.DimProductCategory <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimProductCategory <.. AdventureWorks.Product_Category

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.productblankcategory.adoc[]" as AdventureWorks.Product_Category << U >> {
  Product_Category_Alternate_Id : (int64)
  Product_Category_Id : (int64)
  Product_Category_Name : (string)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproductcategory.adoc[]" as aw.sqldwdb.dbo.DimProductCategory <AdventureWorksDW2019>  << external >> #line.dashed {
  EnglishProductCategoryName
  ProductCategoryAlternateKey
  ProductCategoryKey
  --
}

aw.sqldwdb.dbo.DimProductCategory <.. AdventureWorks.Product_Category
"aw.sqldwdb.dbo.DimProductCategory::EnglishProductCategoryName" <-- "AdventureWorks.Product_Category::Product_Category_Name"
"aw.sqldwdb.dbo.DimProductCategory::ProductCategoryAlternateKey" <-- "AdventureWorks.Product_Category::Product_Category_Alternate_Id"
"aw.sqldwdb.dbo.DimProductCategory::ProductCategoryKey" <-- "AdventureWorks.Product_Category::Product_Category_Id"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


