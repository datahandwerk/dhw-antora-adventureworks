// tag::HeaderFullDisplayName[]
= AdventureWorks.Internet Sales - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
:ExistsProperty--Measures:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
B0D25B38-184A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
AdventureWorks.Internet Sales
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]

// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]

// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-carrierblanktrackingblanknumber]
=== Carrier Tracking Number

[cols="d,8a,m,m,m"]
|===
|
|Carrier Tracking Number
|string
|NULL
|
|===


[#column-currencyblankid]
=== Currency Id

[cols="d,8a,m,m,m"]
|===
|
|Currency Id
|int64
|NULL
|
|===


[#column-customerblankid]
=== Customer Id

[cols="d,8a,m,m,m"]
|===
|
|Customer Id
|int64
|NULL
|
|===


[#column-customerblankpoblanknumber]
=== Customer PO Number

[cols="d,8a,m,m,m"]
|===
|
|Customer PO Number
|string
|NULL
|
|===


[#column-discountblankamount]
=== Discount Amount

[cols="d,8a,m,m,m"]
|===
|
|Discount Amount
|double
|NULL
|
|===


[#column-dueblankdate]
=== Due Date

[cols="d,8a,m,m,m"]
|===
|
|Due Date
|dateTime
|NULL
|
|===


[#column-extendedblankamount]
=== Extended Amount

[cols="d,8a,m,m,m"]
|===
|
|Extended Amount
|decimal
|NULL
|
|===


[#column-freight]
=== Freight

[cols="d,8a,m,m,m"]
|===
|
|Freight
|decimal
|NULL
|
|===


[#column-margin]
=== Margin

[cols="d,8a,m,m,m"]
|===
|
|Margin
|decimal
|NULL
|
|===


[#column-orderblankdate]
=== Order Date

[cols="d,8a,m,m,m"]
|===
|
|Order Date
|dateTime
|NULL
|
|===


[#column-orderblankquantity]
=== Order Quantity

[cols="d,8a,m,m,m"]
|===
|
|Order Quantity
|int64
|NULL
|
|===


[#column-productblankid]
=== Product Id

[cols="d,8a,m,m,m"]
|===
|
|Product Id
|int64
|NULL
|
|===


[#column-productblankstandardblankcost]
=== Product Standard Cost

[cols="d,8a,m,m,m"]
|===
|
|Product Standard Cost
|decimal
|NULL
|
|===


[#column-promotionblankid]
=== Promotion Id

[cols="d,8a,m,m,m"]
|===
|
|Promotion Id
|int64
|NULL
|
|===


[#column-revisionblanknumber]
=== Revision Number

[cols="d,8a,m,m,m"]
|===
|
|Revision Number
|int64
|NULL
|
|===


[#column-salesblankamount]
=== Sales Amount

[cols="d,8a,m,m,m"]
|===
|
|Sales Amount
|decimal
|NULL
|
|===


[#column-salesblankorderblanklineblanknumber]
=== Sales Order Line Number

[cols="d,8a,m,m,m"]
|===
|
|Sales Order Line Number
|int64
|NULL
|
|===


[#column-salesblankorderblanknumber]
=== Sales Order Number

[cols="d,8a,m,m,m"]
|===
|
|Sales Order Number
|string
|NULL
|
|===


[#column-salesblankterritoryblankid]
=== Sales Territory Id

[cols="d,8a,m,m,m"]
|===
|
|Sales Territory Id
|int64
|NULL
|
|===


[#column-shipblankdate]
=== Ship Date

[cols="d,8a,m,m,m"]
|===
|
|Ship Date
|dateTime
|NULL
|
|===


[#column-taxblankamt]
=== Tax Amt

[cols="d,8a,m,m,m"]
|===
|
|Tax Amt
|decimal
|NULL
|
|===


[#column-totalblankproductblankcost]
=== Total Product Cost

[cols="d,8a,m,m,m"]
|===
|
|Total Product Cost
|decimal
|NULL
|
|===


[#column-unitblankprice]
=== Unit Price

[cols="d,8a,m,m,m"]
|===
|
|Unit Price
|decimal
|NULL
|
|===


[#column-unitblankpriceblankdiscountblankpct]
=== Unit Price Discount Pct

[cols="d,8a,m,m,m"]
|===
|
|Unit Price Discount Pct
|double
|NULL
|
|===


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]
























// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-carrierblanktrackingblanknumber>>
|string
|NULL
|

|
|<<column-currencyblankid>>
|int64
|NULL
|

|
|<<column-customerblankid>>
|int64
|NULL
|

|
|<<column-customerblankpoblanknumber>>
|string
|NULL
|

|
|<<column-discountblankamount>>
|double
|NULL
|

|
|<<column-dueblankdate>>
|dateTime
|NULL
|

|
|<<column-extendedblankamount>>
|decimal
|NULL
|

|
|<<column-freight>>
|decimal
|NULL
|

|
|<<column-margin>>
|decimal
|NULL
|

|
|<<column-orderblankdate>>
|dateTime
|NULL
|

|
|<<column-orderblankquantity>>
|int64
|NULL
|

|
|<<column-productblankid>>
|int64
|NULL
|

|
|<<column-productblankstandardblankcost>>
|decimal
|NULL
|

|
|<<column-promotionblankid>>
|int64
|NULL
|

|
|<<column-revisionblanknumber>>
|int64
|NULL
|

|
|<<column-salesblankamount>>
|decimal
|NULL
|

|
|<<column-salesblankorderblanklineblanknumber>>
|int64
|NULL
|

|
|<<column-salesblankorderblanknumber>>
|string
|NULL
|

|
|<<column-salesblankterritoryblankid>>
|int64
|NULL
|

|
|<<column-shipblankdate>>
|dateTime
|NULL
|

|
|<<column-taxblankamt>>
|decimal
|NULL
|

|
|<<column-totalblankproductblankcost>>
|decimal
|NULL
|

|
|<<column-unitblankprice>>
|decimal
|NULL
|

|
|<<column-unitblankpriceblankdiscountblankpct>>
|double
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-ixunderlineinternetblanksalesunderlinecolunderlinecustomerblankid]
=== ix_Internet Sales_col_Customer Id

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Customer Id>>; int64
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Internet Sales_TO_Customer++ +
referenced: xref:adventureworks.customer.adoc[], xref:adventureworks.customer.adoc#index-ixunderlinecustomerunderlinecolunderlinecustomerblankid[+ix_Customer_col_Customer Id+]


[#index-ixunderlineinternetblanksalesunderlinecolunderlinedueblankdate]
=== ix_Internet Sales_col_Due Date

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Due Date>>; dateTime
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Internet Sales_TO_Date++ +
referenced: xref:adventureworks.date.adoc[], xref:adventureworks.date.adoc#index-keyunderlinedateunderlinecolunderlinedate[+key_Date_col_Date+]


[#index-ixunderlineinternetblanksalesunderlinecolunderlineorderblankdate]
=== ix_Internet Sales_col_Order Date

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Order Date>>; dateTime
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Internet Sales_TO_Date++ +
referenced: xref:adventureworks.date.adoc[], xref:adventureworks.date.adoc#index-keyunderlinedateunderlinecolunderlinedate[+key_Date_col_Date+]


[#index-ixunderlineinternetblanksalesunderlinecolunderlineproductblankid]
=== ix_Internet Sales_col_Product Id

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Product Id>>; int64
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Internet Sales_TO_Product++ +
referenced: xref:adventureworks.product.adoc[], xref:adventureworks.product.adoc#index-ixunderlineproductunderlinecolunderlineproductblankid[+ix_Product_col_Product Id+]


[#index-ixunderlineinternetblanksalesunderlinecolunderlineshipblankdate]
=== ix_Internet Sales_col_Ship Date

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Ship Date>>; dateTime
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Internet Sales_TO_Date++ +
referenced: xref:adventureworks.date.adoc[], xref:adventureworks.date.adoc#index-keyunderlinedateunderlinecolunderlinedate[+key_Date_col_Date+]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]
[discrete]
== _


[#measure-internetblankcurrentblankquarterblankmargin]
=== Internet Current Quarter Margin

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblankmargin,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankcurrentblankquarterblankmargin[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblankmargin,opts=optional]

endif::hide-exported-description-measure-internetblankcurrentblankquarterblankmargin[]

--
{empty} +

.Expression
....
TOTALQTD([Internet Total Margin],'Date'[Date])
....




[#measure-internetblankcurrentblankquarterblankmarginblankperformance]
=== Internet Current Quarter Margin Performance

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblankmarginblankperformance,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblankmarginblankperformance,opts=optional]

endif::hide-exported-description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

--
{empty} +

.Expression
....
IF([Internet Previous Quarter Margin Proportion to QTD]<>0,([Internet Current Quarter Margin]-[Internet Previous Quarter Margin Proportion to QTD])/[Internet Previous Quarter Margin Proportion to QTD],BLANK())
....




[#measure-internetblankcurrentblankquarterblanksales]
=== Internet Current Quarter Sales

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblanksales,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankcurrentblankquarterblanksales[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblanksales,opts=optional]

endif::hide-exported-description-measure-internetblankcurrentblankquarterblanksales[]

--
{empty} +

.Expression
....
TOTALQTD([Internet Total Sales],'Date'[Date])
....




[#measure-internetblankcurrentblankquarterblanksalesblankperformance]
=== Internet Current Quarter Sales Performance

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblanksalesblankperformance,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankcurrentblankquarterblanksalesblankperformance,opts=optional]

endif::hide-exported-description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

--
{empty} +

.Expression
....
IFERROR([Internet Current Quarter Sales]/[Internet Previous Quarter Sales Proportion to QTD],BLANK())
....




[#measure-internetblankdistinctblankcountblanksalesblankorder]
=== Internet Distinct Count Sales Order

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankdistinctblankcountblanksalesblankorder,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankdistinctblankcountblanksalesblankorder[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankdistinctblankcountblanksalesblankorder,opts=optional]

endif::hide-exported-description-measure-internetblankdistinctblankcountblanksalesblankorder[]

--
{empty} +

.Expression
....
DISTINCTCOUNT([Sales Order Number])
....




[#measure-internetblankorderblanklinesblankcount]
=== Internet Order Lines Count

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankorderblanklinesblankcount,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankorderblanklinesblankcount[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankorderblanklinesblankcount,opts=optional]

endif::hide-exported-description-measure-internetblankorderblanklinesblankcount[]

--
{empty} +

.Expression
....
COUNTA([Sales Order Line Number])
....




[#measure-internetblankpreviousblankquarterblankmargin]
=== Internet Previous Quarter Margin

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblankmargin,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankpreviousblankquarterblankmargin[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblankmargin,opts=optional]

endif::hide-exported-description-measure-internetblankpreviousblankquarterblankmargin[]

--
{empty} +

.Expression
....
CALCULATE([Internet Total Margin],PREVIOUSQUARTER('Date'[Date]))
....




[#measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd]
=== Internet Previous Quarter Margin Proportion to QTD

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd,opts=optional]

endif::hide-exported-description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

--
{empty} +

.Expression
....
[Internet Previous Quarter Margin]*([Days Current Quarter to Date]/[Days In Current Quarter])
....




[#measure-internetblankpreviousblankquarterblanksales]
=== Internet Previous Quarter Sales

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblanksales,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankpreviousblankquarterblanksales[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblanksales,opts=optional]

endif::hide-exported-description-measure-internetblankpreviousblankquarterblanksales[]

--
{empty} +

.Expression
....
CALCULATE([Internet Total Sales],PREVIOUSQUARTER('Date'[Date]))
....




[#measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd]
=== Internet Previous Quarter Sales Proportion to QTD

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd,opts=optional]

endif::hide-exported-description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

--
{empty} +

.Expression
....
[Internet Previous Quarter Sales]*([Days Current Quarter to Date]/[Days In Current Quarter])
....




[#measure-internetblanktotalblankdiscountblankamount]
=== Internet Total Discount Amount

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankdiscountblankamount,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblankdiscountblankamount[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankdiscountblankamount,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblankdiscountblankamount[]

--
{empty} +

.Expression
....
SUM([Discount Amount])
....




[#measure-internetblanktotalblankfreight]
=== Internet Total Freight

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankfreight,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblankfreight[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankfreight,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblankfreight[]

--
{empty} +

.Expression
....
SUM([Freight])
....




[#measure-internetblanktotalblankmargin]
=== Internet Total Margin

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankmargin,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblankmargin[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankmargin,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblankmargin[]

--
{empty} +

.Expression
....
SUM([Margin])
....




[#measure-internetblanktotalblankproductblankcost]
=== Internet Total Product Cost

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankproductblankcost,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblankproductblankcost[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankproductblankcost,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblankproductblankcost[]

--
{empty} +

.Expression
....
SUM([Total Product Cost])
....




[#measure-internetblanktotalblanksales]
=== Internet Total Sales

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblanksales,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblanksales[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblanksales,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblanksales[]

--
{empty} +

.Expression
....
SUM([Sales Amount])
....




[#measure-internetblanktotalblanktaxblankamt]
=== Internet Total Tax Amt

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblanktaxblankamt,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblanktaxblankamt[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblanktaxblankamt,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblanktaxblankamt[]

--
{empty} +

.Expression
....
SUM([Tax Amt])
....




[#measure-internetblanktotalblankunits]
=== Internet Total Units

.Description
--
include::partial$descriptiontags/adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankunits,opts=optional]

//the following attribute could be set in the include above to enable or disable the usage of exported descriptions

ifndef::hide-exported-description-measure-internetblanktotalblankunits[]

include::partial$adventureworks._measures.adoc[tag=description-measure-internetblanktotalblankunits,opts=optional]

endif::hide-exported-description-measure-internetblanktotalblankunits[]

--
{empty} +

.Expression
....
SUM([Order Quantity])
....




// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions


=== Days Current Quarter to Date - description

// tag::description-measure-daysblankcurrentblankquarterblanktoblankdate[]



// end::description-measure-daysblankcurrentblankquarterblanktoblankdate[]

=== Days in Current Quarter - description

// tag::description-measure-daysblankinblankcurrentblankquarter[]



// end::description-measure-daysblankinblankcurrentblankquarter[]

=== Internet Current Quarter Margin - description

// tag::description-measure-internetblankcurrentblankquarterblankmargin[]



// end::description-measure-internetblankcurrentblankquarterblankmargin[]

=== Internet Current Quarter Margin Performance - description

// tag::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

=== Internet Current Quarter Sales - description

// tag::description-measure-internetblankcurrentblankquarterblanksales[]



// end::description-measure-internetblankcurrentblankquarterblanksales[]

=== Internet Current Quarter Sales Performance - description

// tag::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

=== Internet Distinct Count Sales Order - description

// tag::description-measure-internetblankdistinctblankcountblanksalesblankorder[]



// end::description-measure-internetblankdistinctblankcountblanksalesblankorder[]

=== Internet Order Lines Count - description

// tag::description-measure-internetblankorderblanklinesblankcount[]



// end::description-measure-internetblankorderblanklinesblankcount[]

=== Internet Previous Quarter Margin - description

// tag::description-measure-internetblankpreviousblankquarterblankmargin[]



// end::description-measure-internetblankpreviousblankquarterblankmargin[]

=== Internet Previous Quarter Margin Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

=== Internet Previous Quarter Sales - description

// tag::description-measure-internetblankpreviousblankquarterblanksales[]



// end::description-measure-internetblankpreviousblankquarterblanksales[]

=== Internet Previous Quarter Sales Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

=== Internet Total Discount Amount - description

// tag::description-measure-internetblanktotalblankdiscountblankamount[]



// end::description-measure-internetblanktotalblankdiscountblankamount[]

=== Internet Total Freight - description

// tag::description-measure-internetblanktotalblankfreight[]



// end::description-measure-internetblanktotalblankfreight[]

=== Internet Total Margin - description

// tag::description-measure-internetblanktotalblankmargin[]



// end::description-measure-internetblanktotalblankmargin[]

=== Internet Total Product Cost - description

// tag::description-measure-internetblanktotalblankproductblankcost[]



// end::description-measure-internetblanktotalblankproductblankcost[]

=== Internet Total Sales - description

// tag::description-measure-internetblanktotalblanksales[]



// end::description-measure-internetblanktotalblanksales[]

=== Internet Total Tax Amt - description

// tag::description-measure-internetblanktotalblanktaxblankamt[]



// end::description-measure-internetblanktotalblanktaxblankamt[]

=== Internet Total Units - description

// tag::description-measure-internetblanktotalblankunits[]



// end::description-measure-internetblanktotalblankunits[]

== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:ssasdb:adventureworks.internetblanksales.adoc[] - 
* xref:aw:ssasdb-de-DE:adventureworks.internetblanksales.adoc[] - de-DE
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
2
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]

// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
1
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
1
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]

:is_repo_managed:
:is_ssas:

// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
  Carrier_Tracking_Number : (string)
  Currency_Id : (int64)
  Customer_Id : (int64)
  Customer_PO_Number : (string)
  Discount_Amount : (double)
  Due_Date : (dateTime)
  Extended_Amount : (decimal)
  Freight : (decimal)
  Margin : (decimal)
  Order_Date : (dateTime)
  Order_Quantity : (int64)
  Product_Id : (int64)
  Product_Standard_Cost : (decimal)
  Promotion_Id : (int64)
  Revision_Number : (int64)
  Sales_Amount : (decimal)
  Sales_Order_Line_Number : (int64)
  Sales_Order_Number : (string)
  Sales_Territory_Id : (int64)
  Ship_Date : (dateTime)
  Tax_Amt : (decimal)
  Total_Product_Cost : (decimal)
  Unit_Price : (decimal)
  Unit_Price_Discount_Pct : (double)
  --
  ~ Internet_Current_Quarter_Margin
  ~ Internet_Current_Quarter_Margin_Performance
  ~ Internet_Current_Quarter_Sales
  ~ Internet_Current_Quarter_Sales_Performance
  ~ Internet_Distinct_Count_Sales_Order
  ~ Internet_Order_Lines_Count
  ~ Internet_Previous_Quarter_Margin
  ~ Internet_Previous_Quarter_Margin_Proportion_to_QTD
  ~ Internet_Previous_Quarter_Sales
  ~ Internet_Previous_Quarter_Sales_Proportion_to_QTD
  ~ Internet_Total_Discount_Amount
  ~ Internet_Total_Freight
  ~ Internet_Total_Margin
  ~ Internet_Total_Product_Cost
  ~ Internet_Total_Sales
  ~ Internet_Total_Tax_Amt
  ~ Internet_Total_Units
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.customer.adoc[]" as AdventureWorks.Customer << U >> {
ix_Customer_col_Customer_Id

..
Customer Id; int64
--
ix_Customer_col_Geography_Id

..
Geography Id; int64
}

entity "puml-link:aw:ssasdb:adventureworks.date.adoc[]" as AdventureWorks.Date << U >> {
**key_Date_col_Date**

..
Date; dateTime
}

entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
ix_Internet_Sales_col_Customer_Id

..
Customer Id; int64
--
ix_Internet_Sales_col_Due_Date

..
Due Date; dateTime
--
ix_Internet_Sales_col_Order_Date

..
Order Date; dateTime
--
ix_Internet_Sales_col_Product_Id

..
Product Id; int64
--
ix_Internet_Sales_col_Ship_Date

..
Ship Date; dateTime
}

entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
ix_Product_col_Product_Id

..
Product Id; int64
--
ix_Product_col_Product_Subcategory_Id

..
Product Subcategory Id; int64
}

"AdventureWorks.Customer::ix_Customer_col_Customer_Id" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Customer_Id"
"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Due_Date"
"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Order_Date"
"AdventureWorks.Date::key_Date_col_Date" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Ship_Date"
"AdventureWorks.Product::ix_Product_col_Product_Id" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Product_Id"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
  Carrier_Tracking_Number : (string)
  Currency_Id : (int64)
  Customer_Id : (int64)
  Customer_PO_Number : (string)
  Discount_Amount : (double)
  Due_Date : (dateTime)
  Extended_Amount : (decimal)
  Freight : (decimal)
  Margin : (decimal)
  Order_Date : (dateTime)
  Order_Quantity : (int64)
  Product_Id : (int64)
  Product_Standard_Cost : (decimal)
  Promotion_Id : (int64)
  Revision_Number : (int64)
  Sales_Amount : (decimal)
  Sales_Order_Line_Number : (int64)
  Sales_Order_Number : (string)
  Sales_Territory_Id : (int64)
  Ship_Date : (dateTime)
  Tax_Amt : (decimal)
  Total_Product_Cost : (decimal)
  Unit_Price : (decimal)
  Unit_Price_Discount_Pct : (double)
  --
  ~ Internet_Current_Quarter_Margin
  ~ Internet_Current_Quarter_Margin_Performance
  ~ Internet_Current_Quarter_Sales
  ~ Internet_Current_Quarter_Sales_Performance
  ~ Internet_Distinct_Count_Sales_Order
  ~ Internet_Order_Lines_Count
  ~ Internet_Previous_Quarter_Margin
  ~ Internet_Previous_Quarter_Margin_Proportion_to_QTD
  ~ Internet_Previous_Quarter_Sales
  ~ Internet_Previous_Quarter_Sales_Proportion_to_QTD
  ~ Internet_Total_Discount_Amount
  ~ Internet_Total_Freight
  ~ Internet_Total_Margin
  ~ Internet_Total_Product_Cost
  ~ Internet_Total_Sales
  ~ Internet_Total_Tax_Amt
  ~ Internet_Total_Units
}




footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


