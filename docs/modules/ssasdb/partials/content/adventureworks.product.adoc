// tag::HeaderFullDisplayName[]
= AdventureWorks.Product - U
// end::HeaderFullDisplayName[]

== existing_properties

// tag::existing_properties[]

:ExistsProperty--antorareferencedlist:
:ExistsProperty--is_repo_managed:
:ExistsProperty--is_ssas:
:ExistsProperty--FK:
:ExistsProperty--AntoraIndexList:
:ExistsProperty--Columns:
// end::existing_properties[]

== RepoObject_guid

// tag::RepoObject_guid[]
B1D25B38-184A-EC11-8531-A81E8446D5B0
// end::RepoObject_guid[]

== RepoObject_FullDisplayName2

// tag::RepoObject_FullDisplayName2[]
AdventureWorks.Product
// end::RepoObject_FullDisplayName2[]

== SysObject_type

// tag::SysObject_type[]
U 
// end::SysObject_type[]

== SysObject_type_name

// tag::SysObject_type_name[]
user table
// end::SysObject_type_name[]

== SysObject_id

// tag::SysObject_id[]

// end::SysObject_id[]

== SysObject_modify_date

// tag::SysObject_modify_date[]

// end::SysObject_modify_date[]

== AntoraColumnDetails

// tag::AntoraColumnDetails[]
[discrete]
== _


[#column-class]
=== Class

[cols="d,8a,m,m,m"]
|===
|
|Class
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-class[+dbo.DimProduct.Class+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-color]
=== Color

[cols="d,8a,m,m,m"]
|===
|
|Color
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-color[+dbo.DimProduct.Color+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-daysblanktoblankmanufacture]
=== Days To Manufacture

[cols="d,8a,m,m,m"]
|===
|
|Days To Manufacture
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-daystomanufacture[+dbo.DimProduct.DaysToManufacture+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-dealerblankprice]
=== Dealer Price

[cols="d,8a,m,m,m"]
|===
|
|Dealer Price
|decimal
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-dealerprice[+dbo.DimProduct.DealerPrice+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-description]
=== Description

[cols="d,8a,m,m,m"]
|===
|
|Description
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-englishdescription[+dbo.DimProduct.EnglishDescription+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-isblankfinishedblankproduct]
=== Is Finished Product

[cols="d,8a,m,m,m"]
|===
|
|Is Finished Product
|boolean
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-finishedgoodsflag[+dbo.DimProduct.FinishedGoodsFlag+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-listblankprice]
=== List Price

[cols="d,8a,m,m,m"]
|===
|
|List Price
|decimal
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-listprice[+dbo.DimProduct.ListPrice+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-modelblankname]
=== Model Name

[cols="d,8a,m,m,m"]
|===
|
|Model Name
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-modelname[+dbo.DimProduct.ModelName+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankalternateblankid]
=== Product Alternate Id

[cols="d,8a,m,m,m"]
|===
|
|Product Alternate Id
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-productalternatekey[+dbo.DimProduct.ProductAlternateKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankcategoryblankname]
=== Product Category Name

[cols="d,8a,m,m,m"]
|===
|
|Product Category Name
|string
|NULL
|
|===


[#column-productblankendblankdate]
=== Product End Date

[cols="d,8a,m,m,m"]
|===
|
|Product End Date
|dateTime
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-enddate[+dbo.DimProduct.EndDate+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankid]
=== Product Id

[cols="d,8a,m,m,m"]
|===
|
|Product Id
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-productkey[+dbo.DimProduct.ProductKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankline]
=== Product Line

[cols="d,8a,m,m,m"]
|===
|
|Product Line
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-productline[+dbo.DimProduct.ProductLine+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankname]
=== Product Name

[cols="d,8a,m,m,m"]
|===
|
|Product Name
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-englishproductname[+dbo.DimProduct.EnglishProductName+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankstartblankdate]
=== Product Start Date

[cols="d,8a,m,m,m"]
|===
|
|Product Start Date
|dateTime
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-startdate[+dbo.DimProduct.StartDate+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblankstatus]
=== Product Status

[cols="d,8a,m,m,m"]
|===
|
|Product Status
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-status[+dbo.DimProduct.Status+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblanksubcategoryblankid]
=== Product Subcategory Id

[cols="d,8a,m,m,m"]
|===
|
|Product Subcategory Id
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-productsubcategorykey[+dbo.DimProduct.ProductSubcategoryKey+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-productblanksubcategoryblankname]
=== Product Subcategory Name

[cols="d,8a,m,m,m"]
|===
|
|Product Subcategory Name
|string
|NULL
|
|===


[#column-reorderblankpoint]
=== Reorder Point

[cols="d,8a,m,m,m"]
|===
|
|Reorder Point
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-reorderpoint[+dbo.DimProduct.ReorderPoint+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-safetyblankstockblanklevel]
=== Safety Stock Level

[cols="d,8a,m,m,m"]
|===
|
|Safety Stock Level
|int64
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-safetystocklevel[+dbo.DimProduct.SafetyStockLevel+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-size]
=== Size

[cols="d,8a,m,m,m"]
|===
|
|Size
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-size[+dbo.DimProduct.Size+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-sizeblankrange]
=== Size Range

[cols="d,8a,m,m,m"]
|===
|
|Size Range
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-sizerange[+dbo.DimProduct.SizeRange+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-sizeblankunitblankcode]
=== Size Unit Code

[cols="d,8a,m,m,m"]
|===
|
|Size Unit Code
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-sizeunitmeasurecode[+dbo.DimProduct.SizeUnitMeasureCode+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-standardblankcost]
=== Standard Cost

[cols="d,8a,m,m,m"]
|===
|
|Standard Cost
|decimal
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-standardcost[+dbo.DimProduct.StandardCost+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-style]
=== Style

[cols="d,8a,m,m,m"]
|===
|
|Style
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-style[+dbo.DimProduct.Style+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-weight]
=== Weight

[cols="d,8a,m,m,m"]
|===
|
|Weight
|double
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-weight[+dbo.DimProduct.Weight+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


[#column-weightblankunitblankcode]
=== Weight Unit Code

[cols="d,8a,m,m,m"]
|===
|
|Weight Unit Code
|string
|NULL
|
|===

.Referenced Columns
--
* xref:aw:sqldwdb:dbo.dimproduct.adoc#column-weightunitmeasurecode[+dbo.DimProduct.WeightUnitMeasureCode+] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
--


// end::AntoraColumnDetails[]

== AntoraPkColumnTableRows

// tag::AntoraPkColumnTableRows[]



























// end::AntoraPkColumnTableRows[]

== AntoraNonPkColumnTableRows

// tag::AntoraNonPkColumnTableRows[]
|
|<<column-class>>
|string
|NULL
|

|
|<<column-color>>
|string
|NULL
|

|
|<<column-daysblanktoblankmanufacture>>
|int64
|NULL
|

|
|<<column-dealerblankprice>>
|decimal
|NULL
|

|
|<<column-description>>
|string
|NULL
|

|
|<<column-isblankfinishedblankproduct>>
|boolean
|NULL
|

|
|<<column-listblankprice>>
|decimal
|NULL
|

|
|<<column-modelblankname>>
|string
|NULL
|

|
|<<column-productblankalternateblankid>>
|string
|NULL
|

|
|<<column-productblankcategoryblankname>>
|string
|NULL
|

|
|<<column-productblankendblankdate>>
|dateTime
|NULL
|

|
|<<column-productblankid>>
|int64
|NULL
|

|
|<<column-productblankline>>
|string
|NULL
|

|
|<<column-productblankname>>
|string
|NULL
|

|
|<<column-productblankstartblankdate>>
|dateTime
|NULL
|

|
|<<column-productblankstatus>>
|string
|NULL
|

|
|<<column-productblanksubcategoryblankid>>
|int64
|NULL
|

|
|<<column-productblanksubcategoryblankname>>
|string
|NULL
|

|
|<<column-reorderblankpoint>>
|int64
|NULL
|

|
|<<column-safetyblankstockblanklevel>>
|int64
|NULL
|

|
|<<column-size>>
|string
|NULL
|

|
|<<column-sizeblankrange>>
|string
|NULL
|

|
|<<column-sizeblankunitblankcode>>
|string
|NULL
|

|
|<<column-standardblankcost>>
|decimal
|NULL
|

|
|<<column-style>>
|string
|NULL
|

|
|<<column-weight>>
|double
|NULL
|

|
|<<column-weightblankunitblankcode>>
|string
|NULL
|

// end::AntoraNonPkColumnTableRows[]

== AntoraIndexList

// tag::AntoraIndexList[]

[#index-ixunderlineproductunderlinecolunderlineproductblankid]
=== ix_Product_col_Product Id

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Product Id>>; int64
--
* PK, Unique, Real: 0, 0, 1


[#index-ixunderlineproductunderlinecolunderlineproductblanksubcategoryblankid]
=== ix_Product_col_Product Subcategory Id

* IndexSemanticGroup: xref:other/indexsemanticgroup.adoc#startbnoblankgroupendb[no_group]
+
--
* <<column-Product Subcategory Id>>; int64
--
* PK, Unique, Real: 0, 0, 1
* ++FK_Product_TO_Product Subcategory++ +
referenced: xref:adventureworks.productblanksubcategory.adoc[], xref:adventureworks.productblanksubcategory.adoc#index-ixunderlineproductblanksubcategoryunderlinecolunderlineproductblanksubcategoryblankid[+ix_Product Subcategory_col_Product Subcategory Id+]

// end::AntoraIndexList[]

== AntoraMeasureDetails

// tag::AntoraMeasureDetails[]

// end::AntoraMeasureDetails[]

== AntoraMeasureDescriptions


=== Days Current Quarter to Date - description

// tag::description-measure-daysblankcurrentblankquarterblanktoblankdate[]



// end::description-measure-daysblankcurrentblankquarterblanktoblankdate[]

=== Days in Current Quarter - description

// tag::description-measure-daysblankinblankcurrentblankquarter[]



// end::description-measure-daysblankinblankcurrentblankquarter[]

=== Internet Current Quarter Margin - description

// tag::description-measure-internetblankcurrentblankquarterblankmargin[]



// end::description-measure-internetblankcurrentblankquarterblankmargin[]

=== Internet Current Quarter Margin Performance - description

// tag::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblankmarginblankperformance[]

=== Internet Current Quarter Sales - description

// tag::description-measure-internetblankcurrentblankquarterblanksales[]



// end::description-measure-internetblankcurrentblankquarterblanksales[]

=== Internet Current Quarter Sales Performance - description

// tag::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]



// end::description-measure-internetblankcurrentblankquarterblanksalesblankperformance[]

=== Internet Distinct Count Sales Order - description

// tag::description-measure-internetblankdistinctblankcountblanksalesblankorder[]



// end::description-measure-internetblankdistinctblankcountblanksalesblankorder[]

=== Internet Order Lines Count - description

// tag::description-measure-internetblankorderblanklinesblankcount[]



// end::description-measure-internetblankorderblanklinesblankcount[]

=== Internet Previous Quarter Margin - description

// tag::description-measure-internetblankpreviousblankquarterblankmargin[]



// end::description-measure-internetblankpreviousblankquarterblankmargin[]

=== Internet Previous Quarter Margin Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblankmarginblankproportionblanktoblankqtd[]

=== Internet Previous Quarter Sales - description

// tag::description-measure-internetblankpreviousblankquarterblanksales[]



// end::description-measure-internetblankpreviousblankquarterblanksales[]

=== Internet Previous Quarter Sales Proportion to QTD - description

// tag::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]



// end::description-measure-internetblankpreviousblankquarterblanksalesblankproportionblanktoblankqtd[]

=== Internet Total Discount Amount - description

// tag::description-measure-internetblanktotalblankdiscountblankamount[]



// end::description-measure-internetblanktotalblankdiscountblankamount[]

=== Internet Total Freight - description

// tag::description-measure-internetblanktotalblankfreight[]



// end::description-measure-internetblanktotalblankfreight[]

=== Internet Total Margin - description

// tag::description-measure-internetblanktotalblankmargin[]



// end::description-measure-internetblanktotalblankmargin[]

=== Internet Total Product Cost - description

// tag::description-measure-internetblanktotalblankproductblankcost[]



// end::description-measure-internetblanktotalblankproductblankcost[]

=== Internet Total Sales - description

// tag::description-measure-internetblanktotalblanksales[]



// end::description-measure-internetblanktotalblanksales[]

=== Internet Total Tax Amt - description

// tag::description-measure-internetblanktotalblanktaxblankamt[]



// end::description-measure-internetblanktotalblanktaxblankamt[]

=== Internet Total Units - description

// tag::description-measure-internetblanktotalblankunits[]



// end::description-measure-internetblanktotalblankunits[]

== AntoraParameterList

// tag::AntoraParameterList[]

// end::AntoraParameterList[]

== AntoraXrefCulturesList

// tag::AntoraXrefCulturesList[]
* xref:aw:ssasdb:adventureworks.product.adoc[] - 
* xref:aw:ssasdb-de-DE:adventureworks.product.adoc[] - de-DE
// end::AntoraXrefCulturesList[]

== cultures_count

// tag::cultures_count[]
2
// end::cultures_count[]

== Other tags

source: property.RepoObjectProperty_cross As rop_cross


=== AntoraReferencedList

// tag::antorareferencedlist[]
* xref:aw:sqldwdb:dbo.dimproduct.adoc[] in xref:aw:sqldwdb:nav/objects-by-schema.adoc[]
// end::antorareferencedlist[]


=== AntoraReferencingList

// tag::antorareferencinglist[]

// end::antorareferencinglist[]


=== Description

// tag::description[]

// end::description[]


=== is_repo_managed

// tag::is_repo_managed[]
1
// end::is_repo_managed[]


=== is_ssas

// tag::is_ssas[]
1
// end::is_ssas[]


=== pk_index_guid

// tag::pk_index_guid[]

// end::pk_index_guid[]


=== pk_IndexPatternColumnDatatype

// tag::pk_indexpatterncolumndatatype[]

// end::pk_indexpatterncolumndatatype[]


=== pk_IndexPatternColumnName

// tag::pk_indexpatterncolumnname[]

// end::pk_indexpatterncolumnname[]


=== ReferencedObjectList

// tag::referencedobjectlist[]

// end::referencedobjectlist[]

== Boolean Attributes

source: property.RepoObjectProperty WHERE property_int = 1

// tag::boolean_attributes[]

:is_repo_managed:
:is_ssas:

// end::boolean_attributes[]

== PlantUML diagrams

=== PlantUML Entity

// tag::puml_entity[]
[plantuml, entity-{docname}, svg, subs=macros]
....
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
  Class : (string)
  Color : (string)
  Days_To_Manufacture : (int64)
  Dealer_Price : (decimal)
  Description : (string)
  Is_Finished_Product : (boolean)
  List_Price : (decimal)
  Model_Name : (string)
  Product_Alternate_Id : (string)
  Product_Category_Name : (string)
  Product_End_Date : (dateTime)
  Product_Id : (int64)
  Product_Line : (string)
  Product_Name : (string)
  Product_Start_Date : (dateTime)
  Product_Status : (string)
  Product_Subcategory_Id : (int64)
  Product_Subcategory_Name : (string)
  Reorder_Point : (int64)
  Safety_Stock_Level : (int64)
  Size : (string)
  Size_Range : (string)
  Size_Unit_Code : (string)
  Standard_Cost : (decimal)
  Style : (string)
  Weight : (double)
  Weight_Unit_Code : (string)
  --
}
....

// end::puml_entity[]

=== PlantUML Entity 1 1 FK

// tag::puml_entity_1_1_fk[]
[plantuml, entity_1_1_fk-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.internetblanksales.adoc[]" as AdventureWorks.Internet_Sales << U >> {
ix_Internet_Sales_col_Customer_Id

..
Customer Id; int64
--
ix_Internet_Sales_col_Due_Date

..
Due Date; dateTime
--
ix_Internet_Sales_col_Order_Date

..
Order Date; dateTime
--
ix_Internet_Sales_col_Product_Id

..
Product Id; int64
--
ix_Internet_Sales_col_Ship_Date

..
Ship Date; dateTime
}

entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
ix_Product_col_Product_Id

..
Product Id; int64
--
ix_Product_col_Product_Subcategory_Id

..
Product Subcategory Id; int64
}

entity "puml-link:aw:ssasdb:adventureworks.productblanksubcategory.adoc[]" as AdventureWorks.Product_Subcategory << U >> {
ix_Product_Subcategory_col_Product_Category_Id

..
Product Category Id; int64
--
ix_Product_Subcategory_col_Product_Subcategory_Id

..
Product Subcategory Id; int64
}

"AdventureWorks.Product::ix_Product_col_Product_Id" <-- "AdventureWorks.Internet_Sales::ix_Internet_Sales_col_Product_Id"
"AdventureWorks.Product_Subcategory::ix_Product_Subcategory_col_Product_Subcategory_Id" <-- "AdventureWorks.Product::ix_Product_col_Product_Subcategory_Id"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_fk[]

=== PlantUML 1 1 ObjectRef

// tag::puml_entity_1_1_objectref[]
[plantuml, entity_1_1_objectref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as aw.sqldwdb.dbo.DimProduct <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimProduct <.. AdventureWorks.Product

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_objectref[]

=== PlantUML 30 0 ObjectRef

// tag::puml_entity_30_0_objectref[]
[plantuml, entity_30_0_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as aw.sqldwdb.dbo.DimProduct <AdventureWorksDW2019>  << external >> #line.dashed {
  --
}

aw.sqldwdb.dbo.DimProduct <.. AdventureWorks.Product

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_30_0_objectref[]

=== PlantUML 0 30 ObjectRef

// tag::puml_entity_0_30_objectref[]
[plantuml, entity_0_30_objectref-{docname}, svg, subs=macros]
....
@startuml
'Left to right direction
top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
  --
}



footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_0_30_objectref[]

=== PlantUML 1 1 ColumnRef

// tag::puml_entity_1_1_colref[]
[plantuml, entity_1_1_colref-{docname}, svg, subs=macros]
....
@startuml
left to right direction
'top to bottom direction
hide circle
'avoide "." issues:
set namespaceSeparator none


skinparam class {
  BackgroundColor White
  BackgroundColor<<FN>> Yellow
  BackgroundColor<<FS>> Yellow
  BackgroundColor<<FT>> LightGray
  BackgroundColor<<IF>> Yellow
  BackgroundColor<<IS>> Yellow
  BackgroundColor<<P>>  Aqua
  BackgroundColor<<PC>> Aqua
  BackgroundColor<<SN>> Yellow
  BackgroundColor<<SO>> SlateBlue
  BackgroundColor<<TF>> LightGray
  BackgroundColor<<TR>> Tomato
  BackgroundColor<<U>>  White
  BackgroundColor<<V>>  WhiteSmoke
  BackgroundColor<<X>>  Aqua
  BackgroundColor<<external>> AliceBlue
}


entity "puml-link:aw:ssasdb:adventureworks.product.adoc[]" as AdventureWorks.Product << U >> {
  Class : (string)
  Color : (string)
  Days_To_Manufacture : (int64)
  Dealer_Price : (decimal)
  Description : (string)
  Is_Finished_Product : (boolean)
  List_Price : (decimal)
  Model_Name : (string)
  Product_Alternate_Id : (string)
  Product_Category_Name : (string)
  Product_End_Date : (dateTime)
  Product_Id : (int64)
  Product_Line : (string)
  Product_Name : (string)
  Product_Start_Date : (dateTime)
  Product_Status : (string)
  Product_Subcategory_Id : (int64)
  Product_Subcategory_Name : (string)
  Reorder_Point : (int64)
  Safety_Stock_Level : (int64)
  Size : (string)
  Size_Range : (string)
  Size_Unit_Code : (string)
  Standard_Cost : (decimal)
  Style : (string)
  Weight : (double)
  Weight_Unit_Code : (string)
  --
}

entity "puml-link:aw:sqldwdb:dbo.dimproduct.adoc[]" as aw.sqldwdb.dbo.DimProduct <AdventureWorksDW2019>  << external >> #line.dashed {
  Class
  Color
  DaysToManufacture
  DealerPrice
  EndDate
  EnglishDescription
  EnglishProductName
  FinishedGoodsFlag
  ListPrice
  ModelName
  ProductAlternateKey
  ProductKey
  ProductLine
  ProductSubcategoryKey
  ReorderPoint
  SafetyStockLevel
  Size
  SizeRange
  SizeUnitMeasureCode
  StandardCost
  StartDate
  Status
  Style
  Weight
  WeightUnitMeasureCode
  --
}

aw.sqldwdb.dbo.DimProduct <.. AdventureWorks.Product
"aw.sqldwdb.dbo.DimProduct::Class" <-- "AdventureWorks.Product::Class"
"aw.sqldwdb.dbo.DimProduct::Color" <-- "AdventureWorks.Product::Color"
"aw.sqldwdb.dbo.DimProduct::DaysToManufacture" <-- "AdventureWorks.Product::Days_To_Manufacture"
"aw.sqldwdb.dbo.DimProduct::DealerPrice" <-- "AdventureWorks.Product::Dealer_Price"
"aw.sqldwdb.dbo.DimProduct::EndDate" <-- "AdventureWorks.Product::Product_End_Date"
"aw.sqldwdb.dbo.DimProduct::EnglishDescription" <-- "AdventureWorks.Product::Description"
"aw.sqldwdb.dbo.DimProduct::EnglishProductName" <-- "AdventureWorks.Product::Product_Name"
"aw.sqldwdb.dbo.DimProduct::FinishedGoodsFlag" <-- "AdventureWorks.Product::Is_Finished_Product"
"aw.sqldwdb.dbo.DimProduct::ListPrice" <-- "AdventureWorks.Product::List_Price"
"aw.sqldwdb.dbo.DimProduct::ModelName" <-- "AdventureWorks.Product::Model_Name"
"aw.sqldwdb.dbo.DimProduct::ProductAlternateKey" <-- "AdventureWorks.Product::Product_Alternate_Id"
"aw.sqldwdb.dbo.DimProduct::ProductKey" <-- "AdventureWorks.Product::Product_Id"
"aw.sqldwdb.dbo.DimProduct::ProductLine" <-- "AdventureWorks.Product::Product_Line"
"aw.sqldwdb.dbo.DimProduct::ProductSubcategoryKey" <-- "AdventureWorks.Product::Product_Subcategory_Id"
"aw.sqldwdb.dbo.DimProduct::ReorderPoint" <-- "AdventureWorks.Product::Reorder_Point"
"aw.sqldwdb.dbo.DimProduct::SafetyStockLevel" <-- "AdventureWorks.Product::Safety_Stock_Level"
"aw.sqldwdb.dbo.DimProduct::Size" <-- "AdventureWorks.Product::Size"
"aw.sqldwdb.dbo.DimProduct::SizeRange" <-- "AdventureWorks.Product::Size_Range"
"aw.sqldwdb.dbo.DimProduct::SizeUnitMeasureCode" <-- "AdventureWorks.Product::Size_Unit_Code"
"aw.sqldwdb.dbo.DimProduct::StandardCost" <-- "AdventureWorks.Product::Standard_Cost"
"aw.sqldwdb.dbo.DimProduct::StartDate" <-- "AdventureWorks.Product::Product_Start_Date"
"aw.sqldwdb.dbo.DimProduct::Status" <-- "AdventureWorks.Product::Product_Status"
"aw.sqldwdb.dbo.DimProduct::Style" <-- "AdventureWorks.Product::Style"
"aw.sqldwdb.dbo.DimProduct::Weight" <-- "AdventureWorks.Product::Weight"
"aw.sqldwdb.dbo.DimProduct::WeightUnitMeasureCode" <-- "AdventureWorks.Product::Weight_Unit_Code"

footer The diagram is interactive and contains links.

@enduml
....

// end::puml_entity_1_1_colref[]


== sql_modules_definition

// tag::sql_modules_definition[]
[%collapsible]
=======
[source,sql,numbered,indent=0]
----

----
=======
// end::sql_modules_definition[]


